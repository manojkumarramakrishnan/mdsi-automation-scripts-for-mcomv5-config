package MCOM_LIDSV5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;




















import jxl.write.DateFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.interactions.Actions;

import com.opera.core.systems.scope.protos.UmsProtos.Response;

public class MCOM_LIDSV5 extends XPath{
	static FileOutputStream fileLog = null;
	static FileOutputStream fileLog1 = null;
	static WebDriver driver;
	static void fileCreate() throws FileNotFoundException{
		String fileName = "D:\\TestReport\\SeleniumTestReport\\MCOMLIDSV5_"+new SimpleDateFormat("yyyyMMdd").format(new Date())+".csv";
		String fileNamest = "D:\\TestReport\\SeleniumTestReport\\MCOMLIDSV5SmokeTestCases_"+new SimpleDateFormat("yyyyMMdd").format(new Date())+".csv";
		System.out.println("fileName:" +fileName);
		fileLog = new FileOutputStream(new File(fileName));
		fileLog1 = new FileOutputStream(new File(fileNamest));
	}
	public static void logInfo(String logMsg) throws IOException{
		logMsg +="\n";
		fileLog.write(logMsg.getBytes());
	}
	public static void logInfo1(String logMsg) throws IOException{
		logMsg +="\n";
		fileLog1.write(logMsg.getBytes());
	}	
	public static void main(String[] args) throws InterruptedException, IOException, JSONException  
	{
		fileCreate();
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability("chrome.binary","C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
		System.setProperty("webdriver.ie.driver", "D:\\Registry\\IEDriverServer.exe");
		driver = new InternetExplorerDriver(capabilities);
		driver.manage().deleteAllCookies();
		//V5 Config & Smoke TestCases 		
		launchSite();
		//browsePage();
		//singlePdpPageValidation();
		searchByUPC();
		//non registry & Registry shopping bag
		shoppingBagValidation();
		settingPageValidation();
		searchByWebID();
		favoritePage();
		logOut();
		searchSuggestionsPage();		
		masterProductPage();
		browsePagePriceValidation();
		browseFacetsPage();
		registryMenuSelection();
		registryCreate();
		registryManage();
		registryFind();
	}
	private static void launchSite() 
	{	
		try
		{
			driver.get("http://localhost/SkavaCatalog/index_kiosk.html?t=1&debug=true");
			Thread.sleep(5000);
			Actions action = new Actions(driver);
			action.click().perform();
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void browsePage() 
	{
		try
		{
			//1.Verify that app gets installed and launched successfully without any errors 
			if(driver.findElement(By.xpath("//*[@id='skPageLayoutCell_83-north-center-north_slider']/div/div[3]/div[1]")).isDisplayed())
			{
				String logErrsmp1 ="1,Verify that app gets installed and launched successfully without any errors, Pass"; 
				logInfo1(logErrsmp1);
			}
			else
			{
				String logErrsmf1 ="1,Verify that app gets installed and launched successfully without any errors, Fail"; 
				logInfo1(logErrsmf1);
			}
			try
			{
				//working flow 	//Selecting the MLB category in the home page 
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_83-north-center-north_slider']/div/div[3]/div[1]")).click();
				Thread.sleep(3000);
				String logErr300 ="Pass:The Site gets launched Successfully"; 
				logInfo(logErr300);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}	
			try
			{
				//Selecting randomly the categories in  the MLB category 
				Random r2 = new java.util.Random();
				List<WebElement> links = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_12_id-center']/div/div"));
				WebElement randomElement = links.get(r2.nextInt(links.size()));
				randomElement.click();
				Thread.sleep(2000);  
				String logErr301 ="Pass:The Category Page gets displayed"; 
				logInfo(logErr301);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			Thread.sleep(4000);
			//2.Verify that while selecting all the categories in the home page, the browse page should be displayed 
			if(driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).isDisplayed())
			{
				String logErrsmp2 ="2,Verify that while selecting all the categories in the home page the browse page should be displayed, Pass"; 
				logInfo1(logErrsmp2);
			}
			else
			{
				String logErrsmf2 ="2,Verify that while selecting all the categories in the home page the browse page should be displayed, Fail"; 
				logInfo1 (logErrsmf2);
			}		
			Thread.sleep(2000);	
			try
			{
				//Selecting the products in the categories 
				Random r1 = new java.util.Random();
				List<WebElement> links1 = driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div/div"));
				WebElement randomElement1 = links1.get(r1.nextInt(links1.size()));
				randomElement1.click(); 
				Thread.sleep(5000); 
				String logErr302 ="Pass:The Browse Page gets displayed"; 
				logInfo(logErr302);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			System.out.println(e.toString());
		}
	}
	private static void singlePdpPageValidation() 
	{
		try
		{
			//To test the More Color Functionalities 
			//driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[9]/div/div[1]")).click();	
			//driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[2]/div[1]")).click();	
			//ProductID with the More Colors
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("1847120");		
			// driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("2308437");
			//Product with the two color Swatches 
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("2308456");
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
			Thread.sleep(6000);
			try
			{
				// Single Product Page
				String logErr303 ="Single Product Page"; 
				logInfo(logErr303);
				String logErr304="---------------------"; 
				logInfo(logErr304);
				String currentURL = driver.getCurrentUrl();
				System.out.println("currentURL:" +currentURL);  
				String pdtId=currentURL.substring(currentURL.indexOf("pdt_id=")+7,currentURL.indexOf("&type"));
				System.out.println("pdtId:" +pdtId); 
				String pdtUrl = "http://social.macys.com/skavastream/core/v5/macys/product/"+pdtId+"?type=ID&storeid=1&campaignId=383";
				URL url  = new URL(pdtUrl);
				String pageSource  = new Scanner(url.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource:" +pageSource); 
				if(pageSource.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr6 = "Fail:The Product Page doesn't gets displayed:\n" +pageSource;
					logInfo(logErr6);
				}
				else
				{	 
					JSONObject StreamPDPJson = new JSONObject(pageSource);
					System.out.println("StreamPDPJson:" +StreamPDPJson);	
					String strmPDPName=StreamPDPJson.getString("name");
					System.out.println("strmPDPName:" +strmPDPName);
					String PdtName=driver.findElement(By.xpath(ProductName)).getText();
					System.out.println("PdtName:" +PdtName);
					if(PdtName.equals(strmPDPName))
					{
						String logErr4 = "Pass:The Product Name gets matched with the stream call \nProduct name: " +PdtName+ "\nStreamCall Response:" +strmPDPName; 
						logInfo(logErr4);
					}
					else
					{
						String logErr5 = "Fail:The Product Name doesn't gets matched with the stream call \n Product name: " +PdtName+ "\nStreamCall Response:" +strmPDPName; 
						logInfo(logErr5);
					} 
					String PdtDescriptiontitle=driver.findElement(By.xpath(ProductDescriptionHeader)).getText();
					System.out.println("PdtDescriptiontitle:" +PdtDescriptiontitle);
					JSONObject strmiteminfo=StreamPDPJson.getJSONObject("properties").getJSONObject("iteminfo");
					JSONObject strmDesc=strmiteminfo.getJSONArray("description").getJSONObject(0);
					String strmDescriptiontitle=strmDesc.getString("value");
					System.out.println("strmDescriptiontitle:" +strmDescriptiontitle);
					if(PdtDescriptiontitle.equals(strmDescriptiontitle))
					{
						String logErr8 = "Pass:The Product Description gets matched:\n" +PdtDescriptiontitle+ "\n" +strmDescriptiontitle; 
						logInfo(logErr8);
					}
					else
					{
						String logErr9 = "Fail:The Product Description doesn't gets matched:\n" +PdtDescriptiontitle+ "\n" +strmDescriptiontitle; 
						logInfo(logErr9);
					}
					JSONArray strmBultdes=strmiteminfo.getJSONArray("bulletdescription");
					int Productbulletlength = driver.findElements(By.xpath(Productdesclength)).size();
					System.out.println("Productbulletlength:" +Productbulletlength);	    	
					for(int k=0,l=1;k<strmBultdes.length()||l<=Productbulletlength;k++,l++)
					{
						JSONObject strmBultdesc=strmBultdes.getJSONObject(k);
						String	strmBulletvalue=strmBultdesc.getString("value");
						System.out.println("strmBulletvalue:\n" +strmBulletvalue);
						String productbulletsdescription = driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[3]/div["+l+"]/div[2]")).getAttribute("innerHTML");
						System.out.println("productbulletsdescription:\n" +productbulletsdescription);
						if(productbulletsdescription.equals(strmBulletvalue))
						{
							String logErr6 = "Pass:The ProductDescription Bulletins gets matched:\n" +productbulletsdescription+ "\n" +strmBulletvalue; 
							logInfo(logErr6);
						}
						else
						{
							String logErr7 = "Fail:The ProductDescription Bulletins doesn't gets matched:\n" +productbulletsdescription+ "\n"+strmBulletvalue;
							logInfo(logErr7);
						} 
					}
					String Productdetailsnametitle=driver.findElement(By.xpath(ProductDetailsHeader)).getText();
					System.out.println("Productdetailsname:" +Productdetailsnametitle);
					String Productdescriptiontitle=driver.findElement(By.xpath(productdescription)).getText();
					System.out.println("Productdescriptiontitle:" +Productdescriptiontitle);
					//String Youmightalsoliketitle=driver.findElement(By.xpath("html/body/div[5]/div/div/div/div/div[2]/div[4]/div/div[3]/div[1]")).getAttribute("innerHTML");
					// System.out.println("Youmightalsoliketitle:" +Youmightalsoliketitle);
					//6.Verify that while selecting the products from the browse page,You Might Also Like Panel,search results page the Single PDP,Master and Member Products Pages should be displayed 
					if(driver.findElement(By.xpath("//*[@id='id_rightContainerDivChild']/div[1]/div[1]")).isDisplayed())
					{
						String logErrsmp6 ="6,Verify that while selecting the products from the browse page You Might Also Like Panel search results page the Single PDP Master and Member Products Pages should be displayed, Pass"; 
						logInfo1(logErrsmp6);
					}
					else
					{
						String logErrsmf6 ="6,Verify that while selecting the products from the browse page You Might Also Like Panel search results page the Single PDP Master and Member Products Pages should be displayed, Fail"; 
						logInfo1(logErrsmf6);
					}
					//5.Verify whether recommended products are shown in PDP page
					int productpanelcount=driver.findElements(By.xpath("//*[@id='id_rightContainerDivChild']/div")).size();
					System.out.println("productpanelcount:" +productpanelcount);	
					if(productpanelcount==2&&Productdetailsnametitle.equals("Product Details")&&Productdescriptiontitle.equals("Product Description"))
					{
						String logErrsmf5 ="5, Verify whether recommended products are shown in PDP page, Fail"; 
						logInfo1(logErrsmf5);

					}
					else if(productpanelcount==3&&driver.findElement(By.xpath("//*[@id='reviewCont']")).isDisplayed())
					{ 

						String logErrsmf5y ="5,Verify whether recommended products are shown in PDP page, Fail"; 
						logInfo1(logErrsmf5y);
					}
					else if(productpanelcount==3&&driver.findElement(By.xpath("//*[@id='id_recPdtContainer']/div[1]")).isDisplayed())
					{
						String logErrsmp5y ="5,Verify whether recommended products are shown in PDP page, Pass"; 
						logInfo1(logErrsmp5y);
					}
					//if(driver.findElement(By.xpath("//*[@id='reviewCont']")).isDisplayed())
					//{
					//String Productreviewnametitle=driver.findElement(By.xpath("//*[@id='id_pdtReviewContainer']/div[1]")).getAttribute("innerHTML");
					//System.out.println("Productreviewnametitle:" +Productreviewnametitle);
					//if(Productdetailsnametitle.equals("Product Details")&&Productdescriptiontitle.equals("Product Description")&&Productreviewnametitle.equals("Customer Reviews"))
					//{
					//String logErrsmf5y ="5.Verify whether recommended products are shown in PDP page::: Fail"; 
					//logInfo(logErrsmf5y);
					//}
					//}			
					//else if(driver.findElement(By.xpath("//*[@id='id_recPdtContainer']/div[1]")).isDisplayed())
					//{
					//	String Youmightalsoliketitle=driver.findElement(By.xpath("//*[@id='id_recPdtContainer']/div[1]")).getAttribute("innerHTML");
					//System.out.println("Youmightalsoliketitle:" +Youmightalsoliketitle);
					//if(Productdetailsnametitle.equals("Product Details")&&Productdescriptiontitle.equals("Product Description")&&Youmightalsoliketitle.equals("You Might Also Like"))
					//{
					//String logErrsmp5y ="5.Verify whether recommended products are shown in PDP page::: Pass"; 
					//logInfo(logErrsmp5y);
					//}
					//}
					//}
					else if(productpanelcount==4)
					{
						String logErrsmp5 ="5,Verify whether recommended products are shown in PDP page, Pass"; 
						logInfo1(logErrsmp5);
					}		
					if(Productdetailsnametitle.equals("Product Details")&&Productdescriptiontitle.equals("Product Description"))
					{
						String logErr2 = "Pass:The Product Page Titles gets matched:\n" +Productdetailsnametitle+ "\n" +Productdescriptiontitle;  
						logInfo(logErr2);
					}
					else
					{
						String logErr3 = "Fail:The Product Page Titles doesn't gets matched:\n" +Productdetailsnametitle+ "\n" +Productdescriptiontitle;
						logInfo(logErr3);
					}
					//Product additional images 
					driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_0']")).click();  
					String Productprimaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
					System.out.println("Productprimaryimageurl:" +Productprimaryimageurl);
					String Productprimaryimagetrim=Productprimaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					System.out.println("Prroductprimaryimagetrim:" +Productprimaryimagetrim);
					String strmPDPImage=StreamPDPJson.getString("image");
					System.out.println("strmPDPImage:" +strmPDPImage);
					if(Productprimaryimagetrim.equals(strmPDPImage))
					{
						String logErr14= "Pass:The Product Page Primary Image gets matched:\n" +Productprimaryimagetrim+ "\n" +strmPDPImage; 
						logInfo(logErr14);
					}
					else
					{
						String logErr15= "Fail:The Product Page Primary Image doesn't gets matched:\n" +Productprimaryimagetrim+ "\n" +strmPDPImage; 
						logInfo(logErr15);
					}
					driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click(); 
					int productadditionalimagecount=driver.findElements(By.xpath("//*[@id='id_styleImageDiv']/div/div")).size();  
					System.out.println("productadditionalimagecount:" +productadditionalimagecount);
					//Condition for Additional Images 
					//JSONArray strmadditionalimages= strmiteminfo.getJSONArray("additionalimages");
					//for(int a=0,b=2;a<strmadditionalimages.length()||b<=productadditionalimagecount;a++,b++)
					//{
					//JSONObject strmadditionalobject=strmadditionalimages.getJSONObject(a);
					//String streamadditionalimageurl=strmadditionalobject.getString("image");
					//System.out.println("streamadditionalimageurl:" +streamadditionalimageurl);  
					//driver.findElement(By.xpath("//*[@id='id_styleImageDiv']/div/div["+b+"]")).click();
					//System.out.println("b:" +b);
					//int c=b-1;
					//driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_"+c+"']")).click();
					//String Productsecondaryimageurlzoom=driver.findElement(By.xpath("//*[@id='id_skImageScroller_"+c+"']/img")).getAttribute("src");
					//System.out.println("Productsecondaryimageurlzoom:" +Productsecondaryimageurlzoom);
					//String Productsecondaryimageurlzoomtrim=Productsecondaryimageurlzoom.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					//System.out.println("Productsecondaryimageurlzoomtrim:" +Productsecondaryimageurlzoomtrim); 
					//if(Productsecondaryimageurlzoomtrim.equals(streamadditionalimageurl))
					//{
					//String logErr16= "Pass:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr16);
					//}
					//else
					//{
					//String logErr17= "Fail:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr17);
					//}    
					//driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click();
					//}
					//Color Count and  Selection
					int productcolorcountno=driver.findElements(By.xpath(productcolorcount)).size();
					System.out.println("productcolorcountno:" +productcolorcountno);
					for(int mo=1;mo<=productcolorcountno;mo++)
					{
						if(productcolorcountno>5) 
						{                                            
							driver.findElement(By.xpath("(//*[@class='moreColors'])")).click();
							Thread.sleep(1500);                 
							driver.findElement(By.xpath("(//*[@class='bPswatchImagediv'])["+mo+"]")).click();
							Thread.sleep(1000);     
							driver.findElement(By.xpath("(//*[@class='moreColorClose'])")).click();
						}
						else
						{				                    
							driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[2]/div["+mo+"]")).click();
							Thread.sleep(1500);
						}
						//Size Count and Selection
						int productskucount=driver.findElements(By.xpath(productskusize)).size(); 
						System.out.println("productskucount:" +productskucount);
						int totalskucount= productcolorcountno*productskucount;
						System.out.println("totalskucount:" +totalskucount);
						String productskusizedetailss=driver.findElement(By.xpath(productskusizedetail)).getAttribute("sizename");
						System.out.println("productskusizedetailss:" +productskusizedetailss);
						for(int n=1;n<=productskucount;n++)
						{
							driver.findElement(By.xpath(productsizeselection)).click(); 
							Thread.sleep(6000);                 
							driver.findElement(By.xpath("(//*[@class='sizeTxt'])["+n+"]")).click();
							Thread.sleep(3000);	     
							String productUPCvaluecheck= driver.findElement(By.xpath("(//*[@class='upcid'])")).getText();
							System.out.println("productUPCvaluecheck:" +productUPCvaluecheck);
							String Productupcvaluechecktrim=productUPCvaluecheck.substring(productUPCvaluecheck.indexOf("UPC:")+5);
							System.out.println("Productupcvaluechecktrim:" +Productupcvaluechecktrim);				
							//Stream Comparison 
							JSONObject strmChild=StreamPDPJson.getJSONObject("children");
							JSONArray strmSku=strmChild.getJSONArray("skus");
							for(int m=0;m<strmSku.length();m++)
							{
								JSONObject strmSkuobj=strmSku.getJSONObject(m);
								JSONObject strmProp=strmSkuobj.getJSONObject("properties");
								String  strmSkuId=strmSkuobj.getString("identifier");
								System.out.println("strmSkuId:" +strmSkuId);
								if(Productupcvaluechecktrim.equals(strmSkuId))
								{
									JSONObject strmPropr=strmProp.getJSONObject("orderinfo");
									String  strmProporder=strmPropr.getString("ordertype");
									System.out.println("strmProporder:" +strmProporder);
									JSONObject strmSkuinfo=strmProp.getJSONObject("skuinfo");
									JSONObject strmSkuColor=strmSkuinfo.getJSONObject("color");
									String  strmSkuColorvalue=strmSkuColor.getString("value");
									System.out.println("strmSkuColorvalue:" +strmSkuColorvalue);
									String strmSkuColorlabel=strmSkuColor.getString("label");
									System.out.println("strmSkuColorlabel:" +strmSkuColorlabel);
									JSONObject strmSkuSize=strmSkuinfo.getJSONObject("size1");  
									String  strmSkusizevalue=strmSkuSize.getString("value");
									System.out.println("strmSkusizevalue:" +strmSkusizevalue);
									String  strmSkusizelabel=strmSkuSize.getString("label");  
									System.out.println("strmSkusizelabel:" +strmSkusizelabel);
									JSONObject strmBuyinfo=strmProp.getJSONObject("buyinfo");
									JSONObject strmPricing=strmBuyinfo.getJSONObject("pricing");
									String  strmQtylimit=strmBuyinfo.getString("ropisqtylimit");
									System.out.println("strmQtylimit:" +strmQtylimit);
									JSONArray strmPrices=strmPricing.getJSONArray("prices");
									JSONObject strmOrstrPrice=strmPrices.getJSONObject(0);
									String  strmOrigstrprvalue=strmOrstrPrice.getString("value");
									System.out.println("strmOrigstrprvalue:" +strmOrigstrprvalue);
									JSONObject strmCurrstrPrice=strmPrices.getJSONObject(1);
									String strmCurrstrprvalue=strmCurrstrPrice.getString("value");
									System.out.println("strmCurrstrprvalue:" +strmCurrstrprvalue);
									JSONObject strmFedstrPrice=strmPrices.getJSONObject(2);
									String strmFedstrprvalue=strmFedstrPrice.getString("value");
									System.out.println("strmFedstrprvalue:" +strmFedstrprvalue);
									JSONArray strmAvailability=strmBuyinfo.getJSONArray("availability");
									JSONObject strmAvailability0=strmAvailability.getJSONObject(0);
									String strmOnlineinv=strmAvailability0.getString("onlineinventory");
									System.out.println("strmOnlineinv:" +strmOnlineinv);
									String strmOnline=strmAvailability0.getString("online");
									System.out.println("strmOnline:" +strmOnline); 
									String StrmFedfilinv=strmAvailability0.getString("fedfilinventory");
									System.out.println("StrmFedfilinv:" +StrmFedfilinv);
									String strmFedfil=strmAvailability0.getString("fedfil");
									System.out.println("strmFedfil:" +strmFedfil);
									Float strmOrigFloprvalue = Float.parseFloat(strmOrigstrprvalue);
									Float strmCurrFloprvalue = Float.parseFloat(strmCurrstrprvalue);
									Float strmFedFloprvalue = Float.parseFloat(strmFedstrprvalue); 
									JSONObject strmIteminfo=strmProp.getJSONObject("iteminfo");
									JSONArray strmShipmsgs=strmIteminfo.getJSONArray("shippingmessages");
									JSONObject strmShippingmsgs=strmShipmsgs.getJSONObject(0);
									String strmShippingmsgssuspain=strmShippingmsgs.getString("suspain");
									System.out.println("strmShippingmsgssuspain:" +strmShippingmsgssuspain);
									String strmShippingmsgsdeliverytype=strmShippingmsgs.getString("deliverytype");
									System.out.println("strmShippingmsgsdeliverytype:" +strmShippingmsgsdeliverytype);
									String strmShippingmsgsgiftwrap=strmShippingmsgs.getString("giftwrap");
									System.out.println("strmShippingmsgsgiftwrap:" +strmShippingmsgsgiftwrap);
									String strmShippingmsgsshipdate=strmShippingmsgs.getString("shipdate");
									System.out.println("strmShippingmsgsshipdate:" +strmShippingmsgsshipdate);
									String strmShippingmsgsshipdays=strmShippingmsgs.getString("shipdays");
									System.out.println("strmShippingmsgsshipdays:" +strmShippingmsgsshipdays); 
									String strmShippingmsgsmethod=strmShippingmsgs.getString("method");
									System.out.println("strmShippingmsgsmethod:" +strmShippingmsgsmethod);
									JSONArray strmSwtch=strmIteminfo.getJSONArray("swatches");
									JSONObject strmSwatches0=strmSwtch.getJSONObject(0);
									JSONArray strmPdtimage=strmSwatches0.getJSONArray("pdtimage");
									JSONObject strmPdtimage0=strmPdtimage.getJSONObject(0);
									String strmPdtaddseqnumber=strmPdtimage0.getString("sequencenumber");
									System.out.println("strmPdtaddseqnumber:" +strmPdtaddseqnumber);
									String strmPdtaddname=strmPdtimage0.getString("name");
									System.out.println("strmPdtaddname:" +strmPdtaddname);
									String strmPdtaddimage=strmPdtimage0.getString("image");     
									System.out.println("strmPdtaddimage:" +strmPdtaddimage);
									JSONArray strmStoreinfo=strmProp.getJSONArray("storeinfo");
									System.out.println("strmStoreinfo:" +strmStoreinfo);
									//Need to add here size if doesn't satisfy 
									String WebID=driver.findElement(By.xpath(productwedid)).getText();
									System.out.println("WebID:" +WebID);
									String WebIDtrim=WebID.substring(WebID.indexOf("Web ID:")+8);
									System.out.println("WebIDtrim:" +WebIDtrim);
									String strmPDPIdentifier= StreamPDPJson.getString("identifier");
									System.out.println("strmPDPIdentifier:" +strmPDPIdentifier);
									if(WebIDtrim.equals(strmPDPIdentifier))
									{
										String logErr18= "Pass:The Product Page Identifier gets matched:\n" +WebIDtrim+"\n" +strmPDPIdentifier; 
										logInfo(logErr18);
									}
									else
									{
										String logErr19= "Fail:The Product Page Identifier doesn't gets matched:\n" +WebIDtrim+ "\n" +strmPDPIdentifier; 
										logInfo(logErr19);
									}
									String productUPCvalue= driver.findElement(By.xpath(productUPC)).getText();
									System.out.println("productUPCvalue:" +productUPCvalue);
									String Productupcvaluetrim=productUPCvalue.substring(productUPCvalue.indexOf("UPC:")+5);
									System.out.println("Productupcvaluetrim:" +Productupcvaluetrim);  
									if(Productupcvaluetrim.equals(strmSkuId))
									{
										String logErr20= "Pass:The Product UPC value gets matched:\n" +Productupcvaluetrim+ "\n" +strmSkuId; 
										logInfo(logErr20);
									}
									else
									{
										String logErr21= "Fail:The Product UPC value doesn't gets matched:\n" +Productupcvaluetrim+ "\n" +strmSkuId; 
										logInfo(logErr21);
									}
									String productpagecolorvalue=driver.findElement(By.xpath(productcolorvalue)).getText();

									System.out.println("productpagecolorvalue:" +productpagecolorvalue);
									if(productpagecolorvalue.equalsIgnoreCase(strmSkuColorvalue)); 
									{
										String logErr22= "Pass:The Product Page UPC Color gets matched:\n" +productpagecolorvalue+"\n" +strmSkuColorvalue; 
										logInfo(logErr22);
									}
									String productpagesizevalue=driver.findElement(By.xpath(productsizeselection)).getText(); 
									System.out.println("productpagesizevalue:" +productpagesizevalue);
									if(productpagesizevalue.equals(strmSkusizevalue))
									{
										String logErr24= "Pass:The Product Page UPC Size gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue; 
										logInfo(logErr24);
									}
									else
									{
										String logErr25= "Fail:The Product Page UPC Size doesn't gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue;
										logInfo(logErr25);
									}  
									String sale23pricevalueinpdp=driver.findElement(By.xpath(pricesale)).getText();
									System.out.println("sale23pricevalueinpdp:" +sale23pricevalueinpdp); 
									String pricevalueinpdp=driver.findElement(By.xpath(pricedisplayedinpdp)).getText();
									System.out.println("pricevalueinpdp:" +pricevalueinpdp);
									//Current and original are same
									if(strmCurrFloprvalue.compareTo(strmOrigFloprvalue)==0 && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
									{
										String logErr28= "Pass:The UPC Contains Current/Store price:\n" +pricevalueinpdp+"\n" +strmCurrFloprvalue; 
										logInfo(logErr28);
									}
									// Current/Store<Original
									else if(strmCurrFloprvalue.compareTo(strmOrigFloprvalue)<0 && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
									{
										String salepricevalueinpdp=driver.findElement(By.xpath(pricesale)).getText();
										System.out.println("salepricevalueinpdp:" +salepricevalueinpdp); 
										String logErr29= "Pass:The UPC Contains Current/Store in Red Original in Black w/ label:\n" +salepricevalueinpdp+"\n" +strmCurrFloprvalue+ "\n" +pricevalueinpdp+ "\n" +strmOrigFloprvalue; 
										logInfo(logErr29);
									}
									// Current/Store>Original
									else if(strmCurrFloprvalue.compareTo(strmOrigFloprvalue)>0 && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
									{
										String logErr30= "Pass:The UPC Contains Current/Store in Black:\n" +pricevalueinpdp+"\n" +strmCurrFloprvalue; 
										logInfo(logErr30);
									}
									// Original Price is null
									else if((strmOrigFloprvalue==0) && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
									{
										String logErr31= "Pass:The UPC Contains Current/Store in Black:\n" +pricevalueinpdp+"\n" +strmCurrFloprvalue; 
										logInfo(logErr31);
									}
									// Current price is null and Fedfilavailability price available 
									else if((strmCurrFloprvalue==0) && (strmFedfil.equals("true")))
									{
										String logErr31= "Pass:The UPC Contains Fedfil price:\n" +pricevalueinpdp+"\n" +strmFedFloprvalue; 
										logInfo(logErr31);
									}	
									// current price is null and Fedfilavailability price not available
									else if((strmCurrFloprvalue==0) && (strmFedfil.equals("false")))
									{
										String logErr32= "Pass:The UPC Contains Original price:\n" +pricevalueinpdp+"\n" +strmOrigFloprvalue; 
										logInfo(logErr32);
									}
									// Current and original price is null and Fedfilavailability price not available
									else if((strmCurrFloprvalue==0) && (strmOrigFloprvalue==0) && (strmFedfil.equals("false")))
									{
										String text="Price is unavailable, please validate price status in mPOS or POS.";
										String text1="Price is unavailable, please contact an  associate for pricing";
										if(pricevalueinpdp.equalsIgnoreCase(text))
										{
											String logErr33= "Pass:The Product Page contains penny price in Associate Mode:\n" +pricevalueinpdp+"\n" +text; 
											logInfo(logErr33);
										}
										else  
										{
											String logErr34= "Pass:The Product Page contains penny price in Customer Mode:\n" +pricevalueinpdp+ "\n" +text1; 
											logInfo(logErr34);
										}
									}
									else if((strmCurrFloprvalue==0) && (strmFedFloprvalue==0))
									{
										String logErr35= "Pass:The Product Page contains Original Price:\n" +pricevalueinpdp+ "\n" +strmOrigFloprvalue; 
										logInfo(logErr35);

									}
									//  Current/store price has penny price 
									else if	((strmCurrFloprvalue==0.01) && (strmFedFloprvalue<=0.10)) 			
									{
										String text="Price is unavailable, please validate price status in mPOS or POS.";
										String text1="Price is unavailable, please contact an  associate for pricing";
										if(pricevalueinpdp.equalsIgnoreCase(text))
										{
											String logErr33= "Pass:The Product Page contains penny price in Associate Mode:\n" +pricevalueinpdp+"\n" +text; 
											logInfo(logErr33);
										}
										else  
										{
											String logErr34= "Pass:The Product Page contains penny price in Customer Mode:\n" +pricevalueinpdp+ "\n" +text1; 
											logInfo(logErr34);
										}
									}  
									if(productcolorcountno<=5)
									{
										String logErr60= "Pass:The Product doesn't contain more colors:\n" +productcolorcountno; 
										logInfo(logErr60);
									}
									else
									{
										String logErr61= "Pass:The Product contains more colors:\n" +productcolorcountno; 
										logInfo(logErr61);
									}
									if(strmOnline.equals("true")&&strmFedfil.equals("true"))
									{
										String logErr35= "Pass:The UPC is Available for shipping"; 
										logInfo(logErr35);
										driver.findElement(By.xpath(addtoorder)).click();
										Thread.sleep(3000);
										String quickviewtitlestring=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
										System.out.println("quickviewtitlestring:" +quickviewtitlestring);										
										String quickviewwebidstring=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
										System.out.println("quickviewwebidstring:" +quickviewwebidstring);
										String quickviewwebidstringtrim=quickviewwebidstring.substring(quickviewwebidstring.indexOf("(")+1,quickviewwebidstring.indexOf(")"));
										System.out.println("quickviewwebidstringtrim:" +quickviewwebidstringtrim);
										String quickviewpricestring=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
										System.out.println("quickviewpricestring:" +quickviewpricestring);
										String quickviewcolorstring=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
										System.out.println("quickviewcolorstring:" +quickviewcolorstring);	
										String quickviewsizestring=driver.findElement(By.xpath("(//*[@class='overlaySizeInfoValue'])")).getText();
										System.out.println("quickviewsizestring:" +quickviewsizestring);
										String quickviewimageurl=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
										System.out.println("quickviewimageurl:" +quickviewimageurl);
										String quickviewimageurltrim=quickviewimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
										System.out.println("quickviewimageurltrim:" +quickviewimageurltrim);
										if(quickviewwebidstringtrim.equals(WebID)&&quickviewpricestring.equals(pricevalueinpdp)&&quickviewcolorstring.equalsIgnoreCase(productpagecolorvalue)&&quickviewsizestring.equals(productpagesizevalue)&&quickviewimageurltrim.equals(Productprimaryimagetrim))
										{
											String logErr38= "Pass:The details in the AddtoBag overlay value gets matched:\n" +quickviewtitlestring+"\n" +PdtName+ "\n" +quickviewwebidstringtrim+ "\n" +WebID+ "\n" +quickviewpricestring+ "\n" +pricevalueinpdp+ "\n" +quickviewcolorstring+ "\n" +productpagecolorvalue+ "\n" +quickviewsizestring+ "\n" +productpagesizevalue+ "\n" +quickviewimageurltrim+ "\n" +Productprimaryimagetrim;
											logInfo(logErr38);  
										}
										else
										{
											String logErr39= "Fail:The details in the AddtoBag overlay value doesn't gets matched:\n" +quickviewtitlestring+"\n" +PdtName+ "\n" +quickviewwebidstringtrim+ "\n" +WebID+ "\n" +quickviewpricestring+ "\n" +pricevalueinpdp+ "\n" +quickviewcolorstring+ "\n" +productpagecolorvalue+ "\n" +quickviewsizestring+ "\n" +productpagesizevalue+ "\n" +quickviewimageurltrim+ "\n" +Productprimaryimagetrim;
											logInfo(logErr39);  
										}
										driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();		
									}
									else
									{
										String logErr36= "Pass:The UPC is not Available for shipping";
										logInfo(logErr36);
									}
									//Check others stores functionalities 
									Thread.sleep(2500);
									driver.findElement(By.xpath(productotherstores)).click();
									Thread.sleep(5000);
									//Check other Stores stream call data 
									String pdtupcvalue = "http://social.macys.com/skavastream/core/v5/macys/product/"+Productupcvaluetrim+"?type=UPCWithNearbyStores&storeid=1&campaignId=383";
									URL url1  = new URL(pdtupcvalue);
									String pageSource1  = new Scanner(url1.openConnection().getInputStream()).useDelimiter("\\Z").next();
									System.out.println("pageSource1:" +pageSource1);
									JSONObject StreamPDPUpccheckotherstores=new JSONObject(pageSource1);
									JSONObject streamPDPUpcchild=StreamPDPUpccheckotherstores.getJSONObject("children");
									JSONArray streamPDPUPCSku=streamPDPUpcchild.getJSONArray("skus");
									for(int q=0;q<streamPDPUPCSku.length();q++)
									{
										JSONObject streamPDPUPCSkuobj=streamPDPUPCSku.getJSONObject(q);
										String streamPDPUPCidentifier= streamPDPUPCSkuobj.getString("identifier");
										System.out.println("streamPDPUPCidentifier:" +streamPDPUPCidentifier);
										if(streamPDPUPCidentifier.equals(Productupcvaluetrim))
										{
											JSONObject streamPropUPC=streamPDPUPCSkuobj.getJSONObject("properties");
											JSONArray streamPDPStoreinfo=streamPropUPC.getJSONArray("storeinfo"); 
											int productstorescounts=driver.findElements(By.xpath(productstoresize)).size(); 
											System.out.println("productstorescounts:" +productstorescounts); 
											for(int o=0,p=1;o<streamPDPStoreinfo.length()||p<=productstorescounts;o++,p++)
											{

												JSONObject strmStoreinfo1=streamPDPStoreinfo.getJSONObject(o);
												String strmStoreinfophone=strmStoreinfo1.getString("phone");
												System.out.println("strmStoreinfophone:" +strmStoreinfophone);
												String strmStoreinfoSequenceno=strmStoreinfo1.getString("sequencenumber");
												System.out.println("strmStoreinfoSequenceno:" +strmStoreinfoSequenceno);
												String strmStoreinfoInventory=strmStoreinfo1.getString("inventory");
												System.out.println("strmStoreinfoInventory:" +strmStoreinfoInventory);
												String strmStoreinfoName=strmStoreinfo1.getString("name");
												System.out.println("strmStoreinfoName:" +strmStoreinfoName);
												String strmStoreinfoZipcode=strmStoreinfo1.getString("zipcode");
												System.out.println("strmStoreinfoZipcode:" +strmStoreinfoZipcode);
												String strmStoreinfoState=strmStoreinfo1.getString("state");
												System.out.println("strmStoreinfoState:" +strmStoreinfoState);
												String strmStoreinfoAddress1=strmStoreinfo1.getString("address1");
												System.out.println("strmStoreinfoAddress1:" +strmStoreinfoAddress1);
												String strmStoreinfoAddress2=strmStoreinfo1.getString("address2");
												System.out.println("strmStoreinfoAddress2:" +strmStoreinfoAddress2);
												String strmStoreinfoIdentifier=strmStoreinfo1.getString("identifier");
												System.out.println("strmStoreinfoIdentifier:" +strmStoreinfoIdentifier);
												String strmStoreinfoCity=strmStoreinfo1.getString("city");
												System.out.println("strmStoreinfoCity:" +strmStoreinfoCity);
												String strmStoreinfoNameconcat=strmStoreinfoName+strmStoreinfoIdentifier;
												System.out.println("strmStoreinfoNameconcat:" +strmStoreinfoNameconcat);
												String strmstoreinfocombined=strmStoreinfoNameconcat.replaceAll(""+strmStoreinfoIdentifier+".*","")+" "+"("+strmStoreinfoIdentifier+")";
												System.out.println("strmstoreinfocombined:" +strmstoreinfocombined);
												String checkavailabilityheadertitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[1]/div/div[2]/div")).getText();
												System.out.println("checkavailabilityheadertitle:" +checkavailabilityheadertitle);
												driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[1]/div/div[2]/div")).click();
												Thread.sleep(2000);
												String checkavailabilitystreetNametitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[2]/div/div[1]")).getText();
												System.out.println("checkavailabilitystreetNametitle:" +checkavailabilitystreetNametitle);
												String checkavailabilitystoreLocationtitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[2]/div/div[2]")).getText();
												System.out.println("checkavailabilitystoreLocationtitle:" +checkavailabilitystoreLocationtitle);
												String checkavailabilitystorePhonetitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[2]/div/div[3]")).getText();
												System.out.println("checkavailabilitystorePhonetitle:" +checkavailabilitystorePhonetitle);
												driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[1]/div/div[2]/div")).click();	
												if(checkavailabilityheadertitle.equals(strmstoreinfocombined)&&checkavailabilitystreetNametitle.equals(strmStoreinfoAddress1)&&checkavailabilitystoreLocationtitle.equals(strmStoreinfoAddress2)&&checkavailabilitystorePhonetitle.equals(strmStoreinfophone))
												{
													String logErr27= "Pass:The Product Page UPC Checkother Stores value gets matched:\n" +checkavailabilityheadertitle+"\n" +strmstoreinfocombined+ "\n" +checkavailabilitystreetNametitle+ "\n" +strmStoreinfoAddress1+ "\n" +checkavailabilitystoreLocationtitle+ "\n" +strmStoreinfoAddress2+ "\n" +checkavailabilitystorePhonetitle+ "\n" +strmStoreinfophone;  
													logInfo(logErr27);
												}	
												else
												{
													String logErr28= "Fail:The Product Page UPC Checkother Stores value doesn't gets matched:\n" +checkavailabilityheadertitle+"\n" +strmstoreinfocombined+ "\n" +checkavailabilitystreetNametitle+ "\n" +strmStoreinfoAddress1+ "\n" +checkavailabilitystoreLocationtitle+ "\n" +strmStoreinfoAddress2+ "\n" +checkavailabilitystorePhonetitle+ "\n" +strmStoreinfophone;
													logInfo(logErr28);
												}
												Thread.sleep(5000);
												String logErr37= "-----------------------------------------------------------------------------";
												logInfo(logErr37);
												System.out.println("-----------------------------------------");  
											} 
											driver.findElement(By.xpath(productotherstoresclose)).click();    
											driver.findElement(By.xpath(favoriteicon)).click();	
										}
									}
								}
							}
						} 
					} 
				} 
				//8.Verify that PDP page price value should be displayed as per the PDP page price logic 
				String logErrsmp8="8,Verify that PDP page price value should be displayed as per the PDP page price logic, Pass"; 
				logInfo1(logErrsmp8);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}  
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void searchByUPC() 
	{
		try
		{
			try
			{
				//Search by UPC Values and comparison
				String logErr305 ="Search By UPC"; 
				logInfo(logErr305);
				String logErr306="---------------"; 
				logInfo(logErr306);  
				driver.findElement(By.xpath("(//*[@class='cls_mamFMenu'])")).click();	
				driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[2]/div[1]")).click();	
				Thread.sleep(1000);	    
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("886699560458");
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
				Thread.sleep(6000);
				String pdtUrlupc = "http://social.macys.com/skavastream/core/v5/macys/product/886699560458?type=UPCWithNearbyStores&storeid=1&campaignId=383";
				URL urlupc  = new URL(pdtUrlupc);
				String pageSourceupc  = new Scanner(urlupc.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSourceupc:" +pageSourceupc); 
				//9.Verify that PDP page should be displayed while searching the product via WebID and UPC
				String logErrsmp9="9,Verify that PDP page should be displayed while searching the product via WebID and UPC, Pass"; 
				logInfo1(logErrsmp9);
				if(pageSourceupc.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr251 = "Fail:The UPC Product Page doesn't gets displayed:\n" +pageSourceupc;
					logInfo(logErr251);
				}
				else
				{	 
					JSONObject StreamUPCJson = new JSONObject(pageSourceupc);
					System.out.println("StreamUPCJson:" +StreamUPCJson);	
					String strmupcPDPName=StreamUPCJson.getString("name");
					System.out.println("strmupcPDPName:" +strmupcPDPName);
					String upcPdtName=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_14.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_14_id-center.skc_pageCellLayout.cls_skWidget.PDP_page div#skPageLayoutCell_14_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.pdttitledec div")).getText();
					System.out.println("upcPdtName:" +upcPdtName);
					if(upcPdtName.equals(strmupcPDPName))
					{
						String logErr252 = "Pass:The Product Name gets matched with the stream call \nProduct name: " +upcPdtName+ "\nStreamCall Response:" +strmupcPDPName; 
						logInfo(logErr252);
					}
					else
					{
						String logErr253 = "Fail:The Product Name doesn't gets matched with the stream call \n Product name: " +upcPdtName+ "\nStreamCall Response:" +strmupcPDPName; 
						logInfo(logErr253);
					} 			
					String upcPdtDescriptiontitle=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_14.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_14_id-center.skc_pageCellLayout.cls_skWidget.PDP_page div#skPageLayoutCell_14_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDecContainer.snapItem div#id_descriptionContent.scrollerReviewItems div.reviewTitleContainer div.pdpLongDec.pdpEmailLongDesc")).getText();
					System.out.println("upcPdtDescriptiontitle:" +upcPdtDescriptiontitle);
					JSONObject strmupciteminfo=StreamUPCJson.getJSONObject("properties").getJSONObject("iteminfo");
					JSONObject strmupcDesc=strmupciteminfo.getJSONArray("description").getJSONObject(0);
					String strmupcDescriptiontitle=strmupcDesc.getString("value");
					System.out.println("strmupcDescriptiontitle:" +strmupcDescriptiontitle);
					if(upcPdtDescriptiontitle.equals(strmupcDescriptiontitle))
					{
						String logErr254 = "Pass:The Product Description gets matched:\n" +upcPdtDescriptiontitle+ "\n" +strmupcDescriptiontitle; 
						logInfo(logErr254);
					}
					else
					{
						String logErr255 = "Fail:The Product Description doesn't gets matched:\n" +upcPdtDescriptiontitle+ "\n" +strmupcDescriptiontitle; 
						logInfo(logErr255);
					}			
					JSONArray strmupcBultdes=strmupciteminfo.getJSONArray("bulletdescription");
					int Productupcbulletlength = driver.findElements(By.xpath("//*[@id='id_descriptionContent']/div/div[3]/div")).size();
					System.out.println("Productupcbulletlength:" +Productupcbulletlength);	    	
					for(int k4=0,l4=1;k4<strmupcBultdes.length()||l4<=Productupcbulletlength;k4++,l4++)
					{
						JSONObject strmupcBultdesc=strmupcBultdes.getJSONObject(k4);
						String	strmupcBulletvalue=strmupcBultdesc.getString("value");
						System.out.println("strmupcBulletvalue:\n" +strmupcBulletvalue);
						String productupcbulletsdescription = driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[3]/div["+l4+"]/div[2]")).getAttribute("innerHTML");
						System.out.println("productupcbulletsdescription:\n" +productupcbulletsdescription);
						if(productupcbulletsdescription.equals(strmupcBulletvalue))
						{
							String logErr256 = "Pass:The ProductDescription Bulletins gets matched:\n" +productupcbulletsdescription+ "\n" +strmupcBulletvalue; 
							logInfo(logErr256);
						}
						else
						{
							String logErr257 = "Fail:The ProductDescription Bulletins doesn't gets matched:\n" +productupcbulletsdescription+ "\n"+strmupcBulletvalue;
							logInfo(logErr257);
						} 
					}
					//Product additional images 
					driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_0']")).click();  
					String Productupcprimaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
					System.out.println("Productupcprimaryimageurl:" +Productupcprimaryimageurl);
					String Productupcprimaryimagetrim=Productupcprimaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					System.out.println("Productupcprimaryimagetrim:" +Productupcprimaryimagetrim);
					String strmupcPDPImage=StreamUPCJson.getString("image");
					System.out.println("strmupcPDPImage:" +strmupcPDPImage);
					if(Productupcprimaryimagetrim.equals(strmupcPDPImage))
					{
						String logErr258= "Pass:The Product Page Primary Image gets matched:\n" +Productupcprimaryimagetrim+ "\n" +strmupcPDPImage; 
						logInfo(logErr258);
					}
					else
					{
						String logErr259= "Fail:The Product Page Primary Image doesn't gets matched:\n" +Productupcprimaryimagetrim+ "\n" +strmupcPDPImage; 
						logInfo(logErr259);
					}		
					driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click(); 
					int productupcadditionalimagecount=driver.findElements(By.xpath("//*[@id='id_styleImageDiv']/div/div")).size();  
					System.out.println("productupcadditionalimagecount:" +productupcadditionalimagecount);
					JSONArray strmupcadditionalimages= strmupciteminfo.getJSONArray("additionalimage");
					for(int a4=0,b4=2;a4<strmupcadditionalimages.length()||b4<=productupcadditionalimagecount;a4++,b4++)
					{
						JSONObject strmupcadditionalobject=strmupcadditionalimages.getJSONObject(a4);
						String streamupcadditionalimageurl=strmupcadditionalobject.getString("image");
						System.out.println("streamupcadditionalimageurl:" +streamupcadditionalimageurl);  
						driver.findElement(By.xpath("//*[@id='id_styleImageDiv']/div/div["+b4+"]")).click();
						System.out.println("b4:" +b4);
						int c4=b4-1;
						driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_"+c4+"']")).click();
						String Productupcsecondaryimageurlzoom=driver.findElement(By.xpath("//*[@id='id_skImageScroller_"+c4+"']/img")).getAttribute("src");
						System.out.println("Productupcsecondaryimageurlzoom:" +Productupcsecondaryimageurlzoom);
						String Productupcsecondaryimageurlzoomtrim=Productupcsecondaryimageurlzoom.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
						System.out.println("Productupcsecondaryimageurlzoomtrim:" +Productupcsecondaryimageurlzoomtrim); 
						if(Productupcsecondaryimageurlzoomtrim.equals(streamupcadditionalimageurl))
						{
							String logErr260= "Pass:The Product Page Additional Image gets matched:\n" +Productupcsecondaryimageurlzoomtrim+ "\n" +Productupcsecondaryimageurlzoomtrim; 
							logInfo(logErr260);
						}
						else
						{
							String logErr261= "Fail:The Product Page Additional Image gets matched:\n" +Productupcsecondaryimageurlzoomtrim+ "\n" +Productupcsecondaryimageurlzoomtrim; 
							logInfo(logErr261);
						}    
						driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click();
					}	
					//Stream Comparison 
					JSONObject strmupcChild=StreamUPCJson.getJSONObject("children");
					JSONArray strmupcSku=strmupcChild.getJSONArray("skus");
					JSONObject strmupcSkuobj=strmupcSku.getJSONObject(0);
					JSONObject strmupcProp=strmupcSkuobj.getJSONObject("properties");
					String  strmupcSkuId=strmupcSkuobj.getString("identifier");
					System.out.println("strmupcSkuId:" +strmupcSkuId);
					JSONObject strmupcPropr=strmupcProp.getJSONObject("orderinfo");
					String  strmupcProporder=strmupcPropr.getString("ordertype");
					System.out.println("strmupcProporder:" +strmupcProporder);
					JSONObject strmupcSkuinfo=strmupcProp.getJSONObject("skuinfo");
					JSONObject strmupcSkuColor=strmupcSkuinfo.getJSONObject("color");
					String  strmupcSkuColorvalue=strmupcSkuColor.getString("value");
					System.out.println("strmupcSkuColorvalue:" +strmupcSkuColorvalue);
					String strmupcSkuColorlabel=strmupcSkuColor.getString("label");
					System.out.println("strmupcSkuColorlabel:" +strmupcSkuColorlabel);
					JSONObject strmupcSkuSize=strmupcSkuinfo.getJSONObject("size1");  
					String  strmupcSkusizevalue=strmupcSkuSize.getString("value");
					System.out.println("strmupcSkusizevalue:" +strmupcSkusizevalue);
					String  strmupcSkusizelabel=strmupcSkuSize.getString("label");  
					System.out.println("strmupcSkusizelabel:" +strmupcSkusizelabel);
					JSONObject strmupcBuyinfo=strmupcProp.getJSONObject("buyinfo");
					JSONObject strmupcPricing=strmupcBuyinfo.getJSONObject("pricing");
					String  strmupcQtylimit=strmupcBuyinfo.getString("ropisqtylimit");
					System.out.println("strmupcQtylimit:" +strmupcQtylimit);
					JSONArray strmupcPrices=strmupcPricing.getJSONArray("prices");
					JSONObject strmupcOrstrPrice=strmupcPrices.getJSONObject(0);
					String  strmupcOrigstrprvalue=strmupcOrstrPrice.getString("value");
					System.out.println("strmupcOrigstrprvalue:" +strmupcOrigstrprvalue);
					JSONObject strmupcCurrstrPrice=strmupcPrices.getJSONObject(1);
					String strmupcCurrstrprvalue=strmupcCurrstrPrice.getString("value");
					System.out.println("strmupcCurrstrprvalue:" +strmupcCurrstrprvalue);
					JSONObject strmupcFedstrPrice=strmupcPrices.getJSONObject(2);
					String strmupcFedstrprvalue=strmupcFedstrPrice.getString("value");
					System.out.println("strmupcFedstrprvalue:" +strmupcFedstrprvalue);
					JSONArray strmupcAvailability=strmupcBuyinfo.getJSONArray("availability");
					JSONObject strmAupcvailability0=strmupcAvailability.getJSONObject(0);
					String strmupcOnlineinv=strmAupcvailability0.getString("onlineinventory");
					System.out.println("strmupcOnlineinv:" +strmupcOnlineinv);
					String strmupcOnline=strmAupcvailability0.getString("online");
					System.out.println("strmupcOnline:" +strmupcOnline); 
					String StrmupcFedfilinv=strmAupcvailability0.getString("fedfilinventory");
					System.out.println("StrmupcFedfilinv:" +StrmupcFedfilinv);
					String strmupcFedfil=strmAupcvailability0.getString("fedfil");
					System.out.println("strmupcFedfil:" +strmupcFedfil);
					Float strmupcOrigFloprvalue = Float.parseFloat(strmupcOrigstrprvalue);
					Float strmupcCurrFloprvalue = Float.parseFloat(strmupcCurrstrprvalue);
					Float strmupcFedFloprvalue = Float.parseFloat(strmupcFedstrprvalue); 
					JSONObject strmupcIteminfo=strmupcProp.getJSONObject("iteminfo");
					JSONArray strmupcShipmsgs=strmupcIteminfo.getJSONArray("shippingmessages");
					JSONObject strmupcShippingmsgs=strmupcShipmsgs.getJSONObject(0);
					String strmupcShippingmsgssuspain=strmupcShippingmsgs.getString("suspain");
					System.out.println("strmupcShippingmsgssuspain:" +strmupcShippingmsgssuspain);
					String strmupcShippingmsgsdeliverytype=strmupcShippingmsgs.getString("deliverytype");
					System.out.println("strmupcShippingmsgsdeliverytype:" +strmupcShippingmsgsdeliverytype);
					String strmupcShippingmsgsgiftwrap=strmupcShippingmsgs.getString("giftwrap");
					System.out.println("strmupcShippingmsgsgiftwrap:" +strmupcShippingmsgsgiftwrap);
					String strmupcShippingmsgsshipdate=strmupcShippingmsgs.getString("shipdate");
					System.out.println("strmupcShippingmsgsshipdate:" +strmupcShippingmsgsshipdate);
					String strmupcShippingmsgsshipdays=strmupcShippingmsgs.getString("shipdays");
					System.out.println("strmupcShippingmsgsshipdays:" +strmupcShippingmsgsshipdays); 
					String strmupcShippingmsgsmethod=strmupcShippingmsgs.getString("method");
					System.out.println("strmupcShippingmsgsmethod:" +strmupcShippingmsgsmethod);
					JSONArray strmupcSwtch=strmupcIteminfo.getJSONArray("swatches");
					JSONObject strmupcSwatches0=strmupcSwtch.getJSONObject(0);
					JSONArray strmupcPdtimage=strmupcSwatches0.getJSONArray("pdtimage");
					JSONObject strmupcPdtimage0=strmupcPdtimage.getJSONObject(0);
					String strmupcPdtaddseqnumber=strmupcPdtimage0.getString("sequencenumber");
					System.out.println("strmupcPdtaddseqnumber:" +strmupcPdtaddseqnumber);
					String strmupcPdtaddname=strmupcPdtimage0.getString("name");
					System.out.println("strmupcPdtaddname:" +strmupcPdtaddname);
					String strmupcPdtaddimage=strmupcPdtimage0.getString("image");     
					System.out.println("strmupcPdtaddimage:" +strmupcPdtaddimage);
					JSONArray strmupcStoreinfo=strmupcProp.getJSONArray("storeinfo");
					System.out.println("strmupcStoreinfo:" +strmupcStoreinfo);
					String WebupcID=driver.findElement(By.xpath(productwedid)).getText();
					System.out.println("WebupcID:" +WebupcID);
					String WebIDupctrim=WebupcID.substring(WebupcID.indexOf("Web ID:")+8);
					System.out.println("WebIDupctrim:" +WebIDupctrim);
					String strmPDPupcIdentifier= StreamUPCJson.getString("identifier");
					System.out.println("strmPDPupcIdentifier:" +strmPDPupcIdentifier);
					if(WebIDupctrim.equals(strmPDPupcIdentifier))
					{
						String logErr262= "Pass:The Product Page Identifier gets matched:\n" +WebIDupctrim+"\n" +strmPDPupcIdentifier; 
						logInfo(logErr262);
					}
					else
					{
						String logErr263= "Fail:The Product Page Identifier doesn't gets matched:\n" +WebIDupctrim+ "\n" +strmPDPupcIdentifier; 
						logInfo(logErr263);
					}
					String productUPC1value= driver.findElement(By.xpath(productUPC)).getText();
					System.out.println("productUPCvalue1:" +productUPC1value);
					String Productupcvalue1trim=productUPC1value.substring(productUPC1value.indexOf("UPC:")+5);
					System.out.println("Productupcvaluetrim1:" +Productupcvalue1trim);  
					if(Productupcvalue1trim.equals(strmupcSkuId))
					{
						String logErr264= "Pass:The Product UPC value gets matched:\n" +Productupcvalue1trim+ "\n" +strmupcSkuId; 
						logInfo(logErr264);
					}
					else
					{
						String logErr265= "Fail:The Product UPC value doesn't gets matched:\n" +Productupcvalue1trim+ "\n" +strmupcSkuId; 
						logInfo(logErr265);
					}
					String productupcpagecolorvalue=driver.findElement(By.xpath(productcolorvalue)).getText();
					System.out.println("productupcpagecolorvalue:" +productupcpagecolorvalue);
					if(productupcpagecolorvalue.equalsIgnoreCase(strmupcSkuColorvalue)) 
					{
						String logErr265= "Pass:The Product Page UPC Color gets matched:\n" +productupcpagecolorvalue+"\n" +strmupcSkuColorvalue; 
						logInfo(logErr265);
					}
					else
					{
						String logErr266= "Fail:The Product Page UPC Color doesn't gets matched:\n" +productupcpagecolorvalue+"\n" +strmupcSkuColorvalue; 
						logInfo(logErr266);
					}
					String productupcpagesizevalue=driver.findElement(By.xpath(productsizeselection)).getText(); 
					System.out.println("productupcpagesizevalue:" +productupcpagesizevalue);
					if(productupcpagesizevalue.equals(strmupcSkusizevalue))
					{
						String logErr267= "Pass:The Product Page UPC Size gets matched:\n" +productupcpagesizevalue+"\n" +strmupcSkusizevalue; 
						logInfo(logErr267);
					}
					else
					{
						String logErr268= "Fail:The Product Page UPC Size doesn't gets matched:\n" +productupcpagesizevalue+"\n" +strmupcSkusizevalue;
						logInfo(logErr268);
					}  
					String saleupc12pricevalueinpdp=driver.findElement(By.xpath(pricesale)).getText();
					System.out.println("saleupc12pricevalueinpdp:" +saleupc12pricevalueinpdp); 
					String priceupcvalueinpdp=driver.findElement(By.xpath(pricedisplayedinpdp)).getText();
					System.out.println("priceupcvalueinpdp:" +priceupcvalueinpdp);
					//Current and original are same
					if(strmupcCurrFloprvalue.compareTo(strmupcOrigFloprvalue)==0 && (strmupcFedfil.equals("true")|| strmupcFedfil.equals("false")))
					{
						String logErr269= "Pass:The UPC Contains Current/Store price:\n" +priceupcvalueinpdp+"\n" +strmupcCurrFloprvalue; 
						logInfo(logErr269);
					}
					// Current/Store<Original
					else if(strmupcCurrFloprvalue.compareTo(strmupcOrigFloprvalue)<0 && (strmupcFedfil.equals("true")|| strmupcFedfil.equals("false")))
					{
						String saleupcpricevalueinpdp=driver.findElement(By.xpath(pricesale)).getText();
						System.out.println("saleupcpricevalueinpdp:" +saleupcpricevalueinpdp); 
						String logErr270= "Pass:The UPC Contains Current/Store in Red Original in Black w/ label:\n" +saleupcpricevalueinpdp+"\n" +strmupcCurrFloprvalue+ "\n" +priceupcvalueinpdp+ "\n" +strmupcOrigFloprvalue; 
						logInfo(logErr270);
					}
					// Current/Store>Original
					else if(strmupcCurrFloprvalue.compareTo(strmupcOrigFloprvalue)>0 && (strmupcFedfil.equals("true")|| strmupcFedfil.equals("false")))
					{
						String logErr271= "Pass:The UPC Contains Current/Store in Black:\n" +priceupcvalueinpdp+"\n" +strmupcCurrFloprvalue; 
						logInfo(logErr271);
					}
					// Original Price is null
					else if((strmupcOrigFloprvalue==0) && (strmupcFedfil.equals("true")|| strmupcFedfil.equals("false")))
					{
						String logErr272= "Pass:The UPC Contains Current/Store in Black:\n" +priceupcvalueinpdp+"\n" +strmupcCurrFloprvalue; 
						logInfo(logErr272);
					}
					// Current price is null and Fedfilavailability price available 
					else if((strmupcCurrFloprvalue==0) && (strmupcFedfil.equals("true")))
					{
						String logErr273= "Pass:The UPC Contains Fedfil price:\n" +priceupcvalueinpdp+"\n" +strmupcFedFloprvalue; 
						logInfo(logErr273);
					}	
					// current price is null and Fedfilavailability price not available
					else if((strmupcCurrFloprvalue==0) && (strmupcFedfil.equals("false")))
					{
						String logErr274= "Pass:The UPC Contains Original price:\n" +priceupcvalueinpdp+"\n" +strmupcOrigFloprvalue; 
						logInfo(logErr274);
					}
					// Current and original price is null and Fedfilavailability price not available
					else if((strmupcCurrFloprvalue==0) && (strmupcOrigFloprvalue==0) && (strmupcFedfil.equals("false")))
					{
						String text11="Price is unavailable, please validate price status in mPOS or POS.";
						String text12="Price is unavailable, please contact an  associate for pricing";
						if(priceupcvalueinpdp.equalsIgnoreCase(text11))
						{
							String logErr275= "Pass:The Product Page contains penny price in Associate Mode:\n" +priceupcvalueinpdp+"\n" +text11; 
							logInfo(logErr275);
						}
						else  
						{
							String logErr276= "Pass:The Product Page contains penny price in Customer Mode:\n" +priceupcvalueinpdp+ "\n" +text12; 
							logInfo(logErr276);
						}
					}
					else if((strmupcCurrFloprvalue==0) && (strmupcFedFloprvalue==0))
					{
						String logErr277= "Pass:The Product Page contains Original Price:\n" +priceupcvalueinpdp+ "\n" +strmupcOrigFloprvalue; 
						logInfo(logErr277);

					}
					//  Current/store price has penny price 
					else if	((strmupcCurrFloprvalue==0.01) && (strmupcFedFloprvalue<=0.10)) 			
					{
						String text13="Price is unavailable, please validate price status in mPOS or POS.";
						String text14="Price is unavailable, please contact an  associate for pricing";
						if(priceupcvalueinpdp.equalsIgnoreCase(text13))
						{
							String logErr278= "Pass:The Product Page contains penny price in Associate Mode:\n" +priceupcvalueinpdp+"\n" +text13; 
							logInfo(logErr278);
						}
						else  
						{
							String logErr279= "Pass:The Product Page contains penny price in Customer Mode:\n" +priceupcvalueinpdp+ "\n" +text14; 
							logInfo(logErr279);
						}
					}  
					if(strmupcOnline.equals("true")&&strmupcFedfil.equals("true"))
					{
						String logErr35= "Pass:The UPC is Available for shipping"; 
						logInfo(logErr35);
						driver.findElement(By.xpath(addtoorder)).click();
						Thread.sleep(3000);	
						String quickviewtitlestring1=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
						System.out.println("quickviewtitlestring1:" +quickviewtitlestring1);
						String quickviewwebidstring1=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
						System.out.println("quickviewwebidstring1:" +quickviewwebidstring1);
						String quickviewwebidstringtrim1=quickviewwebidstring1.substring(quickviewwebidstring1.indexOf("(")+1,quickviewwebidstring1.indexOf(")"));
						System.out.println("quickviewwebidstringtrim1:" +quickviewwebidstringtrim1);
						String quickviewpricestring1=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
						System.out.println("quickviewpricestring1:" +quickviewpricestring1);
						String quickviewcolorstring1=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
						System.out.println("quickviewcolorstring1:" +quickviewcolorstring1);
						String quickviewsizestring1=driver.findElement(By.xpath("(//*[@class='overlaySizeInfoValue'])")).getText();
						System.out.println("quickviewsizestring1:" +quickviewsizestring1);
						String quickviewimageurl1=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
						System.out.println("quickviewimageurl1:" +quickviewimageurl1);
						String quickviewimageurltrim1=quickviewimageurl1.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
						System.out.println("quickviewimageurltrim1:" +quickviewimageurltrim1);	
						if(quickviewtitlestring1.equals(upcPdtName)&&quickviewwebidstringtrim1.equals(WebupcID)&&quickviewpricestring1.equals(priceupcvalueinpdp)&&quickviewcolorstring1.equalsIgnoreCase(productupcpagecolorvalue)&&quickviewsizestring1.equals(productupcpagesizevalue)&&quickviewimageurltrim1.equals(Productupcprimaryimagetrim))
						{
							String logErr280= "Pass:The details in the AddtoBag overlay value gets matched:\n" +quickviewtitlestring1+"\n" +upcPdtName+ "\n" +quickviewwebidstringtrim1+ "\n" +WebupcID+ "\n" +quickviewpricestring1+ "\n" +priceupcvalueinpdp+ "\n" +quickviewcolorstring1+ "\n" +productupcpagecolorvalue+ "\n" +quickviewsizestring1+ "\n" +productupcpagesizevalue+ "\n" +quickviewimageurltrim1+ "\n" +Productupcprimaryimagetrim;
							logInfo(logErr280);  
						}
						else
						{
							String logErr281= "Fail:The details in the AddtoBag overlay value doesn't gets matched:\n" +quickviewtitlestring1+"\n" +upcPdtName+ "\n" +quickviewwebidstringtrim1+ "\n" +WebupcID+ "\n" +quickviewpricestring1+ "\n" +priceupcvalueinpdp+ "\n" +quickviewcolorstring1+ "\n" +productupcpagecolorvalue+ "\n" +quickviewsizestring1+ "\n" +productupcpagesizevalue+ "\n" +quickviewimageurltrim1+ "\n" +Productupcprimaryimagetrim;
							logInfo(logErr281);  
						}

						driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();		
					}
					else
					{
						String logErr282= "Pass:The UPC is not Available for shipping";
						logInfo(logErr282);
					}
					//Check others stores functionalities 
					Thread.sleep(2500);
					driver.findElement(By.xpath("//*[@id='id_otherStores']/span")).click();
					Thread.sleep(5000);
					//Check other Stores stream call data 
					String pdtupc123value = "http://social.macys.com/skavastream/core/v5/macys/product/"+Productupcvalue1trim+"?type=UPCWithNearbyStores&storeid=1&campaignId=383";
					URL url125  = new URL(pdtupc123value);
					String pageSource125  = new Scanner(url125.openConnection().getInputStream()).useDelimiter("\\Z").next();
					System.out.println("pageSource1:" +pageSource125);
					JSONObject StreamPDP1Upccheckotherstores=new JSONObject(pageSource125);
					JSONObject streamPDP1Upcchild=StreamPDP1Upccheckotherstores.getJSONObject("children");
					JSONArray streamPDP1UPCSku=streamPDP1Upcchild.getJSONArray("skus");
					for(int q5=0;q5<streamPDP1UPCSku.length();q5++)
					{
						JSONObject streamPDP1UPCSkuobj=streamPDP1UPCSku.getJSONObject(q5);
						String streamPDP1UPCidentifier= streamPDP1UPCSkuobj.getString("identifier");
						System.out.println("streamPDP1UPCidentifier:" +streamPDP1UPCidentifier);
						if(streamPDP1UPCidentifier.equals(Productupcvalue1trim))
						{
							JSONObject stream1PropUPC=streamPDP1UPCSkuobj.getJSONObject("properties");
							JSONArray stream1PDPStoreinfo=stream1PropUPC.getJSONArray("storeinfo"); 
							int productstorescounts1=driver.findElements(By.xpath("//*[@id='storeContainer']/div/div[1]/div/div[2]")).size(); 
							System.out.println("productstorescounts1:" +productstorescounts1); 
							for(int o5=0,p5=1;o5<stream1PDPStoreinfo.length()||p5<=productstorescounts1;o5++,p5++)
							{
								JSONObject strm1Storeinfo1=stream1PDPStoreinfo.getJSONObject(o5);
								String strm1Storeinfophone=strm1Storeinfo1.getString("phone");
								System.out.println("strm1Storeinfophone:" +strm1Storeinfophone);
								String strm1StoreinfoSequenceno=strm1Storeinfo1.getString("sequencenumber");
								System.out.println("strmStoreinfoSequenceno:" +strm1StoreinfoSequenceno);
								String strm1StoreinfoInventory=strm1Storeinfo1.getString("inventory");
								System.out.println("strm1StoreinfoInventory:" +strm1StoreinfoInventory);
								String strm1StoreinfoName=strm1Storeinfo1.getString("name");
								System.out.println("strm1StoreinfoName:" +strm1StoreinfoName);
								String strm1StoreinfoZipcode=strm1Storeinfo1.getString("zipcode");
								System.out.println("strm1StoreinfoZipcode:" +strm1StoreinfoZipcode);
								String strm1StoreinfoState=strm1Storeinfo1.getString("state");
								System.out.println("strm1StoreinfoState:" +strm1StoreinfoState);
								String strm1StoreinfoAddress1=strm1Storeinfo1.getString("address1");
								System.out.println("strm1StoreinfoAddress1:" +strm1StoreinfoAddress1);
								String strm1StoreinfoAddress2=strm1Storeinfo1.getString("address2");
								System.out.println("strm1StoreinfoAddress2:" +strm1StoreinfoAddress2);
								String strm1StoreinfoIdentifier=strm1Storeinfo1.getString("identifier");
								System.out.println("strm1StoreinfoIdentifier:" +strm1StoreinfoIdentifier);
								String strm1StoreinfoCity=strm1Storeinfo1.getString("city");
								System.out.println("strm1StoreinfoCity:" +strm1StoreinfoCity);
								String strm1StoreinfoNameconcat=strm1StoreinfoName+strm1StoreinfoIdentifier;
								System.out.println("strm1StoreinfoNameconcat:" +strm1StoreinfoNameconcat);
								String strm1storeinfocombined=strm1StoreinfoNameconcat.replaceAll(""+strm1StoreinfoIdentifier+".*","")+" "+"("+strm1StoreinfoIdentifier+")";
								System.out.println("strm1storeinfocombined:" +strm1storeinfocombined);
								String checkavailabilityheadertitle1=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[1]/div/div[2]/div")).getText();
								System.out.println("checkavailabilityheadertitle1:" +checkavailabilityheadertitle1);
								driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[1]/div/div[2]/div")).click();
								Thread.sleep(2000);
								String checkavailabilitystreetNametitle1=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[2]/div/div[1]")).getText();
								System.out.println("checkavailabilitystreetNametitle1:" +checkavailabilitystreetNametitle1);
								String checkavailabilitystoreLocationtitle1=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[2]/div/div[2]")).getText();
								System.out.println("checkavailabilitystoreLocationtitle1:" +checkavailabilitystoreLocationtitle1);
								String checkavailabilitystorePhonetitle1=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[2]/div/div[3]")).getText();
								System.out.println("checkavailabilitystorePhonetitle1:" +checkavailabilitystorePhonetitle1);
								driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[1]/div/div[2]/div")).click();	
								if(checkavailabilityheadertitle1.equals(strm1storeinfocombined)&&checkavailabilitystreetNametitle1.equals(strm1StoreinfoAddress1)&&checkavailabilitystoreLocationtitle1.equals(strm1StoreinfoAddress2)&&checkavailabilitystorePhonetitle1.equals(strm1Storeinfophone))
								{
									String logErr281= "Pass:The Product Page UPC Checkother Stores value gets matched:\n" +checkavailabilityheadertitle1+"\n" +strm1storeinfocombined+ "\n" +checkavailabilitystreetNametitle1+ "\n" +strm1StoreinfoAddress1+ "\n" +checkavailabilitystoreLocationtitle1+ "\n" +strm1StoreinfoAddress2+ "\n" +checkavailabilitystorePhonetitle1+ "\n" +strm1Storeinfophone;  
									logInfo(logErr281);
								}	
								else
								{
									String logErr282= "Fail:The Product Page UPC Checkother Stores value doesn't gets matched:\n" +checkavailabilityheadertitle1+"\n" +strm1storeinfocombined+ "\n" +checkavailabilitystreetNametitle1+ "\n" +strm1StoreinfoAddress1+ "\n" +checkavailabilitystoreLocationtitle1+ "\n" +strm1StoreinfoAddress2+ "\n" +checkavailabilitystorePhonetitle1+ "\n" +strm1Storeinfophone;
									logInfo(logErr282);
								}
								Thread.sleep(5000);
								String logErr283= "-----------------------------------------------------------------------------";
								logInfo(logErr283);
								System.out.println("-----------------------------------------");  
							} 
							driver.findElement(By.xpath("//*[@id='id_pdpSeeAllAvailable']/div/div[4]")).click();    
						}
					}
				}

			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}

	}
	private static void shoppingBagValidation() 
	{
		try
		{
			try
			{
				//Shopping Bag Functionalities 		
				String logErr307 ="Shopping Bag Page Functionalities"; 
				logInfo(logErr307);
				String logErr308="----------------------------------"; 
				logInfo(logErr308);
				driver.findElement(By.xpath("(//*[@class='cls_mamFMyBagCont'])")).click();	
				Thread.sleep(8000);
				//Checking the Shopping Bag Status 
				String shoppingbagfootericoncount=driver.findElement(By.xpath("//*[@id='id_skrlMyBagCount']")).getText();
				System.out.println("shoppingbagfootericoncount:" +shoppingbagfootericoncount);
				int shoppingbagtotalproductcount=driver.findElements(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div")).size();
				System.out.println("shoppingbagtotalproductcount:" +shoppingbagtotalproductcount);
				//16.Verify that  added items to the bag should be displayed in the "Order bag" page
				String logErrsmp16 ="16,Verify that  added items to the bag should be displayed in the Order bag page, Pass"; 
				logInfo1(logErrsmp16);	
				for(int c4=1;c4<=shoppingbagtotalproductcount;c4++)
				{
					String producupccodevalue=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]")).getAttribute("upccode");
					System.out.println("producupccodevalue:" +producupccodevalue);
					String shoppingbagproductname=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[1]")).getText();
					System.out.println("shoppingbagproductname:" +shoppingbagproductname); 
					String shoppingbagproductcolors=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[2]")).getText();
					System.out.println("shoppingbagproductcolors:" +shoppingbagproductcolors);
					//Color name label
					//String shoppingbagproductcolorsname=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[2]/text()")).getText();
					//System.out.println("shoppingbagproductcolorsname:" +shoppingbagproductcolorsname);
					String shoppingbagproductsize=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[3]")).getText();
					System.out.println("shoppingbagproductsize:" +shoppingbagproductsize);
					//Size name label
					//String shoppingbagproductsizetext=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[3]/text()")).getText();
					//System.out.println("shoppingbagproductsizetext:" +shoppingbagproductsize);
					String shoppingbagproductprice=(driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[2]/div"))).getText();
					System.out.println("shoppingbagproductprice:" +shoppingbagproductprice);  
					String shoppingbagproductpricetrimmed=shoppingbagproductprice.replaceAll(".*\\$","");
					System.out.println("shoppingbagproductpricetrimmed: " +shoppingbagproductpricetrimmed);
					float shoppingbagproductpricefloat=Float.parseFloat(shoppingbagproductpricetrimmed);    
					System.out.println("shoppingbagproductpricefloat:" +shoppingbagproductpricefloat);       
					String shoppingbagupcqtyvalues=(driver.findElement(By.xpath("(//*[@class='skrlCustomComboTxt'])["+c4+"]"))).getText();
					System.out.println("shoppingbagupcqtyvalues:" +shoppingbagupcqtyvalues);
					int shoppingbagupcqtyvaluesint=Integer.parseInt(shoppingbagupcqtyvalues);
					System.out.println("shoppingbagupcqtyvaluesint:" +shoppingbagupcqtyvaluesint);
					float totalupcproductvalues= shoppingbagproductpricefloat * shoppingbagupcqtyvaluesint; 
					String shoppingbagpageproducttotalprice=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[4]/div[1]")).getText();
					System.out.println("shoppingbagpageproducttotalprice:" +shoppingbagpageproducttotalprice);
					String shoppingbagpageproducttotalpricetrimmed=shoppingbagpageproducttotalprice.replaceAll(".*\\$","");
					System.out.println("shoppingbagpageproducttotalpricetrimmed: " +shoppingbagpageproducttotalpricetrimmed);
					float shoppingbagpageproducttotalpricefloat=Float.parseFloat(shoppingbagpageproducttotalpricetrimmed);    
					System.out.println("shoppingbagpageproducttotalpricefloat:" +shoppingbagpageproducttotalpricefloat);
					if(totalupcproductvalues==shoppingbagpageproducttotalpricefloat)
					{
						String logErr160= "Pass:The Productdetails,Price,Qty and Totalprice values gets matched in the shopping bag:\n" +producupccodevalue+ "\n" +shoppingbagproductname+ "\n"  +shoppingbagproductcolors+  "\n"   +shoppingbagproductsize+ "\n"  +shoppingbagproductprice+ "\n" +shoppingbagupcqtyvalues+ "\n" +shoppingbagpageproducttotalprice;
						logInfo(logErr160);	 
						String logErr162= "------------------------------------------------------------------------";
						logInfo(logErr162);
					}
					else
					{
						String logErr161= "Fail:The Productdetails,Price,Qty and Totalprice values doesn't gets matched in the shopping bag:\n" +producupccodevalue+ "\n" +shoppingbagproductname+ "\n"  +shoppingbagproductcolors+  "\n"   +shoppingbagproductsize+ "\n"  +shoppingbagproductprice+ "\n" +shoppingbagupcqtyvalues+ "\n" +shoppingbagpageproducttotalprice;   
						logInfo(logErr161);
						String logErr163= "------------------------------------------------------------------------";
						logInfo(logErr163);
					}
					String shoppingbagproductstotalpricevalues=driver.findElement(By.xpath("//*[@id='id_skrlSubTotalAmount']")).getText(); 
					System.out.println("shoppingbagproductstotalpricevalues: " +shoppingbagproductstotalpricevalues); 
					//Shopping bag suspend
					driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div/div[4]/div[2]")).click();
					Thread.sleep(500);
					//17.Verify that while selecting the ""Clear All Items & Remove Items" in the shopping bag page, the item should be removed from the shopping bag page 
					String logErrsmp17 ="17,Verify that while selecting the Clear All Items & Remove Items in the shopping bag page the item should be removed from the shopping bag page, Pass"; 
					logInfo1(logErrsmp17);
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer div.startNewOrderDiv.suspendwaringCancelBtn div.suspendOverlayConfrimBtns.suspendwaringCancelBtnImg div.suspendBtnText")).click();   
					Thread.sleep(1000);
					driver.findElement(By.xpath("(//*[@class='listCheckOutBtn handlingPressState'])")).click();
					//driver.findElement(By.xpath("//*[@id='id_skrlSusspendBtn']")).click();
					Thread.sleep(4000);
					//18.Verify that  while selecting the Suspend /Check out option, the associate login page should be displayed if the user doesn't gets logged-in before
					//String logErrsmp18 ="18,Verify that  while selecting the Suspend /Check out option the associate login page should be displayed if the user doesn't gets logged-in before, Pass"; 
					//logInfo1(logErrsmp18);
					driver.findElement(By.xpath("//*[@id='id_loginForm_AID']")).sendKeys("71253659");			
					driver.findElement(By.xpath("//*[@id='id_loginForm_PIN']")).sendKeys("0947");
					driver.findElement(By.xpath("(//*[@class='cls_mamSignin'])")).click();   
					Thread.sleep(5000);	
					//19.verify that while selecting the Suspend /Check out option, the Confimation popup should be enabled when the user is alreeady logged-in
					String logErrsmp19 ="19,verify that while selecting the Suspend/Check out option the Confimation popup should be enabled when the user is alreeady logged-in, Pass"; 
					logInfo1(logErrsmp19);
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer.skrlSuspendSwitchUserBtn div.startNewOrderDiv.suspendwaringCancelBtn div.suspendOverlayConfrimBtns.suspendwaringCancelBtnImg div.suspendBtnText")).click();
					Thread.sleep(2000);	
					//Apply Coupon Functionalities   
					driver.findElement(By.xpath("(//*[@class='listCouponText'])")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@id='id_favPromoScanTxtInput']")).click();
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='id_favPromoScanTxtInput']")).sendKeys("00000000000315040005");
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='id_favPromoSubmit']")).click();
					Thread.sleep(60000);
					driver.findElement(By.xpath("//*[@id='id_errorPopUpContainer']")).isDisplayed();
					String applycouponerror=driver.findElement(By.xpath("//*[@id='id_errorPopUpContainer']/div[2]")).getText();
					System.out.println("applycouponerror:" +applycouponerror);
					//22.Verify that "Apply Coupon"  functionalities should be worked as expected 
					String logErrsmp22 ="22,Verify that Apply Coupon functionalities should be worked as expected,Fail\n" +applycouponerror;
					logInfo1(logErrsmp22);
					Thread.sleep(1000);
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_29.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#Shopping_Bag_Page.page.lyt_cont_div div#id_myBagContent.cls_mamMyBagPage div.skrlorderPageKisok div#id_errorPopUpContainer.errorPopUpContainer div.errorPopupOkBtn")).click();
					Thread.sleep(3000);	
					driver.findElement(By.xpath("(//*[@class='listSuspendBtn handlingPressState'])")).click();  
					Thread.sleep(2000);
					//Switch User Account
					//driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer.skrlSuspendSwitchUserBtn div.startNewOrderDiv.suspendSwitchUserBtn div.suspendOverlayConfrimBtns.suspendSwitchUserBtnImg")).click();
					//Thread.sleep(1000);	
					//driver.findElement(By.xpath("//*[@id='id_loginForm_AID']")).sendKeys("71253659");
					//driver.findElement(By.xpath("//*[@id='id_loginForm_PIN']")).sendKeys("0947");
					//driver.findElement(By.xpath("//*[@id='id_mamLoginForm']/div/div[2]/div/div[3]/div[2]")).click();
					//Thread.sleep(1000); 
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer.skrlSuspendSwitchUserBtn div.startNewOrderDiv.suspendWaringOkBtn div.startNewOrderImg.suspendWaringOkBtnImg")).click();				
					Thread.sleep(10000);  
					if(driver.findElement(By.className("sksuspendOverlayContainer")).isDisplayed())
					{
						System.out.println("Yes");	

					}
					else
					{
						System.out.println("Nooo");

					}
					//20.verify that while selecting the "Continue" button in the Suspend confirmation popup, the "Suspend Transaction/Suspend Failed" overlay  should  be enabled
					String logErrsmp20 ="20,verify that while selecting the Suspend/Check out option the Confimation popup should be enabled when the user is alreeady logged-in, Pass"; 
					logInfo1(logErrsmp20);
					String suspendresponsedetails=driver.findElement(By.className("suspendOverlayHeadingDiv")).getText();				
					System.out.println("suspendresponsedetails:" +suspendresponsedetails);	
					Thread.sleep(5000);
					if(suspendresponsedetails.equals("TRANSACTION SUSPENDED"))
					{
						String suspendresponsedetailss=driver.findElement(By.className("suspendOverlayHeadingDiv")).getText();				
						System.out.println("suspendresponsedetailss:" +suspendresponsedetailss);	
						String suspendTransactionCode=driver.findElement(By.className("suspendTransactionCodeDiv")).getText();
						System.out.println("suspendTransactionCode:" +suspendTransactionCode);
						String suspendTransactionSubtotalvalue=driver.findElement(By.className("suspendTransactionSubtotalDiv")).getText();
						System.out.println("suspendTransactionSubtotalvalue:" +suspendTransactionSubtotalvalue);
						String suspendTransactionMSGdetails=driver.findElement(By.className("suspendTransactionMsgDiv")).getText();
						System.out.println("suspendTransactionMSGdetails:" +suspendTransactionMSGdetails);
						Thread.sleep(1000);
						driver.findElement(By.cssSelector(".suspendBtnText.startNewOrderText")).click();			
						String logErr165= "Pass:The Products gets Suspended Successfully:\n" +suspendresponsedetailss+ "\n" +suspendTransactionCode+ "\n" +suspendTransactionSubtotalvalue+ "\n" +suspendTransactionMSGdetails;    
						logInfo(logErr165);
					}
					else
					{ 
						String suspendfailuredataerror=driver.findElement(By.className("suspendOverlayHeadingDiv")).getText();
						System.out.println("suspendfailuredataerror:" +suspendfailuredataerror);
						String suspendfailuretranserror=driver.findElement(By.className("suspendTransactionMsgDiv")).getText();
						System.out.println("suspendfailuretranserror:" +suspendfailuretranserror);
						Thread.sleep(1000);
						driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer div.startNewOrderDiv.suspendErrorOkBtn div.startNewOrderImg.suspendErrorOkBtnImg div.suspendBtnText")).click();
						String logErr164= "Fail:The Product Suspend Transaction Gets Failed:\n" +suspendfailuredataerror+ "\n" +suspendfailuretranserror;    
						logInfo(logErr164);			
					}   
				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void settingPageValidation() {
		// TODO Auto-generated method stub
		try{

			//Settings Page Functionalities
			String logErr903 ="Settings Page Functionalities"; 
			logInfo(logErr903);
			String logErr904="----------------------------------"; 
			logInfo(logErr904);
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[9]/div/div[1]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[7]/div[1]")).click();
			Thread.sleep(1000);
			//29.Verify that  selecting the �Settings� option from the Menu section, the settings page should be displayed
			String logErrsmp29="29,Verify that  selecting the Settings option from the Menu section the settings page should be displayed, Pass"; 
			logInfo1(logErrsmp29);
			String settingsheader=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[1]")).getText();
			System.out.println("settingsheader:" +settingsheader);
			String settingstoreid=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[2]/div[1]")).getText();
			System.out.println("settingsheader:" +settingstoreid);
			String settingsfob=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[1]")).getText();
			System.out.println("settingsfob:" +settingsfob);
			String settingssellingarea=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[2]")).getText();
			System.out.println("settingssellingarea:" +settingssellingarea);
			String settingscheckout=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[3]")).getText();
			System.out.println("settingscheckout:" +settingscheckout);
			String settingsassistedcheckout=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[4]")).getText();
			System.out.println("settingsassistedcheckout:" +settingsassistedcheckout);
			String settingssearchsuggestion=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[5]")).getText();
			System.out.println("settingssearchsuggestion:" +settingssearchsuggestion);
			String settingsdeviceid=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[2]/div[2]")).getText();
			System.out.println("settingsdeviceid:" +settingsdeviceid);		
			String settingscatalogname=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[1]")).getText();
			System.out.println("settingscatalogname:" +settingscatalogname);
			String settingsserviceversion=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[2]")).getText();
			System.out.println("settingsserviceversion:" +settingsserviceversion);		
			String settingsappversion=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[3]")).getText();
			System.out.println("settingsappversion:" +settingsappversion);
			String settingsenvironment=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[4]")).getText();
			System.out.println("settingsenvironment:" +settingsenvironment);
			String logErr450 ="Settings Page:" +settingsheader+ "\n" +settingstoreid+ "\n"  +settingsfob+ "\n"  +settingssellingarea+ "\n"  +settingscheckout+ "\n" +settingsassistedcheckout+ "\n"  +settingssearchsuggestion+ "\n" +settingssearchsuggestion+ "\n" +settingsdeviceid+ "\n" +settingscatalogname+ "\n" +settingsserviceversion+ "\n"  +settingsappversion+ "\n" +settingsenvironment;
			logInfo(logErr450);				
			//30.Verify that  �Settings� page should contain �Store Id,Device ID,FOB, Catalog Name,Selling Area,Checkout,ServiceVersion, Assisted Checkout, AppVersion, Search Suggestion and Environment
			String logErrsmp30 ="30,Verify that  �Settings� page should contain Store Id Device ID FOB  Catalog Name Selling Area Checkout ServiceVersion  Assisted Checkout AppVersion Search Suggestion and Environment, Pass"; 
			logInfo1(logErrsmp30);
			String settingsassistedcheckouttri=settingsassistedcheckout.replaceAll(".*\\:","");
			System.out.println("settingsassistedcheckouttri: " +settingsassistedcheckouttri);
			String settingsassistedcheckouttrim=settingsassistedcheckouttri.trim();
			if(settingsassistedcheckouttrim.equals("OFF") && driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[5]/div")).isDisplayed())
			{
				//23.Verify that "Checkout" & "Guest Checkout" functionalities should be as expected as per the truth table 
				String logErrsmp23 ="23,Verify that Checkout & Guest Checkout functionalities should be as expected as per the truth table, Pass"; 
				logInfo1(logErrsmp23);
			}
			else
			{
				//23.Verify that "Checkout" & "Guest Checkout" functionalities should be as expected as per the truth table
				String logErrsmf23 ="23,Verify that Checkout & Guest Checkout functionalities should be as expected as per the truth table, Fail"; 
				logInfo1(logErrsmf23);
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void searchByWebID() 
	{
		try
		{
			try
			{
				//Search By WebId
				String logErr309 ="Search By WebID"; 
				logInfo(logErr309);
				String logErr310="------------------"; 
				logInfo(logErr310);
				driver.findElement(By.xpath("(//*[@class='cls_mamFMenu'])")).click();	
				driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[2]/div[1]")).click();	
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("2155165"); 
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
				Thread.sleep(4000);
				String currentURL = driver.getCurrentUrl();
				System.out.println("currentURL:" +currentURL);  
				String pdtwebId=currentURL.substring(currentURL.indexOf("pdt_id=")+7,currentURL.indexOf("&type"));
				System.out.println("pdtwebId:" +pdtwebId);
				String pdtUrl22 = "http://social.macys.com/skavastream/core/v5/macys/product/"+pdtwebId+"?type=ID&storeid=1&campaignId=383";
				URL url22  = new URL(pdtUrl22);
				String pageSource22  = new Scanner(url22.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource22:" +pageSource22);   
				if(pageSource22.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr600 = "Fail:The Product Page doesn't gets displayed:\n" +pageSource22;
					logInfo(logErr600);
				}
				else
				{	 
					JSONObject Stream1PDPJson = new JSONObject(pageSource22);
					System.out.println("Stream1PDPJson:" +Stream1PDPJson);	
					String strm1PDPName=Stream1PDPJson.getString("name");
					System.out.println("strm1PDPName:" +strm1PDPName);
					String Pdt1Name=driver.findElement(By.xpath(ProductName)).getText();
					System.out.println("Pdt1Name:" +Pdt1Name);
					if(Pdt1Name.equals(strm1PDPName))
					{
						String logErr601 = "Pass:The Product Name gets matched with the stream call \nProduct name: " +Pdt1Name+ "\nStreamCall Response:" +strm1PDPName; 
						logInfo(logErr601);
					}
					else
					{
						String logErr602= "Fail:The Product Name doesn't gets matched with the stream call \n Product name: " +Pdt1Name+ "\nStreamCall Response:" +strm1PDPName; 
						logInfo(logErr602);
					} 
					String Pdt1Descriptiontitle=driver.findElement(By.xpath(ProductDescriptionHeader)).getText();
					System.out.println("Pdt1Descriptiontitle:" +Pdt1Descriptiontitle);
					String Pdt1Descriptiontitletrim=Pdt1Descriptiontitle.trim();
					System.out.println("Pdt1Descriptiontitletrim:" +Pdt1Descriptiontitletrim);
					JSONObject strm1iteminfo=Stream1PDPJson.getJSONObject("properties").getJSONObject("iteminfo");
					JSONObject strm1Desc=strm1iteminfo.getJSONArray("description").getJSONObject(0);
					String strm1Descriptiontitle=strm1Desc.getString("value");
					System.out.println("strm1Descriptiontitle:" +strm1Descriptiontitle);
					if(Pdt1Descriptiontitletrim.equals(strm1Descriptiontitle))
					{
						String logErr603= "Pass:The Product Description gets matched:\n" +Pdt1Descriptiontitletrim+ "\n" +strm1Descriptiontitle; 
						logInfo(logErr603);
					}
					else
					{
						String logErr604= "Fail:The Product Description doesn't gets matched:\n" +Pdt1Descriptiontitletrim+ "\n" +strm1Descriptiontitle; 
						logInfo(logErr604);
					}

					JSONArray strm1Bultdes=strm1iteminfo.getJSONArray("bulletdescription");
					int Product1bulletlength = driver.findElements(By.xpath(Productdesclength)).size();
					System.out.println("Product1bulletlength:" +Product1bulletlength);	    	
					for(int k5=0,l5=1;k5<strm1Bultdes.length()||l5<=Product1bulletlength;k5++,l5++)
					{
						JSONObject strm1Bultdesc=strm1Bultdes.getJSONObject(k5);
						String	strm1Bulletvalue=strm1Bultdesc.getString("value");
						System.out.println("strm1Bulletvalue:\n" +strm1Bulletvalue);
						String product1bulletsdescription = driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[3]/div["+l5+"]/div[2]")).getAttribute("innerHTML");
						System.out.println("product1bulletsdescription:\n" +product1bulletsdescription);
						if(product1bulletsdescription.equals(strm1Bulletvalue))
						{
							String logErr605 = "Pass:The ProductDescription Bulletins gets matched:\n" +product1bulletsdescription+ "\n" +strm1Bulletvalue; 
							logInfo(logErr605);
						}
						else
						{
							String logErr606= "Fail:The ProductDescription Bulletins doesn't gets matched:\n" +product1bulletsdescription+ "\n"+strm1Bulletvalue;
							logInfo(logErr606);
						} 
					}			
					String Product1detailsnametitle=driver.findElement(By.xpath(ProductDetailsHeader)).getText();
					System.out.println("Product1detailsname:" +Product1detailsnametitle);
					String Product1descriptiontitle=driver.findElement(By.xpath(productdescription)).getText();
					System.out.println("Product1descriptiontitle:" +Product1descriptiontitle);
					if(Product1detailsnametitle.equals("Product Details")&&Product1descriptiontitle.equals("Product Description"))
					{
						String logErr607= "Pass:The Product Page Titles gets matched:\n" +Product1detailsnametitle+ "\n" +Product1descriptiontitle;  
						logInfo(logErr607);
					}
					else
					{
						String logErr608 = "Fail:The Product Page Titles doesn't gets matched:\n" +Product1detailsnametitle+ "\n" +Product1descriptiontitle;
						logInfo(logErr608);
					}
					//Product additional images 
					driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_0']")).click();  
					String Product1primaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
					System.out.println("Product1primaryimageurl:" +Product1primaryimageurl);
					String Product1primaryimagetrim=Product1primaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					System.out.println("Prroduct1primaryimagetrim:" +Product1primaryimagetrim);
					String strm1PDPImage=Stream1PDPJson.getString("image");
					System.out.println("strm1PDPImage:" +strm1PDPImage);
					if(Product1primaryimagetrim.equals(strm1PDPImage))
					{
						String logErr609= "Pass:The Product Page Primary Image gets matched:\n" +Product1primaryimagetrim+ "\n" +strm1PDPImage; 
						logInfo(logErr609);
					}
					else
					{
						String logErr610= "Fail:The Product Page Primary Image doesn't gets matched:\n" +Product1primaryimagetrim+ "\n" +strm1PDPImage; 
						logInfo(logErr610);
					}
					driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click(); 
					//int product1additionalimagecount=driver.findElements(By.xpath("//*[@id='id_styleImageDiv']/div/div")).size();  
					//System.out.println("product1additionalimagecount:" +product1additionalimagecount);
					//JSONArray strm1additionalimages= strm1iteminfo.getJSONArray("additionalimages");
					//for(int a5=0,b5=2;a5<strm1additionalimages.length()||b5<=product1additionalimagecount;a5++,b5++)
					//{
					//JSONObject strm1additionalobject=strm1additionalimages.getJSONObject(a5);
					//String stream1additionalimageurl=strm1additionalobject.getString("image");
					//System.out.println("stream1additionalimageurl:" +stream1additionalimageurl);  
					//driver.findElement(By.xpath("//*[@id='id_styleImageDiv']/div/div["+b5+"]")).click();
					//System.out.println("b5:" +b5);
					//int c5=b5-1;
					//driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_"+c5+"']")).click();
					//String Product1secondaryimageurlzoom=driver.findElement(By.xpath("//*[@id='id_skImageScroller_"+c5+"']/img")).getAttribute("src");
					//System.out.println("Product1secondaryimageurlzoom:" +Product1secondaryimageurlzoom);
					//String Product1secondaryimageurlzoomtrim=Product1secondaryimageurlzoom.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					//System.out.println("Product1secondaryimageurlzoomtrim:" +Product1secondaryimageurlzoomtrim); 
					//if(Product1secondaryimageurlzoomtrim.equals(stream1additionalimageurl))
					//{
					//String logErr611= "Pass:The Product Page Additional Image gets matched:\n" +Product1secondaryimageurlzoomtrim+ "\n" +stream1additionalimageurl; 
					//logInfo(logErr611);
					//}
					//else
					//{
					//String logErr612= "Fail:The Product Page Additional Image gets matched:\n" +Product1secondaryimageurlzoomtrim+ "\n" +stream1additionalimageurl; 
					//logInfo(logErr612);
					//}    
					//driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click();
					//}
					int product1colorcountno=driver.findElements(By.xpath(productcolorcount)).size();
					System.out.println("product1colorcountno:" +product1colorcountno);
					int product1skucount=driver.findElements(By.xpath(productskusize)).size(); 
					System.out.println("product1skucount:" +product1skucount);
					int total1skucount= product1colorcountno*product1skucount;
					System.out.println("total1skucount:" +total1skucount);
					String product1skusizedetailss=driver.findElement(By.xpath(productskusizedetail)).getAttribute("sizename");
					System.out.println("product1skusizedetailss:" +product1skusizedetailss);
					//Stream Comparison 
					JSONObject strm1Child=Stream1PDPJson.getJSONObject("children");
					JSONArray strm1Sku=strm1Child.getJSONArray("skus");
					for(int m5=0,n5=1;m5<strm1Sku.length()||n5<=total1skucount;m5++,n5++)
					{
						JSONObject strm1Skuobj=strm1Sku.getJSONObject(m5);
						JSONObject strm1Prop=strm1Skuobj.getJSONObject("properties");
						String  strm1SkuId=strm1Skuobj.getString("identifier");
						System.out.println("strm1SkuId:" +strm1SkuId);
						JSONObject strm1Propr=strm1Prop.getJSONObject("orderinfo");
						String  strm1Proporder=strm1Propr.getString("ordertype");
						System.out.println("strm1Proporder:" +strm1Proporder);
						JSONObject strm1Skuinfo=strm1Prop.getJSONObject("skuinfo");
						JSONObject strm1SkuColor=strm1Skuinfo.getJSONObject("color");
						String  strm1SkuColorvalue=strm1SkuColor.getString("value");
						System.out.println("strm1SkuColorvalue:" +strm1SkuColorvalue);
						String strm1SkuColorlabel=strm1SkuColor.getString("label");
						System.out.println("strm1SkuColorlabel:" +strm1SkuColorlabel);
						JSONObject strm1SkuSize=strm1Skuinfo.getJSONObject("size1");  
						String  strm1Skusizevalue=strm1SkuSize.getString("value");
						System.out.println("strm1Skusizevalue:" +strm1Skusizevalue);
						String  strm1Skusizelabel=strm1SkuSize.getString("label");  
						System.out.println("strm1Skusizelabel:" +strm1Skusizelabel);
						JSONObject strm1Buyinfo=strm1Prop.getJSONObject("buyinfo");
						JSONObject strm1Pricing=strm1Buyinfo.getJSONObject("pricing");
						String  strm1Qtylimit=strm1Buyinfo.getString("ropisqtylimit");
						System.out.println("strm1Qtylimit:" +strm1Qtylimit);
						JSONArray strm1Prices=strm1Pricing.getJSONArray("prices");
						JSONObject strm1OrstrPrice=strm1Prices.getJSONObject(0);
						String  strm1Origstrprvalue=strm1OrstrPrice.getString("value");
						System.out.println("strm1Origstrprvalue:" +strm1Origstrprvalue);
						JSONObject strm1CurrstrPrice=strm1Prices.getJSONObject(1);
						String strm1Currstrprvalue=strm1CurrstrPrice.getString("value");
						System.out.println("strm1Currstrprvalue:" +strm1Currstrprvalue);
						JSONObject strm1FedstrPrice=strm1Prices.getJSONObject(2);
						String strm1Fedstrprvalue=strm1FedstrPrice.getString("value");
						System.out.println("strm1Fedstrprvalue:" +strm1Fedstrprvalue);
						JSONArray strm1Availability=strm1Buyinfo.getJSONArray("availability");
						JSONObject strm1Availability0=strm1Availability.getJSONObject(0);
						String strm1Onlineinv=strm1Availability0.getString("onlineinventory");
						System.out.println("strm1Onlineinv:" +strm1Onlineinv);
						String strm1Online=strm1Availability0.getString("online");
						System.out.println("strm1Online:" +strm1Online); 
						String Strm1Fedfilinv=strm1Availability0.getString("fedfilinventory");
						System.out.println("Strm1Fedfilinv:" +Strm1Fedfilinv);
						String strm1Fedfil=strm1Availability0.getString("fedfil");
						System.out.println("strm1Fedfil:" +strm1Fedfil);
						Float strm1OrigFloprvalue = Float.parseFloat(strm1Origstrprvalue);
						Float strm1CurrFloprvalue = Float.parseFloat(strm1Currstrprvalue);
						Float strm1FedFloprvalue = Float.parseFloat(strm1Fedstrprvalue); 
						JSONObject strm1Iteminfo=strm1Prop.getJSONObject("iteminfo");
						JSONArray strm1Shipmsgs=strm1Iteminfo.getJSONArray("shippingmessages");
						JSONObject strm1Shippingmsgs=strm1Shipmsgs.getJSONObject(0);
						String strm1Shippingmsgssuspain=strm1Shippingmsgs.getString("suspain");
						System.out.println("strm1Shippingmsgssuspain:" +strm1Shippingmsgssuspain);
						String strm1Shippingmsgsdeliverytype=strm1Shippingmsgs.getString("deliverytype");
						System.out.println("strm1Shippingmsgsdeliverytype:" +strm1Shippingmsgsdeliverytype);
						String strm1Shippingmsgsgiftwrap=strm1Shippingmsgs.getString("giftwrap");
						System.out.println("strm1Shippingmsgsgiftwrap:" +strm1Shippingmsgsgiftwrap);
						String strm1Shippingmsgsshipdate=strm1Shippingmsgs.getString("shipdate");
						System.out.println("strm1Shippingmsgsshipdate:" +strm1Shippingmsgsshipdate);
						String strm1Shippingmsgsshipdays=strm1Shippingmsgs.getString("shipdays");
						System.out.println("strm1Shippingmsgsshipdays:" +strm1Shippingmsgsshipdays); 
						String strm1Shippingmsgsmethod=strm1Shippingmsgs.getString("method");
						System.out.println("strm1Shippingmsgsmethod:" +strm1Shippingmsgsmethod);					
						JSONArray strm1Swtch=strm1Iteminfo.getJSONArray("swatches");
						JSONObject strm1Swatches0=strm1Swtch.getJSONObject(0);
						JSONArray strm1Pdtimage=strm1Swatches0.getJSONArray("pdtimage");
						JSONObject strm1Pdtimage0=strm1Pdtimage.getJSONObject(0);
						String strm1Pdtaddseqnumber=strm1Swatches0.getString("sequencenumber");
						System.out.println("strm1Pdtaddseqnumber:" +strm1Pdtaddseqnumber);
						String strm1Pdtaddname=strm1Swatches0.getString("name");
						System.out.println("strm1Pdtaddname:" +strm1Pdtaddname);
						//String strm1Pdtaddimage=strm1Swatches0.getString("image");     
						//System.out.println("strm1Pdtaddimage:" +strm1Pdtaddimage);
						JSONArray strm1Storeinfo=strm1Prop.getJSONArray("storeinfo");
						System.out.println("strm1Storeinfo:" +strm1Storeinfo);
						driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[2]/div["+n5+"]")).click();
						//driver.findElement(By.xpath(productsizeselection)).click(); 
						Thread.sleep(6000);
						//driver.findElement(By.xpath("//*[@id='id_"+strm1Skusizevalue+"']")).click();
						Thread.sleep(3000);
						String Web1ID=driver.findElement(By.xpath(productwedid)).getText();
						System.out.println("Web1ID:" +Web1ID);
						String Web1IDtrim=Web1ID.substring(Web1ID.indexOf("Web ID:")+8);
						System.out.println("Web1IDtrim:" +Web1IDtrim);
						String strm1PDPIdentifier= Stream1PDPJson.getString("identifier");
						System.out.println("strm1PDPIdentifier:" +strm1PDPIdentifier);
						if(Web1IDtrim.equals(strm1PDPIdentifier))
						{
							String logErr613= "Pass:The Product Page Identifier gets matched:\n" +Web1IDtrim+"\n" +strm1PDPIdentifier; 
							logInfo(logErr613);
						}
						else
						{
							String logErr614= "Fail:The Product Page Identifier doesn't gets matched:\n" +Web1IDtrim+ "\n" +strm1PDPIdentifier; 
							logInfo(logErr614);
						}
						String product1UPCvalue= driver.findElement(By.xpath(productUPC)).getText();
						System.out.println("product1UPCvalue:" +product1UPCvalue);
						String Product1upcvaluetrim=product1UPCvalue.substring(product1UPCvalue.indexOf("UPC:")+5);
						System.out.println("Product1upcvaluetrim:" +Product1upcvaluetrim);  
						if(Product1upcvaluetrim.equals(strm1SkuId))
						{
							String logErr615= "Pass:The Product UPC value gets matched:\n" +Product1upcvaluetrim+ "\n" +strm1SkuId; 
							logInfo(logErr615);
						}
						else
						{
							String logErr616= "Fail:The Product UPC value doesn't gets matched:\n" +Product1upcvaluetrim+ "\n" +strm1SkuId; 
							logInfo(logErr616);
						}
						String product1pagecolorvalue=driver.findElement(By.xpath(productcolorvalue)).getText();
						System.out.println("product1pagecolorvalue:" +product1pagecolorvalue);
						if(product1pagecolorvalue.equalsIgnoreCase(strm1SkuColorvalue)); 
						{
							String logErr617= "Pass:The Product Page UPC Color gets matched:\n" +product1pagecolorvalue+"\n" +strm1SkuColorvalue; 
							logInfo(logErr617);
						}
						String product1pagesizevalue=driver.findElement(By.xpath(productsizeselection)).getText(); 
						System.out.println("product1pagesizevalue:" +product1pagesizevalue);
						if(product1pagesizevalue.equals(strm1Skusizevalue))
						{
							String logErr618= "Pass:The Product Page UPC Size gets matched:\n" +product1pagesizevalue+"\n" +strm1Skusizevalue; 
							logInfo(logErr618);
						}
						else
						{
							String logErr619= "Fail:The Product Page UPC Size doesn't gets matched:\n" +product1pagesizevalue+"\n" +strm1Skusizevalue;
							logInfo(logErr619);
						}  
						String price1valueinpdp=driver.findElement(By.xpath(pricedisplayedinpdp)).getText();
						System.out.println("price1valueinpdp:" +price1valueinpdp);
						//Current and original are same
						if(strm1CurrFloprvalue.compareTo(strm1OrigFloprvalue)==0 && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
						{
							String logErr620= "Pass:The UPC Contains Current/Store price:\n" +price1valueinpdp+"\n" +strm1CurrFloprvalue; 
							logInfo(logErr620);
						}
						// Current/Store<Original
						else if(strm1CurrFloprvalue.compareTo(strm1OrigFloprvalue)<0 && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
						{
							String sale1pricevalueinpdp=driver.findElement(By.xpath(pricesale)).getText();
							System.out.println("sale1pricevalueinpdp:" +sale1pricevalueinpdp); 
							String logErr621= "Pass:The UPC Contains Current/Store in Red Original in Black w/ label:\n" +sale1pricevalueinpdp+"\n" +strm1CurrFloprvalue+ "\n" +price1valueinpdp+ "\n" +strm1OrigFloprvalue; 
							logInfo(logErr621);
						}
						// Current/Store>Original
						else if(strm1CurrFloprvalue.compareTo(strm1OrigFloprvalue)>0 && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
						{
							String logErr622= "Pass:The UPC Contains Current/Store in Black:\n" +price1valueinpdp+"\n" +strm1CurrFloprvalue; 
							logInfo(logErr622);
						}
						// Original Price is null
						else if((strm1OrigFloprvalue==0) && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
						{
							String logErr623= "Pass:The UPC Contains Current/Store in Black:\n" +price1valueinpdp+"\n" +strm1CurrFloprvalue; 
							logInfo(logErr623);
						}
						// Current price is null and Fedfilavailability price available 
						else if((strm1CurrFloprvalue==0) && (strm1Fedfil.equals("true")))
						{
							String logErr624= "Pass:The UPC Contains Fedfil price:\n" +price1valueinpdp+"\n" +strm1FedFloprvalue; 
							logInfo(logErr624);
						}	
						// current price is null and Fedfilavailability price not available
						else if((strm1CurrFloprvalue==0) && (strm1Fedfil.equals("false")))
						{
							String logErr625= "Pass:The UPC Contains Original price:\n" +price1valueinpdp+"\n" +strm1OrigFloprvalue; 
							logInfo(logErr625);
						}
						// Current and original price is null and Fedfilavailability price not available
						else if((strm1CurrFloprvalue==0) && (strm1OrigFloprvalue==0) && (strm1Fedfil.equals("false")))
						{
							String text22="Price is unavailable, please validate price status in mPOS or POS.";
							String text23="Price is unavailable, please contact an  associate for pricing";
							if(price1valueinpdp.equalsIgnoreCase(text22))
							{
								String logErr626= "Pass:The Product Page contains penny price in Associate Mode:\n" +price1valueinpdp+"\n" +text22; 
								logInfo(logErr626);
							}
							else  
							{
								String logErr627= "Pass:The Product Page contains penny price in Customer Mode:\n" +price1valueinpdp+ "\n" +text23; 
								logInfo(logErr627);
							}
						}
						else if((strm1CurrFloprvalue==0) && (strm1FedFloprvalue==0))
						{
							String logErr628= "Pass:The Product Page contains Original Price:\n" +price1valueinpdp+ "\n" +strm1OrigFloprvalue; 
							logInfo(logErr628);

						}
						//  Current/store price has penny price 
						else if	((strm1CurrFloprvalue==0.01) && (strm1FedFloprvalue<=0.10)) 			
						{
							String text24="Price is unavailable, please validate price status in mPOS or POS.";
							String text25="Price is unavailable, please contact an  associate for pricing";
							if(price1valueinpdp.equalsIgnoreCase(text24))
							{
								String logErr629= "Pass:The Product Page contains penny price in Associate Mode:\n" +price1valueinpdp+"\n" +text24; 
								logInfo(logErr629);
							}
							else  
							{
								String logErr630= "Pass:The Product Page contains penny price in Customer Mode:\n" +price1valueinpdp+ "\n" +text25; 
								logInfo(logErr630);
							}
						}  				
						if(product1colorcountno<=5)
						{
							String logErr631= "Pass:The Product doesn't contain more colors:\n" +product1colorcountno; 
							logInfo(logErr631);
						}
						else
						{
							String logErr632= "Pass:The Product contains more colors:\n" +product1colorcountno; 
							logInfo(logErr632);
						}
						if(strm1Online.equals("true")&&strm1Fedfil.equals("true"))
						{
							String logErr633= "Pass:The UPC is Available for shipping"; 
							logInfo(logErr633);
							driver.findElement(By.xpath(addtoorder)).click();
							Thread.sleep(3000);
							String quickviewtitlestring2=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
							System.out.println("quickviewtitlestring2:" +quickviewtitlestring2);
							String quickviewwebidstring2=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
							System.out.println("quickviewwebidstring2:" +quickviewwebidstring2);
							String quickviewwebidstringtrim2=quickviewwebidstring2.substring(quickviewwebidstring2.indexOf("(")+1,quickviewwebidstring2.indexOf(")"));
							System.out.println("quickviewwebidstringtrim2:" +quickviewwebidstringtrim2);
							String quickviewpricestring2=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
							System.out.println("quickviewpricestring2:" +quickviewpricestring2);
							String quickviewcolorstring2=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
							System.out.println("quickviewcolorstring2:" +quickviewcolorstring2);
							//String quickviewsizestring2=driver.findElement(By.xpath(quickviewsize)).getText();
							//System.out.println("quickviewsizestring2:" +quickviewsizestring2);
							String quickviewimageurl2=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
							System.out.println("quickviewimageurl2:" +quickviewimageurl2);
							String quickviewimageurltrim2=quickviewimageurl2.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
							System.out.println("quickviewimageurltrim2:" +quickviewimageurltrim2);						
							if(quickviewtitlestring2.equals(Pdt1Name)&&quickviewwebidstringtrim2.equals(Web1ID)&&quickviewimageurltrim2.equals(Product1primaryimagetrim))
							{
								String logErr634= "Pass:The details in the AddtoBag overlay value gets matched:\n" +quickviewtitlestring2+"\n" +Pdt1Name+ "\n" +quickviewwebidstringtrim2+ "\n" +Web1ID+ "\n" +quickviewpricestring2+ "\n" +price1valueinpdp+ "\n" +quickviewcolorstring2+ "\n" +product1pagecolorvalue+  "\n" +quickviewimageurltrim2+ "\n" +Product1primaryimagetrim;
								logInfo(logErr634);  
							}
							else
							{
								String logErr635= "Fail:The details in the AddtoBag overlay value doesn't gets matched:\n" +quickviewtitlestring2+"\n" +Pdt1Name+ "\n" +quickviewwebidstringtrim2+ "\n" +Web1ID+ "\n" +quickviewpricestring2+ "\n" +price1valueinpdp+ "\n" +quickviewcolorstring2+ "\n" +product1pagecolorvalue+ "\n" +quickviewimageurltrim2+ "\n" +Product1primaryimagetrim;
								logInfo(logErr635);  
							}

							driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();		
						}
						else
						{
							String logErr636= "Pass:The UPC is not Available for shipping";
							logInfo(logErr636);
						}
						//Check others stores functionalities 
						Thread.sleep(2500);
						driver.findElement(By.xpath(productotherstores)).click();
						Thread.sleep(5000);
						//Check other Stores stream call data 
						String pdt1upcvalue = "http://social.macys.com/skavastream/core/v5/macys/product/"+Product1upcvaluetrim+"?type=UPCWithNearbyStores&storeid=1&campaignId=383";
						URL url124  = new URL(pdt1upcvalue);
						String pageSource124  = new Scanner(url124.openConnection().getInputStream()).useDelimiter("\\Z").next();
						System.out.println("pageSource124:" +pageSource124);
						JSONObject Stream1PDPUpccheckotherstores=new JSONObject(pageSource124);
						JSONObject stream1PDPUpcchild=Stream1PDPUpccheckotherstores.getJSONObject("children");
						JSONArray stream1PDPUPCSku=stream1PDPUpcchild.getJSONArray("skus");
						for(int q=0;q<stream1PDPUPCSku.length();q++)
						{
							JSONObject stream1PDPUPCSkuobj=stream1PDPUPCSku.getJSONObject(q);
							String stream1PDPUPCidentifier= stream1PDPUPCSkuobj.getString("identifier");
							System.out.println("stream1PDPUPCidentifier:" +stream1PDPUPCidentifier);
							if(stream1PDPUPCidentifier.equals(Product1upcvaluetrim))
							{
								JSONObject stream1PropUPC=stream1PDPUPCSkuobj.getJSONObject("properties");
								JSONArray stream1PDPStoreinfo=stream1PropUPC.getJSONArray("storeinfo"); 
								int product1storescounts=driver.findElements(By.xpath(productstoresize)).size(); 
								System.out.println("product1storescounts:" +product1storescounts); 
								for(int o5=0,p5=1;o5<stream1PDPStoreinfo.length()||p5<=product1storescounts;o5++,p5++)
								{

									JSONObject strm1Storeinfo1=stream1PDPStoreinfo.getJSONObject(o5);
									String strm1Storeinfophone=strm1Storeinfo1.getString("phone");
									System.out.println("strm1Storeinfophone:" +strm1Storeinfophone);								
									String strm1StoreinfoSequenceno=strm1Storeinfo1.getString("sequencenumber");
									System.out.println("strm1StoreinfoSequenceno:" +strm1StoreinfoSequenceno);
									String strm1StoreinfoInventory=strm1Storeinfo1.getString("inventory");
									System.out.println("strm1StoreinfoInventory:" +strm1StoreinfoInventory);
									String strm1StoreinfoName=strm1Storeinfo1.getString("name");
									System.out.println("strm1StoreinfoName:" +strm1StoreinfoName);
									String strm1StoreinfoZipcode=strm1Storeinfo1.getString("zipcode");
									System.out.println("strm1StoreinfoZipcode:" +strm1StoreinfoZipcode);
									String strm1StoreinfoState=strm1Storeinfo1.getString("state");
									System.out.println("strm1StoreinfoState:" +strm1StoreinfoState);							
									String strm1StoreinfoAddress1=strm1Storeinfo1.getString("address1");
									System.out.println("strm1StoreinfoAddress1:" +strm1StoreinfoAddress1);		
									String strm1StoreinfoAddress2=strm1Storeinfo1.getString("address2");
									System.out.println("strm1StoreinfoAddress2:" +strm1StoreinfoAddress2);		
									String strm1StoreinfoIdentifier=strm1Storeinfo1.getString("identifier");
									System.out.println("strm1StoreinfoIdentifier:" +strm1StoreinfoIdentifier);		
									String strm1StoreinfoCity=strm1Storeinfo1.getString("city");		
									System.out.println("strm1StoreinfoCity:" +strm1StoreinfoCity);				
									String strm1StoreinfoNameconcat=strm1StoreinfoName+strm1StoreinfoIdentifier;			
									System.out.println("strm1StoreinfoNameconcat:" +strm1StoreinfoNameconcat);					
									String strm1storeinfocombined=strm1StoreinfoNameconcat.replaceAll(""+strm1StoreinfoIdentifier+".*","")+" "+"("+strm1StoreinfoIdentifier+")";					
									System.out.println("strm1storeinfocombined:" +strm1storeinfocombined);			
									String check1availabilityheadertitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[1]/div/div[2]/div")).getText();
									System.out.println("check1availabilityheadertitle:" +check1availabilityheadertitle);				
									driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[1]/div/div[2]/div")).click();
									Thread.sleep(2000);
									String check1availabilitystreetNametitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[2]/div/div[1]")).getText();
									System.out.println("check1availabilitystreetNametitle:" +check1availabilitystreetNametitle);							
									String check1availabilitystoreLocationtitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[2]/div/div[2]")).getText();
									System.out.println("check1availabilitystoreLocationtitle:" +check1availabilitystoreLocationtitle);	
									String check1availabilitystorePhonetitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[2]/div/div[3]")).getText();
									System.out.println("check1availabilitystorePhonetitle:" +check1availabilitystorePhonetitle);
									driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p5+"]/div[1]/div/div[2]/div")).click();	
									if(check1availabilityheadertitle.equals(strm1storeinfocombined)&&check1availabilitystreetNametitle.equals(strm1StoreinfoAddress1)&&check1availabilitystoreLocationtitle.equals(strm1StoreinfoAddress2)&&check1availabilitystorePhonetitle.equals(strm1Storeinfophone))
									{
										String logErr637= "Pass:The Product Page UPC Checkother Stores value gets matched:\n" +check1availabilityheadertitle+"\n" +strm1storeinfocombined+ "\n" +check1availabilitystreetNametitle+ "\n" +strm1StoreinfoAddress1+ "\n" +check1availabilitystoreLocationtitle+ "\n" +strm1StoreinfoAddress2+ "\n" +check1availabilitystorePhonetitle+ "\n" +strm1Storeinfophone;  
										logInfo(logErr637);
									}	
									else
									{
										String logErr638= "Fail:The Product Page UPC Checkother Stores value doesn't gets matched:\n" +check1availabilityheadertitle+"\n" +strm1storeinfocombined+ "\n" +check1availabilitystreetNametitle+ "\n" +strm1StoreinfoAddress1+ "\n" +check1availabilitystoreLocationtitle+ "\n" +strm1StoreinfoAddress2+ "\n" +check1availabilitystorePhonetitle+ "\n" +strm1Storeinfophone;
										logInfo(logErr638);
									}
									Thread.sleep(5000);
									String logErr639= "-----------------------------------------------------------------------------";
									logInfo(logErr639);
									System.out.println("-----------------------------------------");  
								} 
								driver.findElement(By.xpath(productotherstoresclose)).click();  
								driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_14.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_14_id-center.skc_pageCellLayout.cls_skWidget.PDP_page div#skPageLayoutCell_14_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_leftContainerDiv.leftContainerDiv div.pdpfavList div.pdpfavList_icon")).click();
							}
						} 
					} 
				} 
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void favoritePage() 
	{
		try
		{
			//for testing the multiple email scenarios 	
			//driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[9]/div/div[1]")).click();	
			//driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[2]/div[1]")).click();	
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("1927266");
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
			//Thread.sleep(9000);
			//driver.findElement(By.xpath("//*[@id='id_leftContainerDiv']/div[3]/div")).click();
			//driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[2]/div[2]")).click();
			//Thread.sleep(1000);
			Thread.sleep(2000);
			try
			{		
				//favorites page
				String logErr311 ="Favorites Page Functionalities"; 
				logInfo(logErr311);
				String logErr312="---------------------------------"; 
				logInfo(logErr312);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[7]/div[1]")).click();
				Thread.sleep(8000);
				//7.Verify that product should be able added to Favorites and Add to bag from the PDP,Master PDP,Member PDP page and Quickview 
				String logErrsmp7="7,Verify that product should be able added to Favorites and Add to bag from the PDP Master PDP Member PDP page and Quickview, Pass"; 
				logInfo1(logErrsmp7);
				int favoriteproductscolumnvalues=driver.findElements(By.xpath(favoriteproductscount)).size(); 
				System.out.println("favoriteproductscountvalues:" +favoriteproductscolumnvalues);
				for(int q=1;q<=favoriteproductscolumnvalues;q++)
				{
					String favoritescolumnid=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div["+q+"]")).getAttribute("id");
					System.out.println("favoritescolumnid:" +favoritescolumnid);
					int favoritecolumsize=driver.findElements(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div["+q+"]/div")).size();
					System.out.println("favoritecolumsize:" +favoritecolumsize);
					for(int r=1;r<=2;r++)
					{
						driver.findElement(By.xpath("//*[@id='"+favoritescolumnid+"']/div["+r+"]/div")).click();
						System.out.println("favpdtselect:" +"//*[@id='"+favoritescolumnid+"']/div["+r+"]");
						Thread.sleep(4000);			
						String favoritesproductid=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div[1]/div["+r+"]")).getAttribute("prdtid");
						System.out.println("favoritesproductid:" +favoritesproductid);
						String favoritesproductname=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div[1]/div["+r+"]")).getAttribute("title");
						System.out.println("favoritesproductname:" +favoritesproductname);
						String favoritesproductprice=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div[1]/div["+r+"]/div[3]")).getText();
						System.out.println("favoritesproductprice:" +favoritesproductprice);
						String pdtUrl4 = "http://social.macys.com/skavastream/core/v5/macys/product/"+favoritesproductid+"?type=ID&storeid=1&campaignId=383";	
						URL url4  = new URL(pdtUrl4);
						String pageSource4  = new Scanner(url4.openConnection().getInputStream()).useDelimiter("\\Z").next();
						System.out.println("pageSource4:" +pageSource4);
						JSONObject StreamFavJson = new JSONObject(pageSource4);
						System.out.println("StreamFavJson:" +StreamFavJson); 
						Thread.sleep(3000);
						//QuickView details
						if(driver.findElement(By.className("qvTitleContainerDiv")).isDisplayed())
						{
							System.out.println("Yesss");
						}
						else
						{
							System.out.println("Noooo");
						}			
						//25.Verify that while selecting the favorites and  item in the page, the Quick view overlay should be enabled 
						String logErrsmp25 ="25,Verify that while selecting the favorites and  item in the page the Quick view overlay should be enabled, Pass"; 
						logInfo1(logErrsmp25);
						String favoritetitle=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_quickViewOverlayCon_"+favoritesproductid+".commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.pdttitledec div")).getText();	  			
						System.out.println("favoritetitle:" +favoritetitle);
						String favoriteproductimage=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+favoritesproductid+"']/img")).getAttribute("src");
						System.out.println("favoriteproductimage:" +favoriteproductimage);
						if(favoritesproductname.equals(favoritesproductname))
						{
							String logErr100= "Pass:The products name in the favorites page gets matches in the QuickView overlay:\n" +favoritesproductname;   
							logInfo(logErr100);
						}
						else
						{
							String logErr101= "Fail:The products name in the favorites page doesn't gets matches in the QuickView overlay:\n" +favoritesproductname;   
							logInfo(logErr101);
						}
						try
						{
							String favoritequickprice=driver.findElement(By.xpath("//*[@id='id_regPrice_"+favoritesproductid+"']")).getText();
							System.out.println("favoritequickprice:" +favoritequickprice);
							if(favoritesproductprice.equals(favoritequickprice))
							{
								String logErr960= "Pass:The products price in the favorites page gets matches in the QuickView overlay:\n" +favoritesproductprice;    
								logInfo(logErr960);
							}
							else
							{
								String logErr961= "Fail:The products price in the favorites page gets matches in the QuickView overlay:\n" +favoritesproductprice;    
								logInfo(logErr961);
							}					
						}
						catch(Exception e)
						{
							System.out.println(e.toString());
						}
						try
						{                 
							String favoritecolor=driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[1]/div[2]")).getText();				
							System.out.println("favoritecolor:" +favoritecolor);
							String logErr962= "Pass:The products Color in the favorites page gets matches in the QuickView overlay:\n" +favoritecolor;    
							logInfo(logErr962);				
						}
						catch(Exception e)
						{
							System.out.println(e.toString());
						}					
						Thread.sleep(1000);		
						//Error Handling for Size
						try
						{
							driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_quickViewOverlayCon_"+favoritesproductid+".commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div#id_sizeAvailabilityCont.sizeAvailabilityCont div#id_sizeContainer.sizeContainer div#id_dropContainer.sizeCont div#id_sizeTitle.sizeArrow")).click();
							Thread.sleep(1000);	
							String favoritesizeoverla=driver.findElement(By.cssSelector(".titleDivSizeText")).getText();
							System.out.println("favoritesizeoverla:" +favoritesizeoverla);
							driver.findElement(By.cssSelector(".sizeClsBtn")).click();
							Thread.sleep(1000);
						}
						catch(Exception e)
						{
							System.out.println(e.toString());
						}
						driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_quickViewOverlayCon_"+favoritesproductid+".commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div.cls_assisted_checkout.qvQtyContainer div#id_QtyContainer.sizeContainer.quantityContainer div#id_dropContainer.sizeCont div#id_qtyTitle.qtyArrow")).click();				
						Thread.sleep(1000);
						driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_quickViewOverlayCon_"+favoritesproductid+".commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_qtyMenuItems.qtyMenuContainer div.titleDivQtyCon div.qtyClsBtn")).click();
						Thread.sleep(1000);		
						//Quick View Scrolling 
						Thread.sleep(1000);
						WebElement elecr;	
						System.out.println("sss");
						elecr=driver.findElement(By.cssSelector("#id_availabilityCont")); 
						Actions abec = new Actions(driver);
						System.out.println("m");
						abec.dragAndDropBy(elecr,0,900).build().perform();
						System.out.println("drag");
						Thread.sleep(2000);
						try
						{
							if(driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_quickViewOverlayCon_"+favoritesproductid+".commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div.cls_assisted_checkout.addToOrderCont div#id_addtoOrder.addtoOrder")).isDisplayed())				
							{
								System.out.println("Yesss");
							}
							else
							{
								System.out.println("Noooooooo");
							}
							String favoritesaddtobagtext=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_quickViewOverlayCon_"+favoritesproductid+".commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div.cls_assisted_checkout.addToOrderCont div#id_addtoOrder.addtoOrder")).getText();					
							System.out.println("favoritesaddtobagtext:" +favoritesaddtobagtext);
							if(favoritesaddtobagtext.equals("Add to Bag"))  
							{
								Thread.sleep(3000);
								String logErr102= "Pass: The product contains Add to bag Availability";
								logInfo(logErr102);
								driver.findElement(By.xpath("(//*[@class='addtoOrder'])")).click();
								Thread.sleep(3000);
								String favoritesaddtobagproducttitle=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
								System.out.println("favoritesaddtobagproducttitle:" +favoritesaddtobagproducttitle);
								String favoritesaddtobagproductwebid=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
								System.out.println("favoritesaddtobagproductwebid:" +favoritesaddtobagproductwebid);
								String favoritesaddtobagproductprice=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
								System.out.println("favoritesaddtobagproductprice:" +favoritesaddtobagproductprice);
								String favoritesaddtobagproductcolor=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
								System.out.println("favoritesaddtobagproductcolor:" +favoritesaddtobagproductcolor);
								String favoritesaddtobagproductsize=driver.findElement(By.xpath("(//*[@class='overlaySizeInfoValue'])")).getText();
								System.out.println("favoritesaddtobagproductsize:" +favoritesaddtobagproductsize);
								driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();
								String logErr105= "Add to Bag Overlay Details:\n" +favoritesaddtobagproducttitle+ "\n" +favoritesaddtobagproductwebid+ "\n" +favoritesaddtobagproductprice+ "\n" +favoritesaddtobagproductcolor+ "\n" +favoritesaddtobagproductsize;
								logInfo(logErr105);
							}
							else
							{
								String logErr103="Pass:The product doesn't contains Add to bag Availability:\n";
								logInfo(logErr103);

							} 	
						}
						catch(Exception e)
						{
							System.out.println(e.toString());
						}
						Thread.sleep(1000);
						driver.findElement(By.xpath("//*[@id='id_seeFullPdtDetails_"+favoritesproductid+"']")).click();
						Thread.sleep(5000);
						//26.Verify that while selecting the "See full product details" button, the PDP page should be displayed
						String logErrsmp26 ="26,Verify that while selecting the See full product details button  the PDP page should be displayed, Pass"; 
						logInfo1(logErrsmp26);
						driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[2]/div/div[1]")).click();
						Thread.sleep(2000);
						// driver.findElement(By.xpath("//*[@id='id_qvClsBtnDiv']")).click();
						String logErr104="-------------------------------------------------------------------------------------";
						logInfo(logErr104);
						System.out.println("-----------------------------------------");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@id='id_favList']/div[1]/div[2]/div[2]/div[1]")).click();
			Thread.sleep(1000);
			//need to check the condition 	
			try
			{
				if(driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.singelPdtEmailShare")).isDisplayed())
				{
					Thread.sleep(1000);
					//12.Verify that  �QR and e-mail �share overlay should be enabled for the single product share in shopping bag,favorites and PDP Page
					String logErrsmp12 ="12,Verify that  �QR and e-mail �share overlay should be enabled for the single product share in shopping bag favorites and PDP Page, Pass"; 
					logInfo1(logErrsmp12);				
					driver.findElement(By.xpath("//*[@id='id_custNameText']")).sendKeys("QA");
					driver.findElement(By.xpath("//*[@id='id_custEmailText']")).sendKeys("naresh@skava.com");
					driver.findElement(By.xpath("//*[@id='id_associateNameText']")).sendKeys("QA");
					driver.findElement(By.xpath("//*[@id='id_associateEmailText']")).sendKeys("q@q.com");
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.singelPdtEmailShare")).click();
					Thread.sleep(2000);
					String sharesuccess=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.mailStatusInfo")).getText();
					System.out.println("sharesuccess:" +sharesuccess);
					if(sharesuccess.equals("Your email has been sent successfully"))
					{
						String logErr89="Pass:The product in the favorites page has been shared successfully\n";   
						logInfo(logErr89);
						//15.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
						String logErrsmp15 ="15.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully::: Pass"; 
						logInfo(logErrsmp15);
					}  
					else
					{
						String logErr90="Fail:The product in the favorites page doesn't gets shared successfully\n";   
						logInfo(logErr90);
						//15.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
						String logErrsmf15 ="15,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, fail"; 
						logInfo(logErrsmf15);
					} 
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_30.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#favorites_page.page.lyt_cont_div div#id_favListContainer div.loadingMaskCollectionOverlay")).click();
					Thread.sleep(2000);
					try
					{
						if(driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).isDisplayed())
						{
							driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).click();
							Thread.sleep(1000);
							driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]")).click();
							Thread.sleep(2000); 
						}
						else
						{
							driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]")).click();
							Thread.sleep(2000); 
						}
					}
					catch(Exception e)
					{
						System.out.println(e.toString());	

					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			Thread.sleep(3000);
			try
			{
				//13.Verify that  �e-mail �share overlay should be alone enabled for the multiple product share in shopping bag,favorites
				String logErrsmp13 ="13,Verify that  �e-mail �share overlay should be alone enabled for the multiple product share in shopping bag favorites, Pass"; 
				logInfo1(logErrsmp13);
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_favListShareContainer.multifavListShareContainer.overlaytobehide div#id_sharePopup.multipleSharePopup.hideOnbodyClick.cls_midleAlign div#id_favListShare.cls_favListshare div.cls_favListShareContinue")).click();
				driver.findElement(By.xpath("//*[@id='id_custNameText']")).sendKeys("QA");
				driver.findElement(By.xpath("//*[@id='id_custEmailText']")).sendKeys("naresh@skava.com");
				driver.findElement(By.xpath("//*[@id='id_associateNameText']")).sendKeys("QA");
				driver.findElement(By.xpath("//*[@id='id_associateEmailText']")).sendKeys("q@q.com");
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.singelPdtEmailShare")).click();
				Thread.sleep(5000);
				String sharesuccess=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.mailStatusInfo")).getText();
				System.out.println("sharesuccess:" +sharesuccess);
				if(sharesuccess.equals("Your email has been sent successfully"))
				{
					String logErr89="Pass:The product in the favorites page has been shared successfully\n";   
					logInfo(logErr89);
					//15.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
					String logErrsmp15 ="15,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, Pass"; 
					logInfo1(logErrsmp15);
				}  
				else
				{
					String logErr90="Fail:The product in the favorites page doesn't gets shared successfully\n";   
					logInfo(logErr90);
					//15.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
					String logErrsmf15 ="15,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, fail"; 
					logInfo1(logErrsmf15);
				} 
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_30.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#favorites_page.page.lyt_cont_div div#id_favListContainer div.loadingMaskCollectionOverlay")).click();
				Thread.sleep(2000);
				try
				{
					if(driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).isDisplayed())
					{
						driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).click();
						Thread.sleep(1000);
						driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]")).click();
						Thread.sleep(2000); 
					}
					else
					{
						driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]")).click();
						Thread.sleep(2000); 
					}

				}
				catch(Exception e)
				{
					System.out.println(e.toString());	

				}
			} 		
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			Thread.sleep(3000);		
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void logOut()
	{
		try
		{
			try
			{
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[9]/di")).click();
				Thread.sleep(1000); 
				driver.findElement(By.xpath("//*[@id='id_mamFSMLoginCont']/div[1]")).click();
				Thread.sleep(5000);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}	
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void searchSuggestionsPage() 
	{
		try
		{
			try
			{ 
				//Search Results Page functionalities
				String logErr313 ="Search Suggestions & Search Results Page "; 
				logInfo(logErr313);
				String logErr314="------------------"; 
				logInfo(logErr314);    
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[9]/div/div[1]")).click();	
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[2]")).click();	
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("Puma");
				Thread.sleep(10000);
				//Search Suggestions
				String pdtUrl5 = "http://social.macys.com/skavastream/core/v5/macys/searchsuggestion?campaignId=383&search=Puma&limit=10";	
				URL url5  = new URL(pdtUrl5);
				String pageSource5  = new Scanner(url5.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource5:" +pageSource5);
				JSONObject StreamSearchSuggestJson = new JSONObject(pageSource5);
				JSONObject searchsuggestchildren=StreamSearchSuggestJson.getJSONObject("children");
				JSONArray searchsuggestionarray=searchsuggestchildren.getJSONArray("suggestion");	
				int searchsuggestcountsize=driver.findElements(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div")).size(); 
				System.out.println("searchsuggestcountsize:" +searchsuggestcountsize);	
				for(int y=0,z=1;y<searchsuggestionarray.length()||z<=searchsuggestcountsize;y++,z++)
				{

					JSONObject searchsuggesstionobject=searchsuggestionarray.getJSONObject(y);
					String searchsuggestionsname=searchsuggesstionobject.getString("name");
					System.out.println("searchsuggestionsname: "+y+"" +searchsuggestionsname);
					String searchsuggestioninthepage=driver.findElement(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div["+z+"]/span")).getText();
					System.out.println("searchsuggestioninthepage: "+z+"" +searchsuggestioninthepage);
					if(searchsuggestionsname.equals(searchsuggestioninthepage))
					{
						String logErr50= "Pass:Search Suggestion values gets matched with the stream call "+z+":\n" +searchsuggestionsname+"\n" +searchsuggestioninthepage;   
						logInfo(logErr50);
					}
					else
					{
						String logErr51= "Fail:Search Suggestion values doesn't gets matched with the stream call "+z+":\n" +searchsuggestionsname+"\n" +searchsuggestioninthepage;   
						logInfo(logErr51);
					}

				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			try
			{
				driver.findElement(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div[10]/span")).click();	
				Thread.sleep(20000);		
				String pdtUrl3 = "http://social.macys.com/skavastream/core/v5/macys/search?campaignId=383&limit=24&search=Puma%20Jackets&storeid=1&offset=1";	
				URL url3  = new URL(pdtUrl3);
				String pageSource3  = new Scanner(url3.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource3:" +pageSource3);
				JSONObject StreamSearchJson = new JSONObject(pageSource3);
				System.out.println("StreamSearchJson:" +StreamSearchJson);
				//10.Verify that search results page should be displayed while searching the product via  Keyword
				String logErrsmp10 ="10,Verify that search results page should be displayed while searching the product via  Keyword, Pass"; 
				logInfo1(logErrsmp10);
				JSONObject streamsearchchildren=StreamSearchJson.getJSONObject("children");
				JSONArray  streamsearchproductcount=streamsearchchildren.getJSONArray("products");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_sPcolorSwatch_Color_Blue']/div")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_sPfilterCont']/div[2]")).click();
				Thread.sleep(1000);
				int searchproductcountsize=driver.findElements(By.xpath(searchproduct)).size(); 
				System.out.println("searchproductcountsize:" +searchproductcountsize);
				//driver.findElement(By.xpath("//*[@id='id_sPfilterCont']/div[2]")).click();
				//Thread.sleep(1000);
				for(int u=0,t=1;u<streamsearchproductcount.length()||t<=searchproductcountsize;u++,t++)
				{  
					JSONObject strmsearchsku=streamsearchproductcount.getJSONObject(u);
					System.out.println("strmsearchsku:" +strmsearchsku);
					String strmsearchskuname=strmsearchsku.getString("name");
					System.out.println("strmsearchskuname:" +strmsearchskuname);
					String strmsearchskuimage=strmsearchsku.getString("image");
					System.out.println("strmsearchskuimage:" +strmsearchskuimage);
					JSONObject streamproductproperties=strmsearchsku.getJSONObject("properties");
					JSONObject streamproductbuyinfo=streamproductproperties.getJSONObject("buyinfo");
					JSONObject streamproductpricingobject=streamproductbuyinfo.getJSONObject("pricing");
					JSONArray streamproductpricesarray=streamproductpricingobject.getJSONArray("prices");
					JSONObject streamstoresaleprice=streamproductpricesarray.getJSONObject(0);
					String streamstoresalepricevalue=streamstoresaleprice.getString("value");
					System.out.println("streamstoresalepricevalue:" +streamstoresalepricevalue);
					float streamstoresalepricevaluefloat=Float.parseFloat(streamstoresalepricevalue); 
					System.out.println("streamstoresalepricevaluefloat" +streamstoresalepricevaluefloat);
					JSONObject streamstoreregularprice=streamproductpricesarray.getJSONObject(1);
					String streamstoreregularpricevalue=streamstoreregularprice.getString("value");	
					System.out.println("streamstoreregularpricevalue:" +streamstoreregularpricevalue);
					float streamstoreregularpricevaluefloat=Float.parseFloat(streamstoreregularpricevalue); 
					System.out.println("streamstoreregularpricevaluefloat" +streamstoreregularpricevaluefloat);
					//JSONObject streamonlinesaleprice=streamproductpricesarray.getJSONObject(2);
					//String streamonlinesalepricevalue=streamonlinesaleprice.getString("value");
					//System.out.println("streamonlinesalepricevalue:" +streamonlinesalepricevalue);
					//JSONObject streamonlineregprice=streamproductpricesarray.getJSONObject(3);
					//String streamonlineregpricevalue=streamonlineregprice.getString("value");
					//System.out.println("streamonlineregpricevalue:" +streamonlineregpricevalue);
					System.out.println("t:" +t);
					if(t==10)
					{
						WebElement el;	
						System.out.println("START");
						el=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div[12]/div[1]/div[1]")); 
						System.out.println("MIDDLE"); 
						Actions a = new Actions(driver);
						System.out.println("Action");
						a.dragAndDropBy(el,-2850,0).build().perform();
						System.out.println("Drag");
						// a.wait();
						// System.out.println("Wait");
						//when t==10 works 	
						String searchproductid=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
						System.out.println("searchproductid:" +searchproductid);
						int searchproductindividualsize=driver.findElements(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div")).size();
						System.out.println("searchproductindividualsize:" +searchproductindividualsize);
						if(searchproductindividualsize==5)
						{
							String searchproductid1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid1:" +searchproductid1);
							String searchproductimageurl=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid1+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl:" +searchproductimageurl);
							String searchproductnamedetails=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails:" +searchproductnamedetails);
							String searchproductsaleprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[1]")).getText();
							System.out.println("searchproductsaleprice:" +searchproductsaleprice);
							String searchproductsalepricetrim=searchproductsaleprice.replaceAll(".*\\$","");
							System.out.println("searchproductsalepricetrim: " +searchproductsalepricetrim);
							float searchproductsalepricetrimfloat=Float.parseFloat(searchproductsalepricetrim);
							System.out.println("searchproductsalepricetrimfloat:" +searchproductsalepricetrimfloat);		
							String searchproductregprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductregprice:" +searchproductregprice);
							String searchproductregpricetrim=searchproductregprice.replaceAll(".*\\$","");
							System.out.println("searchproductregpricetrim: " +searchproductregpricetrim);				
							float searchproductregpricetrimfloat=Float.parseFloat(searchproductregpricetrim); 
							System.out.println("searchproductregpricetrimfloat:" +searchproductregpricetrimfloat);
							if(searchproductsalepricetrimfloat==streamstoresalepricevaluefloat||streamstoreregularpricevaluefloat==searchproductregpricetrimfloat)
							{
								String logErr200= "Pass:The search product gets details\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr200);
							}
							else
							{
								String logErr201= "Fail:The search product gets details doesn't gets matched\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr201);
							}
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==6)
						{
							String searchproductid2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid2:" +searchproductid2);
							String searchproductimageurl2=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid2+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl2:" +searchproductimageurl2);
							String searchproductnamedetails2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails2:" +searchproductnamedetails2);
							String searchproductreviewcount=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount:" +searchproductreviewcount);
							String searchproductsaleprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice1:" +searchproductsaleprice1);
							String searchproductsaleprice1trim=searchproductsaleprice1.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice1trim: " +searchproductsaleprice1trim);
							float searchproductsaleprice1trimfloat=Float.parseFloat(searchproductsaleprice1trim); 
							System.out.println("searchproductsaleprice1trimfloat:" +searchproductsaleprice1trimfloat);
							String searchproductregprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice1:" +searchproductregprice1);
							String searchproductregprice1trim=searchproductregprice1.replaceAll(".*\\$","");
							System.out.println("searchproductregprice1trim: " +searchproductregprice1trim);
							float searchproductregprice1trimfloat=Float.parseFloat(searchproductregprice1trim); 
							System.out.println("searchproductregprice1trimfloat:" +searchproductregprice1trimfloat);	
							if(streamstoresalepricevaluefloat==searchproductsaleprice1trimfloat)
							{
								String logErr202= "Pass:The search product gets details gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr202);
							}
							else
							{
								String logErr203= "Fail:The search product gets details doesn't gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr203);
							}					
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==7)
						{
							String searchproductid3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid3:" +searchproductid3);
							String searchproductimageurl3=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid3+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl3:" +searchproductimageurl3);
							String searchproductnamedetails3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails3:" +searchproductnamedetails3);
							String searchproductreviewcount1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount1:" +searchproductreviewcount1);
							String searchproductsaleprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice2:" +searchproductsaleprice2);
							String searchproductsaleprice2trim=searchproductsaleprice2.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice2trim: " +searchproductsaleprice2trim);
							float searchproductsaleprice2trimfloat=Float.parseFloat(searchproductsaleprice2trim);
							System.out.println("searchproductsaleprice2trimfloat: " +searchproductsaleprice2trimfloat);	
							String searchproductregprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice2:" +searchproductregprice2);
							String searchproductregprice2trim=searchproductregprice2.replaceAll(".*\\$","");
							System.out.println("searchproductregprice2trim: " +searchproductregprice2trim);	
							float searchproductregprice2trimfloat=Float.parseFloat(searchproductregprice2trim);
							System.out.println("searchproductregprice2trimfloat: " +searchproductregprice2trimfloat);		
							String searchproductseeitemforprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[5]")).getText();
							System.out.println("searchproductseeitemforprice:" +searchproductseeitemforprice);
							if(searchproductsaleprice2trimfloat==streamstoresalepricevaluefloat)			
							{
								String logErr204= "Pass:The search product gets details gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr204);
							}
							else
							{
								String logErr205= "Fail:The search product gets details doesn't gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr205);
							}	
							System.out.println("-----------------------------------------");
						}

					}
					else
					{
						String searchproductid=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
						System.out.println("searchproductid:" +searchproductid);
						int searchproductindividualsize=driver.findElements(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div")).size();
						System.out.println("searchproductindividualsize:" +searchproductindividualsize);
						if(searchproductindividualsize==5)
						{
							String searchproductid1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid1:" +searchproductid1);
							String searchproductimageurl=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid1+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl:" +searchproductimageurl);
							String searchproductnamedetails=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails:" +searchproductnamedetails);
							String searchproductsaleprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[1]")).getText();
							System.out.println("searchproductsaleprice:" +searchproductsaleprice);
							String searchproductsalepricetrim=searchproductsaleprice.replaceAll(".*\\$","");
							System.out.println("searchproductsalepricetrim: " +searchproductsalepricetrim);
							float searchproductsalepricetrimfloat=Float.parseFloat(searchproductsalepricetrim); 
							System.out.println("searchproductsalepricetrimfloat:" +searchproductsalepricetrimfloat);		
							String searchproductregprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductregprice:" +searchproductregprice);
							String searchproductregpricetrim=searchproductregprice.replaceAll(".*\\$","");
							System.out.println("searchproductregpricetrim: " +searchproductregpricetrim);				
							float searchproductregpricetrimfloat=Float.parseFloat(searchproductregpricetrim); 
							System.out.println("searchproductregpricetrimfloat:" +searchproductregpricetrimfloat);
							if(searchproductsalepricetrimfloat==streamstoresalepricevaluefloat||streamstoreregularpricevaluefloat==searchproductregpricetrimfloat)
							{
								String logErr206="Pass:The search product gets details gets matched\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr206);
							}
							else
							{
								String logErr207="Fail:The search product gets details doesn't gets matched\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr207);
							}					
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==6)
						{
							String searchproductid2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid2:" +searchproductid2);
							String searchproductimageurl2=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid2+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl2:" +searchproductimageurl2);
							String searchproductnamedetails2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails2:" +searchproductnamedetails2);
							String searchproductreviewcount=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount:" +searchproductreviewcount);
							String searchproductsaleprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice1:" +searchproductsaleprice1);
							String searchproductsaleprice1trim=searchproductsaleprice1.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice1trim: " +searchproductsaleprice1trim);
							float searchproductsaleprice1trimfloat=Float.parseFloat(searchproductsaleprice1trim); 
							System.out.println("searchproductsaleprice1trimfloat:" +searchproductsaleprice1trimfloat);
							String searchproductregprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice1:" +searchproductregprice1);
							String searchproductregprice1trim=searchproductregprice1.replaceAll(".*\\$","");
							System.out.println("searchproductregprice1trim: " +searchproductregprice1trim);
							float searchproductregprice1trimfloat=Float.parseFloat(searchproductregprice1trim); 
							System.out.println("searchproductregprice1trimfloat:" +searchproductregprice1trimfloat);	
							if(streamstoresalepricevaluefloat==searchproductsaleprice1trimfloat)
							{
								String logErr208="Pass:The search product gets details gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr208);
							}
							else
							{
								String logErr209= "Fail:The search product gets details doesn't gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr209);
							}		
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==7)
						{
							String searchproductid3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid3:" +searchproductid3);
							String searchproductimageurl3=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid3+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl3:" +searchproductimageurl3);
							String searchproductnamedetails3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails3:" +searchproductnamedetails3);
							String searchproductreviewcount1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount1:" +searchproductreviewcount1);
							String searchproductsaleprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice2:" +searchproductsaleprice2);
							String searchproductsaleprice2trim=searchproductsaleprice2.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice2trim: " +searchproductsaleprice2trim);
							float searchproductsaleprice2trimfloat=Float.parseFloat(searchproductsaleprice2trim);
							System.out.println("searchproductsaleprice2trimfloat: " +searchproductsaleprice2trimfloat);	
							String searchproductregprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice2:" +searchproductregprice2);
							String searchproductregprice2trim=searchproductregprice2.replaceAll(".*\\$","");
							System.out.println("searchproductregprice2trim: " +searchproductregprice2trim);	
							float searchproductregprice2trimfloat=Float.parseFloat(searchproductregprice2trim);
							System.out.println("searchproductregprice2trimfloat: " +searchproductregprice2trimfloat);		
							String searchproductseeitemforprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[5]")).getText();
							System.out.println("searchproductseeitemforprice:" +searchproductseeitemforprice);
							if(searchproductsaleprice2trimfloat==streamstoresalepricevaluefloat)
							{
								String logErr210= "Pass:The search product gets details gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr210);
							}
							else
							{
								String logErr211= "Fail:The search product gets details doesn't gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr211);
							}	
							System.out.println("-----------------------------------------"); 
						}
					}
				}  
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void masterProductPage() 
	{	
		try
		{
			try
			{
				// Master Product Page 
				String logErr315 ="Master Product Page Functionalities"; 
				logInfo(logErr315);
				String logErr316="------------------"; 
				logInfo(logErr316);
				driver.manage().deleteAllCookies();
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[9]/div/div[1]")).click();
				driver.findElement(By.xpath(".//*[@id='id_mamFooterSubMenuScroll']/div[2]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("perfu");
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div/span")).click();
				Thread.sleep(8000);
				String Masterproductid=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_198162_master']")).getAttribute("prdtid");
				System.out.println("Masterproductid:" +Masterproductid);
				driver.findElement(By.xpath("//*[@id='id_sPimageDiv_198162_master']/img")).click();
				Thread.sleep(10000);
				//Master Product Stream JSON 		
				String pdtUrl7 = "http://social.macys.com/skavastream/core/v5/macys/product/"+Masterproductid+"?type=MASTER&campaignId=383";	
				URL url7  = new URL(pdtUrl7);
				String pageSource7  = new Scanner(url7.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource7:" +pageSource7);
				JSONObject StreamMasterJson = new JSONObject(pageSource7);
				System.out.println("StreamMasterJson:" +StreamMasterJson);
				//Master Stream Call data values 
				String streammasterproductname=StreamMasterJson.getString("name");
				System.out.println("streammasterproductname:" +streammasterproductname);
				JSONObject streammasterprop=StreamMasterJson.getJSONObject("properties");
				JSONObject streamiteminfo=streammasterprop.getJSONObject("iteminfo");
				JSONArray streammasterdescription=streamiteminfo.getJSONArray("description");
				JSONObject streammasterdescriptionobject=streammasterdescription.getJSONObject(0);
				String streammasterdescriptiontext=streammasterdescriptionobject.getString("value");
				System.out.println("streammasterdescriptiontext:" +streammasterdescriptiontext);
				String streammasterproductidentifier=StreamMasterJson.getString("identifier");
				System.out.println("streammasterproductidentifier:" +streammasterproductidentifier);
				//Master Product Details 
				String masterproductname=driver.findElement(By.xpath("//*[@id='id_decContainer']/div[1]/div")).getText();
				System.out.println("masterproductname:" +masterproductname);
				String masterproductdesc=driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[2]")).getText();
				System.out.println("masterproductdesc:" +masterproductdesc);
				String masterwebid=driver.findElement(By.xpath("//*[@id='id_decContainer']/div[2]/div[1]")).getText();
				System.out.println("masterwebid:" +masterwebid);		
				String masterwebidtrim=masterwebid.substring(masterwebid.indexOf("Web ID:")+8);
				System.out.println("masterwebidtrim:" +masterwebidtrim);
				String masterproductheadertitle=driver.findElement(By.xpath("//*[@id='id_rightContainerDivMaster']/div[1]/div[1]")).getText();
				System.out.println("masterproductheadertitle:" +masterproductheadertitle);
				String masterproductheaderdesc=driver.findElement(By.xpath("//*[@id='id_rightContainerDivMaster']/div[2]/div[1]")).getText();
				System.out.println("masterproductheaderdesc:" +masterproductheaderdesc);
				String logErr129= "Master Product Page";
				logInfo(logErr129);
				String logErr130="\n" +masterproductheadertitle ;
				logInfo(logErr130);	
				if(masterwebidtrim.equals(streammasterproductidentifier)&&streammasterproductname.equals(masterproductname))
				{
					String logErr127= "Pass:The Master Product identifier & name value gets matched:\n" +masterwebidtrim+ "\n" +streammasterproductidentifier+ "\n" +streammasterproductname+ "\n" +masterproductname;
					logInfo(logErr127);
				}
				else
				{
					String logErr128= "Fail:The Master Product identifier & name value doesn't gets matched:\n" +masterwebidtrim+ "\n" +streammasterproductidentifier;
					logInfo(logErr128);
				}
				String logErr131="\n" +masterproductheaderdesc ;
				logInfo(logErr131);
				if(masterproductdesc.equals(streammasterdescriptiontext))
				{
					String logErr132= "Pass:The Master Product description value gets matched:\n" +masterproductdesc+ "\n" +streammasterdescriptiontext;
					logInfo(logErr132);
				}
				else
				{
					String logErr133= "Fail:The Master Product description value doesn't gets matched:\n" +masterproductdesc+ "\n" +streammasterdescriptiontext;
					logInfo(logErr133);
				}
				//master product primary image 	
				driver.findElement(By.xpath(".//*[@id='id_pdtLargeImg']")).click();  
				Thread.sleep(100);
				String MProductprimaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
				System.out.println("MProductprimaryimageurl:" +MProductprimaryimageurl);
				String MProductprimaryimagetrim=MProductprimaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
				System.out.println("MProductprimaryimagetrim:" +MProductprimaryimagetrim);
				String masterproductimage=StreamMasterJson.getString("image");
				System.out.println("masterproductimage:" +masterproductimage);
				if(masterproductimage.equals(MProductprimaryimagetrim))
				{
					String logErr134= "Pass:The Product Page Primary Image value gets matched:\n" +masterproductimage+ "\n" +MProductprimaryimagetrim; 
					logInfo(logErr134);
				}
				else
				{
					String logErr135= "Fail:The Product Page Primary Image value doesn't gets matched:\n" +masterproductimage+ "\n" +MProductprimaryimagetrim; 
					logInfo(logErr135);
				}    
				driver.findElement(By.xpath("(//*[@class='skrlPinchZoomClose'])")).click();
				Thread.sleep(1000);
				//Master Product Page:View Collection
				driver.findElement(By.xpath("//*[@id='id_rightContainerDivMaster']/div[1]/div[2]/div/div/div[3]")).click();
				Thread.sleep(2000);
				int memberproductcount=driver.findElements(By.xpath("//*[@id='id_ptdItemContainer']/div/div")).size();
				System.out.println("memberproductcount:" +memberproductcount);
				JSONObject streammasterchildren=StreamMasterJson.getJSONObject("children");
				JSONArray streammastermembproduct=streammasterchildren.getJSONArray("products");
				for(int c1=0,d1=1;c1<streammastermembproduct.length()||d1<=memberproductcount;c1++,d1++)
				{  
					String memberproductwebID=driver.findElement(By.xpath("(//*[@class='childWebId'])["+d1+"]")).getText();
					System.out.println("memberproductwebID:" +memberproductwebID);
					String memberwebidtrim=memberproductwebID.substring(memberproductwebID.indexOf("Web ID:")+8);
					System.out.println("memberwebidtrim:" +memberwebidtrim);		    
					driver.findElement(By.xpath("(//*[@class='checkPriceBtn'])["+d1+"]")).click();
					Thread.sleep(1000);
					String pdtUrl9 = "http://social.macys.com/skavastream/core/v5/macys/product/"+memberwebidtrim+"?type=ID&storeid=1&campaignId=383";	
					URL url9  = new URL(pdtUrl7);
					String pageSource9  = new Scanner(url9.openConnection().getInputStream()).useDelimiter("\\Z").next();
					System.out.println("pageSource9:" +pageSource9);
					JSONObject StreamMemberJson = new JSONObject(pageSource9);
					System.out.println("StreamMemberJson:" +StreamMemberJson);	
					JSONObject streammembrupclevel=streammastermembproduct.getJSONObject(c1);
					String streammemberproductname=streammembrupclevel.getString("name");
					System.out.println("streammemberproductname:" +streammemberproductname);
					String streammemberpdtidentidier=streammembrupclevel.getString("identifier");
					System.out.println("streammemberpdtidentidier:" +streammemberpdtidentidier);
					String streammemberpdtimage=streammembrupclevel.getString("image");	
					System.out.println("streammemberpdtimage:" +streammemberpdtimage);	
					//Price Value
					JSONObject streammemberupcprop=streammembrupclevel.getJSONObject("properties");
					JSONObject streammemberbuyinfo=streammemberupcprop.getJSONObject("buyinfo");   
					JSONObject streammemberpricing=streammemberbuyinfo.getJSONObject("pricing");
					JSONArray  streammemberoverprices=streammemberpricing.getJSONArray("prices");
					JSONObject streammemberoverlayrreg=streammemberoverprices.getJSONObject(0);
					String  streammemberproductregprice=streammemberoverlayrreg.getString("value");					
					System.out.println("streammemberproductregprice:" +streammemberproductregprice);
					String  streammemberproductregpricefina="$"+streammemberproductregprice;
					String streammemberproductregpricefina1=streammemberproductregpricefina.replaceAll(".0.*","")+".00";
					System.out.println("streammemberproductregpricefina1:" +streammemberproductregpricefina1);	    
					String memberproductoverlaywebid=driver.findElement(By.xpath("(//*[@class='childWebId'])["+d1+"]")).getText();
					System.out.println("memberproductoverlaywebid:" +memberproductoverlaywebid);  
					String memberproductoverlayimage=driver.findElement(By.xpath("(//*[@class='PtdContImg'])["+d1+"]")).getAttribute("src");
					System.out.println("memberproductoverlayimage:" +memberproductoverlayimage);
					String memberproductoverlayimagetrim=memberproductoverlayimage.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					System.out.println("memberproductoverlayimagetrim:" +memberproductoverlayimagetrim);	   
					String memberproductoverlayname=driver.findElement(By.xpath("//*[@id='id_chldPdtHeading_"+memberwebidtrim+"']/div")).getText();
					System.out.println("memberproductoverlayname:" +memberproductoverlayname);
					if(memberwebidtrim.equals(streammemberpdtidentidier)&&streammemberpdtimage.equals(memberproductoverlayimagetrim))
					{
						String logErr136= "Pass:The Member Product WebID,Image and Name value gets matched:\n" +streammemberpdtidentidier+ "\n" +streammemberpdtimage+ "\n" +streammemberproductname; 
						logInfo(logErr136);
					}
					else
					{
						String logErr137= "Fail:The Member Product WebID,Image and Name value gets matched:\n" +streammemberpdtidentidier+ "\n" +streammemberpdtimage+ "\n" +streammemberproductname; 
						logInfo(logErr137);
					}		
					Thread.sleep(1000);                                                  
					String memberproductoverlayprice=driver.findElement(By.xpath("(//*[@class='rlorgprice hasprice'])["+d1+"]")).getText();			
					System.out.println("memberproductoverlayprice:" +memberproductoverlayprice);
					if(streammemberproductregpricefina1.equals(memberproductoverlayprice))   		
					{
						String logErr138= "Pass:The Member Product price value gets matched:\n" +streammemberproductregpricefina1; 
						logInfo(logErr138);
					}
					else
					{
						String logErr139= "Fail:The Member Product price value doesn't gets matched:\n" +streammemberproductregpricefina1; 
						logInfo(logErr139);
					}
					Thread.sleep(1000);
					//Add to bag functionality 
					JSONArray streammemberavailability=streammemberbuyinfo.getJSONArray("availability");
					JSONObject streammemberavailabilityvalue=streammemberavailability.getJSONObject(0);
					String streammemberavailabilityinstore=streammemberavailabilityvalue.getString("instore");
					System.out.println("streammemberavailabilityinstore:" +streammemberavailabilityinstore);
					String streammemberavailabilityonline=streammemberavailabilityvalue.getString("online");
					System.out.println("streammemberavailabilityonline:" +streammemberavailabilityonline);		
					if(streammemberavailabilityonline.equals("true"))
					{
						String logErr140= "Pass:The UPC is Available for shipping"; 
						logInfo(logErr140);
						Thread.sleep(1000);
						driver.findElement(By.xpath("//*[@id='id_addtoOrder_"+streammemberpdtidentidier+"']")).click();
						Thread.sleep(5000);
						String memberpdtaddtobagoverlayname=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
						System.out.println("memberpdtaddtobagoverlayname:" +memberpdtaddtobagoverlayname);
						String memberpdtaddtobagoverlaywebid=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
						System.out.println("memberpdtaddtobagoverlaywebid:" +memberpdtaddtobagoverlaywebid);
						String memberpdtaddtobagoverlaywebidtrim=memberproductwebID.substring(memberpdtaddtobagoverlaywebid.indexOf("Web ID:")+7);
						System.out.println("memberpdtaddtobagoverlaywebidtrim:" +memberpdtaddtobagoverlaywebidtrim);	
						String memberpdtaddtobagoverlayprice=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
						System.out.println("memberpdtaddtobagoverlayprice:" +memberpdtaddtobagoverlayprice);		
						String memberpdtaddtobagoverlayimage=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
						System.out.println("memberpdtaddtobagoverlayimage:" +memberpdtaddtobagoverlayimage);
						driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();
						if(memberpdtaddtobagoverlayname.equals(streammemberproductname)&&memberpdtaddtobagoverlaywebidtrim.equals(streammemberpdtidentidier)&&memberpdtaddtobagoverlayprice.equals(streammemberproductregpricefina1)&&memberpdtaddtobagoverlayimage.equals(memberproductoverlayimagetrim))
						{
							String logErr142= "Pass:The Added items to the shopping bag values are gets matched:\n"+memberpdtaddtobagoverlayname+ "\n" +memberpdtaddtobagoverlaywebidtrim+"\n"+memberpdtaddtobagoverlayprice+"\n"+memberpdtaddtobagoverlayimage;
							logInfo(logErr142);
						}
						else
						{
							String logErr143= "Fail:The Added items to the shopping bag values doesn't  gets matched:\n"+memberpdtaddtobagoverlayname+ "\n" +memberpdtaddtobagoverlaywebidtrim+"\n"+memberpdtaddtobagoverlayprice+"\n"+memberpdtaddtobagoverlayimage;
							logInfo(logErr143);

						}

						Thread.sleep(1000);
					}
					else
					{
						String logErr141= "Pass:The UPC is not Available for shipping";
						logInfo(logErr141);
					}
					//Member Product Check other stores
					driver.findElement(By.xpath("//*[@id='id_otherStores_"+streammemberpdtidentidier+"']/span")).click();
					Thread.sleep(3000);
					String memberproductupcvalues= driver.findElement(By.xpath("//*[@id='id_chilUpc_"+streammemberpdtidentidier+"']")).getText();
					System.out.println("memberproductupcvalues:" +memberproductupcvalues);
					String memberproductupcvaluestrim=memberproductupcvalues.substring(memberproductupcvalues.indexOf("UPC:")+5);
					System.out.println("memberproductupcvaluestrim:" +memberproductupcvaluestrim);  	
					String membpdtupcvalue = "http://social.macys.com/skavastream/core/v5/macys/product/"+memberproductupcvaluestrim+"?type=UPCWithNearbyStores&storeid=1&campaignId=383";
					URL url20  = new URL(membpdtupcvalue);
					String pageSource20  = new Scanner(url20.openConnection().getInputStream()).useDelimiter("\\Z").next();
					System.out.println("pageSource20:" +pageSource20);
					JSONObject StreammembPDPUpccheckotherstores=new JSONObject(pageSource20);
					int memberproductcheckotherscount=driver.findElements(By.xpath("//*[@id='storeContainer']/div")).size();
					System.out.println("memberproductcheckotherscount:" +memberproductcheckotherscount);
					JSONObject streammemberupcchildren=StreammembPDPUpccheckotherstores.getJSONObject("children");
					JSONArray  streammemberproductsku= streammemberupcchildren.getJSONArray("skus");
					JSONObject streammemberproductskuvalue=streammemberproductsku.getJSONObject(0);
					JSONObject streammemberproductproperties=streammemberproductskuvalue.getJSONObject("properties");
					JSONArray  streammemberproductstoreinfo=streammemberproductproperties.getJSONArray("storeinfo");
					for(int e1=0,f1=1;e1<streammemberproductstoreinfo.length()||f1<=memberproductcheckotherscount;e1++,f1++)
					{
						JSONObject streammemberproductskuvalues=streammemberproductstoreinfo.getJSONObject(e1);
						String streammemberproductstorephoneno=streammemberproductskuvalues.getString("phone");
						System.out.println("streammemberproductstorephoneno:" +streammemberproductstorephoneno);
						String streammemberproductstorename=streammemberproductskuvalues.getString("name");		
						System.out.println("streammemberproductstorename:" +streammemberproductstorename);
						String streammemberproductstorezipcode=streammemberproductskuvalues.getString("zipcode");
						System.out.println("streammemberproductstorezipcode:" +streammemberproductstorezipcode);
						String streammemberproductstorestate=streammemberproductskuvalues.getString("state");
						System.out.println("streammemberproductstorestate:" +streammemberproductstorestate);
						String streammemberproductstoreaddress1=streammemberproductskuvalues.getString("address1");
						System.out.println("streammemberproductstoreaddress1:" +streammemberproductstorestate);
						String streammemberproductstoreaddress2=streammemberproductskuvalues.getString("address2");
						System.out.println("streammemberproductstoreaddress2:" +streammemberproductstorestate);
						String streammemberproductstorecity=streammemberproductskuvalues.getString("city");
						System.out.println("streammemberproductstorecity:" +streammemberproductstorecity);
						String streammemberproductstoreinventory=streammemberproductskuvalues.getString("inventory");
						System.out.println("streammemberproductstoreinventory:" +streammemberproductstoreinventory);	
						String streammemberproductidentifier=streammemberproductskuvalues.getString("identifier");
						System.out.println("streammemberproductidentifier:" +streammemberproductidentifier);			
						String streammemberstoreinfoNameconcat=streammemberproductstorename+streammemberproductidentifier;
						System.out.println("streammemberstoreinfoNameconcat:" +streammemberstoreinfoNameconcat);
						String streammemberstorenamecombined=streammemberstoreinfoNameconcat.replaceAll(""+streammemberproductidentifier+".*","")+" "+"("+streammemberproductidentifier+")";
						System.out.println("streammemberstorenamecombined:" +streammemberstorenamecombined);
						driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div")).click();
						Thread.sleep(1000);
						String memberproductstorename=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div/div[2]/div")).getText();
						System.out.println("memberproductstorename:" +memberproductstorename);
						String memberproductstoreinventory=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div/div[3]/div")).getText(); 
						System.out.println("memberproductstoreinventory:" +memberproductstoreinventory);
						String memberproductstreetname=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[2]/div/div[1]")).getText();
						System.out.println("memberproductstreetname:" +memberproductstreetname);
						String memberproductstreetlocation=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[2]/div/div[2]")).getText();  
						System.out.println("memberproductstreetlocation:" +memberproductstreetlocation);
						String memberproductstorephonenumber=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[2]/div/div[3]")).getText();
						System.out.println("memberproductstorephonenumber:" +memberproductstorephonenumber);		
						driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div/div[2]/div")).click();
						if(streammemberstorenamecombined.equals(memberproductstorename)&&streammemberproductstoreinventory.equals(memberproductstoreinventory)&&streammemberproductstoreaddress1.equals(memberproductstreetname)&&streammemberproductstoreaddress2.equals(memberproductstreetlocation)&&streammemberproductstorephoneno.equals(memberproductstorephonenumber))
						{
							String logErr158= "Pass:The Member product overlay Check Other Store values are gets matched:\n" +streammemberstorenamecombined+"\n" +streammemberproductstoreinventory+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstorephoneno;
							logInfo(logErr158);

						}
						else
						{
							String logErr159= "Pass:The Member product overlay Check Other Store values are gets matched:\n" +streammemberstorenamecombined+"\n" +streammemberproductstoreinventory+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstorephoneno;
							logInfo(logErr159);
						}			
					}
					driver.findElement(By.xpath("//*[@id='id_masterPdpSeeAllAvailable']/div/div[4]")).click();
					Thread.sleep(3000);
					WebElement elec;	
					System.out.println("START");
					elec=driver.findElement(By.xpath("//*[@id='id_shipavailableStore_"+memberwebidtrim+"']")); 
					System.out.println("MIDDLE"); 
					Actions abc = new Actions(driver);
					System.out.println("Action");
					abc.dragAndDropBy(elec,0,-200).build().perform();
					System.out.println("Drag");
				}
				driver.findElement(By.xpath("//*[@id='id_chooseItemsOverlay']/div/div[2]")).click(); 
				Thread.sleep(2000);
				try
				{
					//Review Comparison 
					String streamproductreview=driver.findElement(By.xpath("//*[@id='reviewCont']/div[1]/div[1]")).getText();
					System.out.println("streamproductreview:" +streamproductreview);
					if(streamproductreview.contains("reviews"))
					{
						driver.findElement(By.xpath("//*[@id='reviewCont']/div[1]/div[1]")).click();
						Thread.sleep(6000);
						String masterproductreviewheader=driver.findElement(By.xpath("//*[@id='id_pdtReviewContainer']/div[1]")).getText();
						System.out.println("masterproductreviewheader:" +masterproductreviewheader);
						if(masterproductreviewheader.equals("Customer Reviews"))
						{
							String logErr125= "\nMaster Product:" +masterproductreviewheader;  
							logInfo(logErr125);
						}
						//Review Stream Call JSON   
						String pdtUrl8 = "http://social.macys.com/skavastream/ratingreview/v5/macys/reviews/"+Masterproductid+"?campaignId=383&sort=desc&offset=0&limit=100";	
						URL url8  = new URL(pdtUrl8);
						String pageSource8  = new Scanner(url8.openConnection().getInputStream()).useDelimiter("\\Z").next();
						System.out.println("pageSource8:" +pageSource8);
						JSONObject StreamReviewJson = new JSONObject(pageSource8);
						System.out.println("StreamReviewJson:" +StreamReviewJson);
						JSONObject Streamreviewprop=StreamReviewJson.getJSONObject("properties");
						JSONObject Streamreviewratings=Streamreviewprop.getJSONObject("reviewrating");
						int Streamreviewcount=Streamreviewratings.getInt("reviewcount");
						System.out.println("Streamreviewcount:" +Streamreviewcount);
						JSONArray Streamreviews=Streamreviewratings.getJSONArray("reviews");	
						for(int a1=0,b1=1;a1<Streamreviews.length()||b1<=Streamreviewcount;a1++,b1++)
						{
							//Stream review details 
							JSONObject streamreviewdetails=Streamreviews.getJSONObject(a1);
							String streamreviewlabel=streamreviewdetails.getString("label");
							System.out.println("streamreviewlabel:" +streamreviewlabel);
							String streamreviewusername=streamreviewdetails.getString("username");
							System.out.println("streamreviewusername:" +streamreviewusername);
							String streamreviewcreated=streamreviewdetails.getString("created");
							System.out.println("streamreviewcreated:" +streamreviewcreated);
							String streamreviewcreatedtrim=streamreviewcreated.replaceAll("T.*","");    		
							System.out.println("streamreviewcreatedtrim:" +streamreviewcreatedtrim);
							String streamreviewvalue=streamreviewdetails.getString("value");
							System.out.println("streamreviewvalue:" +streamreviewvalue);
							//Reviewpanel in the PDP Page 
							String reviewpanelusername=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[1]/div[2]")).getText();   
							System.out.println("reviewpanelusername:" +reviewpanelusername);
							String reviewpanelreviewname=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[1]/b")).getText();
							System.out.println("reviewpanelreviewname:" +reviewpanelreviewname);
							String reviewpanelreviewdate=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[2]/i")).getText();
							System.out.println("reviewpanelreviewdate:" +reviewpanelreviewdate);		
							String reviewpanelreviewcomments=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[3]")).getText(); 
							System.out.println("reviewpanelreviewcomments:" +reviewpanelreviewcomments);
							WebElement ele;	
							System.out.println("START");
							ele=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[3]")); 
							System.out.println("MIDDLE"); 
							Actions ab = new Actions(driver);
							System.out.println("Action");
							ab.dragAndDropBy(ele,0,-60).build().perform();
							System.out.println("Drag");
							if(streamreviewlabel.equals(reviewpanelusername)&&streamreviewusername.equals(reviewpanelreviewname)&&streamreviewvalue.equalsIgnoreCase(reviewpanelreviewcomments))
							{
								String logErr123= "Pass:The Review Panels details are gets matched with the stream call:\n"  +streamreviewlabel+ "\n" +streamreviewusername+ "\n" +reviewpanelreviewdate+ "\n" +streamreviewvalue;   
								logInfo(logErr123);
								String logErr125="-------------------------------------------------";
								logInfo(logErr125);
							}  
							else
							{
								String logErr124= "Fail:The Review Panels details doesn't gets matched with the stream call:\n"  +streamreviewlabel+ "\n" +streamreviewusername+ "\n" +reviewpanelreviewdate+ "\n" +streamreviewvalue;   
								logInfo(logErr124);
								String logErr126="-------------------------------------------------";
								logInfo(logErr126);
							}
							System.out.println("-------------------------------------------------------------------");
						}
					} 
				}
				catch(Exception e)
				{
					System.out.println(e.toString());
				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}		
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}   	

	private static void browsePagePriceValidation()
	{
		try
		{
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]")).click();
			Thread.sleep(2000);
			try
			{
				//working flow 	//Selecting the MLB category in the home page 
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_83-north-center-north_slider']/div/div[3]/div[1]")).click();
				Thread.sleep(3000);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}	
			try
			{
				//Selecting randomly the categories in  the MLB category 
				Random r2 = new java.util.Random();
				List<WebElement> links = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_12_id-center']/div/div"));
				WebElement randomElement = links.get(r2.nextInt(links.size()));
				randomElement.click();
				Thread.sleep(2000); 
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			Thread.sleep(4000);			
			//Browse Page
			String logErr900 ="Browse Page-Price Functionality"; 
			logInfo(logErr900);
			String logErr901="-------------";
			logInfo(logErr901);
			Thread.sleep(2000);
			//Price High to Low
			driver.findElement(By.xpath("//*[@id='id_bPsortByCont']/div[3]/div[2]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='id_sortByOpt_1']/span")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
			Thread.sleep(1000);
			int browseproductcount=driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div")).size();
			System.out.println("browseproductcount:" +browseproductcount);
			int browseproducttrim=browseproductcount-1;
			for(int br=1;br<=6;br++)
			{
				//Sitelet details 
				String productid=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+br+"]")).getAttribute("prdtid");
				System.out.println("productid:" +productid);                  
				String browseproductname=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+br+"]")).getText();
				System.out.println("browseproductname:" +browseproductname);		  
				String browsesitesaleprice=driver.findElement(By.xpath("(//*[@class='bPpdtSalePriceDiv'])["+br+"]")).getText();
				System.out.println("browsesitesaleprice:" +browsesitesaleprice); 		
				String browsesiteregprice=driver.findElement(By.xpath("(//*[@class='bPpdtRegPriceDiv'])["+br+"]")).getText();
				System.out.println("browsesiteregprice:" +browsesiteregprice);	
				//Local Data 
				String url1="C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtstore_2_EDT.js";	
				//URL url11  = new URL(url1);
				//String pageSource  = new Scanner(url11.openConnection().getInputStream()).useDelimiter("\\Z").next();
				//System.out.println("pageSource:" +pageSource); 	
				String pageSource=readFile(url1);
				String pageSourcetrim=pageSource.substring(pageSource.indexOf("skone_callback(")+15,pageSource.indexOf(")"));
				System.out.println("pageSourcetrim:" +pageSourcetrim);
				JSONObject browsepricedetails=new JSONObject(pageSourcetrim);
				String Filetime=browsepricedetails.getString("filetime");
				System.out.println("Filetime:" +Filetime);
				try
				{
					JSONObject browseprice=browsepricedetails.getJSONObject("prices");
					JSONObject browsepriceid=browseprice.getJSONObject(productid);
					System.out.println("browsepriceid:" +browsepriceid);
					String browseregprice=browsepriceid.getString("reg");
					System.out.println("browseregprice:" +browseregprice);
					Float browseregpricefloat=Float.parseFloat(browseregprice);
					String browsesaleprice=browsepriceid.getString("sale");
					System.out.println("browsesaleprice:" +browsesaleprice);		
					Float browsesalepricefloat=Float.parseFloat(browsesaleprice);		
					//BrowsePage Logic - Current/Store=Original
					if(browsesalepricefloat.compareTo(browseregpricefloat)==0)
					{
						String browsesiteregprice1=driver.findElement(By.xpath("(//*[@class='bPpdtRegPriceDiv'])["+br+"]")).getText();
						System.out.println("browsesiteregprice1:" +browsesiteregprice1);
						String browsesiteregpricetrim=browsesiteregprice1.replaceAll(".*\\$","");
						System.out.println("browsesiteregpricetrim: " +browsesiteregpricetrim);
						float browsesiteregpricetrimfloat=Float.parseFloat(browsesiteregpricetrim);	
						if((browsesiteregpricetrimfloat==browseregpricefloat))
						{
							String logErr878= "Pass:The Product Contains Regprice/SalePrice in black:\n" +productid+ "\n" +browseproductname+ "\n"+browsesalepricefloat+"\n" +browseregpricefloat; 
							logInfo(logErr878);
						}
						else 
						{
							String logErr8781= "Fail:The Product doesn't gets displayed Regprice/SalePrice in black:\n" +productid+ "\n" +browseproductname+ "\n"+browsesalepricefloat+"\n" +browseregpricefloat; 
							logInfo(logErr8781);
						}
					}
					// Current/Store<Original				
					else if(browsesalepricefloat.compareTo(browseregpricefloat)<0)
					{
						String browsesitesaleprice1=driver.findElement(By.xpath("(//*[@class='bPpdtSalePriceDiv'])["+br+"]")).getText();
						System.out.println("browsesitesaleprice1:" +browsesitesaleprice1);	
						String browsesitesalepricetrim=browsesitesaleprice1.replaceAll(".*\\$","");
						System.out.println("browsesitesalepricetrim: " +browsesitesalepricetrim);
						float browsesitesalepricetrimfloat=Float.parseFloat(browsesitesalepricetrim);		
						String browsesiteregprice2=driver.findElement(By.xpath("(//*[@class='bPpdtRegPriceDiv'])["+br+"]")).getText();
						System.out.println("browsesiteregprice2:" +browsesiteregprice2);
						String browsesiteregpricetrim=browsesiteregprice2.replaceAll(".*\\$","");
						System.out.println("browsesiteregpricetrim: " +browsesiteregpricetrim);
						float browsesiteregpricetrimfloat=Float.parseFloat(browsesiteregpricetrim);		
						if((browsesitesalepricetrimfloat==browsesalepricefloat)&&(browsesiteregpricetrimfloat==browseregpricefloat))
						{

							String logErr879= "Pass:The Product Contains SalePrice in Red Regprice in Black label:\n" +productid+ "\n" +browseproductname+ "\n" +browsesalepricefloat+"\n" +browsesiteregprice2; 
							logInfo(logErr879);
						}
						else
						{
							String logErr8791= "Fail:The Product doesn't gets displayed SalePrice in Red Regprice in Black label:\n" +productid+ "\n" +browseproductname+ "\n" +browsesalepricefloat+"\n" +browsesiteregprice2; 
							logInfo(logErr8791);

						}
					}
				}		
				catch(Exception e)
				{
					System.out.println(e.toString());
				}
				//4.Verify that "jstore" price file should be displayed in the browse page for the products if there is the "jstore" zone file for the category  or " See Item for Price" should be displayed for the products in the browse page 
				String logErrsmp4 ="4,Verify that jstore price file should be displayed in the browse page for the products if there is the jstore zone file for the category  or See Item for Price should be displayed for the products in the browse page, Pass"; 
				logInfo1(logErrsmp4);	
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void browseFacetsPage() 
	{
		try
		{
			try
			{
				//Browse Page Functionalities
				String logErr951 ="Browse Page Functionalities"; 
				logInfo(logErr951);
				String logErr952="------------------"; 
				logInfo(logErr952);		
				//3.Verify that products are displayed in the browse page according to the selected facets 
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]")).click();
				Thread.sleep(2000);
				//NHL Category
				driver.findElement(By.xpath(".//*[@id='skPageLayoutCell_83-north-center-center_slider']/div/div[3]/div[1]")).click();
				Thread.sleep(2000);
				//ANAHEIM DUCK
				driver.findElement(By.xpath(".//*[@id='skPageLayoutCell_10_id-center']/div/div[28]")).click();
				Thread.sleep(5000);
				//More Size Overlay Condition 	
				if(driver.findElement(By.xpath("//*[@id='id_bPsizeCont']/div[2]/div[9]/div")).isDisplayed())
				{
					driver.findElement(By.xpath("//*[@id='id_bPsizeCont']/div[2]/div[9]/div")).click();
					Thread.sleep(1000);  
					int moresizecount=driver.findElements(By.xpath("(//*[@class='bPsizeTxt'])")).size();
					System.out.println("moresizecount:" +moresizecount);			
				}
				else
				{
					int moresizecount=driver.findElements(By.xpath("(//*[@class='bPsizeTxt'])")).size();
					System.out.println("moresizecount:" +moresizecount);
				}	
				Thread.sleep(2000);
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_9.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#browse.page.lyt_cont_div div#skPageLayoutCell_9_id-center.skc_pageCellLayout.cls_skWidget.BrowsePage_widget div#skPageLayoutCell_9_id-center.cls_customWidget div#id_BrowsePageContent.BrowsePageContent.skPageContainer div#id_moreSizePopup.moreSizePopup.bpPopup div#id_moreSizeContainer.moreSizeContainer div.moreSizeScrollerItems div#id_bPsizeSwatch_Size_S.bPsizeSwatch div.bPsizeSwatchText div.bPsizeTxt")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
				Thread.sleep(2000);
				int browsefacetscount=driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div")).size();
				System.out.println("browsefacetscount:" +browsefacetscount);		
				String url12="C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtcategory66205.js";	
				//URL url112  = new URL(url12);
				//String pageSource12  = new Scanner(url112.openConnection().getInputStream()).useDelimiter("\\Z").next();
				//String pageSource12  = fileRead(url112.openConnection().getInputStream());
				String pageSource12=readFile(url12);
				System.out.println("pageSource12:" +pageSource12); 	
				String pageSourcefacetstrim=pageSource12.substring(pageSource12.indexOf("skone_callback(")+15,pageSource12.indexOf(")"));
				System.out.println("pageSourcefacetstrim:" +pageSourcefacetstrim);
				JSONObject browsepricedetails=new JSONObject(pageSourcefacetstrim);	
				JSONArray browsefacets=browsepricedetails.getJSONArray("facets");
				JSONObject browsesizefacets=browsefacets.getJSONObject(1);
				JSONArray browsesizefacetsvalue=browsesizefacets.getJSONArray("values");
				JSONObject  browsesizefacetsvalues=browsesizefacetsvalue.getJSONObject(2);
				JSONArray browsesizefacetsvaluesvalues=browsesizefacetsvalues.getJSONArray("value");
				String logErr897="Size Facets Selection Functionalities"; 
				logInfo(logErr897);
				String logErr898="-------------------------------------"; 
				logInfo(logErr898);
				for(int browsesize=1;browsesize<=browsefacetscount;browsesize++)
				{                                                                   
					String browsesizefacetsstring=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage']) ["+browsesize+"]")).getAttribute("prdtid");					
					System.out.println("browsesizefacetsstring:" +browsesizefacetsstring);
					for(int size=0;size<browsesizefacetsvaluesvalues.length();size++)
					{
						String browsesizeproductidsize=browsesizefacetsvaluesvalues.getString(size);
						System.out.println("browsesizeproductidsize:" +browsesizeproductidsize);

						if(browsesizefacetsstring.equals(browsesizeproductidsize))
						{
							String logErr890="Pass:Browse Page Size Facets value gets match:\n" +browsesizeproductidsize+ "\n" +browsesizefacetsstring; 
							logInfo(logErr890);
						}		
					}	
				}
				//3.Verify that products are displayed in the browse page according to the selected facets 
				String logErrsmp3 ="3,Verify that products are displayed in the browse page according to the selected facets, Pass"; 
				logInfo1(logErrsmp3);
				//Geneder>Male
				driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_9.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#browse.page.lyt_cont_div div#skPageLayoutCell_9_id-center.skc_pageCellLayout.cls_skWidget.BrowsePage_widget div#skPageLayoutCell_9_id-center.cls_customWidget div#id_BrowsePageContent.BrowsePageContent.skPageContainer div#id_bPleftSlider.bPleftSlider div#id_bPLeftContentWrapper.bPLeftContentWrapper div#id_bPLeftContentScroller.bPLeftContentScroller div#id_bPfacetCont.bPfacetCont div#id_bPfacet_Gender.bPfacet div.bPfaceNameLeftContent div.bPfacetName")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_9.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#browse.page.lyt_cont_div div#skPageLayoutCell_9_id-center.skc_pageCellLayout.cls_skWidget.BrowsePage_widget div#skPageLayoutCell_9_id-center.cls_customWidget div#id_BrowsePageContent.BrowsePageContent.skPageContainer div#id_bPfacetSubMenu.bPfacetSubMenu.subslider div#id_bPfacetSubWrapper.bPfacetSubWrapper div#id_bPfacetSubScroller.bPfacetSubScroller div#id_bPfacet_Gender_Male.bPfacet div.bPfacetName")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_9.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#browse.page.lyt_cont_div div#skPageLayoutCell_9_id-center.skc_pageCellLayout.cls_skWidget.BrowsePage_widget div#skPageLayoutCell_9_id-center.cls_customWidget div#id_BrowsePageContent.BrowsePageContent.skPageContainer div#id_bPfacetSubMenu.bPfacetSubMenu.subslider div#id_bPfilterCont.bPfilterCont div.bPfacetSubLeftArrow")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#studiop_9.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#browse.page.lyt_cont_div div#skPageLayoutCell_9_id-center.skc_pageCellLayout.cls_skWidget.BrowsePage_widget div#skPageLayoutCell_9_id-center.cls_customWidget div#id_BrowsePageContent.BrowsePageContent.skPageContainer div#id_bPleftSlider.bPleftSlider div#id_bPfilterCont.bPfilterCont div.bPleftArrow")).click();
				Thread.sleep(1000);
				int browsecategoryfacets=driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div")).size(); 
				System.out.println("browsecategoryfacets:" +browsecategoryfacets);
				JSONObject browsecatfacets=browsefacets.getJSONObject(2);
				JSONArray  browsecatfacetsvalue=browsecatfacets.getJSONArray("values");
				JSONObject browsecatfacetsvalues=browsecatfacetsvalue.getJSONObject(3);
				JSONArray browsecatfacetsvaluesvalues=browsecatfacetsvalues.getJSONArray("value");	
				String logErr895="Category Facets Selection Functionalities"; 
				logInfo(logErr895);
				String logErr896="------------------------------------------"; 
				logInfo(logErr896);	
				for(int bcgs=1;bcgs<=browsecategoryfacets;bcgs++)
				{
					String browsecatfacetsstring=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+bcgs+"]")).getAttribute("prdtid");
					System.out.println("browsecatfacetsstring:" +browsecatfacetsstring);
					for(int csize=0;csize<browsecatfacetsvaluesvalues.length();csize++)
					{
						String browsecatproductidsize=browsecatfacetsvaluesvalues.getString(csize);
						System.out.println("browsecatproductidsize:" +browsecatproductidsize);					
						if(browsecatfacetsstring.equals(browsecatproductidsize))
						{
							String logErr894="Pass:Browse Page Category Facets value gets match:\n" +browsecatfacetsstring+ "\n" +browsecatproductidsize; 
							logInfo(logErr894);
						}		
					}	
				}
				//3.Verify that products are displayed in the browse page according to the selected facets 
				String logErrsmp31 ="3,Verify that products are displayed in the browse page according to the selected facets, Pass"; 
				logInfo1(logErrsmp31);
			}	
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]")).click();
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void registryMenuSelection() 
	{
		try
		{
			try
			{
				//Selecting the Registry option from the menu section
				driver.findElement(By.xpath("(//*[@class='cls_mamFMenu'])")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("(//*[@class='cls_mamFSMBtns'])[5]")).click();
				Thread.sleep(1000);
				String logErr3000="Pass:The Registry Landing Page gets displayed"; 
				logInfo(logErr3000);
			}		
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void registryCreate() 
	{
		try
		{
			try
			{
				String registrylpc=driver.getWindowHandle();
				System.out.println(registrylpc);
				Thread.sleep(1000);
				//Registry Create BVR Pages
				driver.findElement(By.xpath("(//*[@class='itemTextInner'])[1]")).click();
				Thread.sleep(12000);
				driver.switchTo().frame("wssgPageFrame");
				driver.findElement(By.xpath("//*[@id='loginEmail']")).sendKeys("testregistry@skava.com");
				driver.findElement(By.xpath("//*[@id='loginPassword']")).sendKeys("skava123");
				driver.findElement(By.xpath("//*[@id='signinButton']")).click();
				Thread.sleep(6000);	
				if(driver.findElement(By.xpath("(//*[@class='cls_mamFooterMenuCon'])")).isDisplayed())
				{
					//38.Verify user is able to login usign create page, Then BVR page should be shown with "I Do" icon enabled 
					String logErrsmp38 ="38,Verify user is able to login usign create page Then BVR page should be shown with I Do icon enabled, Pass"; 
					logInfo1(logErrsmp38);			
				}
				else
				{
					//38.Verify user is able to login usign create page, Then BVR page should be shown with "I Do" icon enabled 
					String logErrsmf38 ="38,Verify user is able to login usign create page Then BVR page should be shown with I Do icon enabled, Fail"; 
					logInfo1(logErrsmf38);
				}
				driver.findElement(By.xpath("(//*[@class='toggleBtnCont handlingPressState'])")).click();
				Thread.sleep(2000);	
				if(driver.findElement(By.xpath("(//*[@class='cls_mamFMacysLogoCont'])")).isDisplayed())
				{
					//66.Verify wherther bvr signout is works fine 
					String logErrsmp66="66,Verify wherther bvr signout is works fine, Pass"; 
					logInfo1(logErrsmp66);	
				}
				else
				{
					//66.Verify wherther bvr signout is works fine 
					String logErrsmf66 ="66,Verify wherther bvr signout is works fine, Fail"; 
					logInfo1(logErrsmf66);
				}
				driver.switchTo().window(registrylpc);
			}		
			catch(Exception e)
			{
				System.out.println(e.toString());
			}

		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void registryManage() 
	{
		try
		{
			try
			{
				driver.findElement(By.xpath("(//*[@class='cls_mamFMenu'])")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("(//*[@class='cls_mamFSMBtns'])[5]")).click();
				Thread.sleep(1000);
				String logErr3000="Pass:The Registry Landing Page gets displayed"; 
				logInfo(logErr3000);
				String registrylpm=driver.getWindowHandle();
				System.out.println(registrylpm);
				//Registry Manage BVR Pages
				driver.findElement(By.xpath("(//*[@class='itemTextInner'])[2]")).click();
				Thread.sleep(8000);		
				driver.switchTo().frame("wssgPageFrame");
				driver.findElement(By.xpath("//*[@id='loginEmail']")).sendKeys("testregistry@skava.com");
				driver.findElement(By.xpath("//*[@id='loginPassword']")).sendKeys("skava123");
				driver.findElement(By.xpath("//*[@id='signInForm']/div[3]/div")).click();
				Thread.sleep(8000);	
				if(driver.findElement(By.xpath("(//*[@class='cls_mamFooterMenuCon'])")).isDisplayed())
				{
					//39.Verify user is able to login using manage page, Then BVR page should be shown with "I Do" icon enabled 
					String logErrsmp39 ="39,Verify user is able to login using manage page then BVR page should be shown with I Do icon enabled, Pass"; 
					logInfo1(logErrsmp39);			
				}
				else
				{
					//38.Verify user is able to login usign create page, Then BVR page should be shown with "I Do" icon enabled 
					String logErrsmf39 ="39,Verify user is able to login using manage page then BVR page should be shown with I Do icon enabled, Fail"; 
					logInfo1(logErrsmf39);
				}			
				driver.findElement(By.xpath("(//*[@class='toggleBtnCont handlingPressState'])")).click();
				Thread.sleep(2000);	
				if(driver.findElement(By.xpath("(//*[@class='cls_mamFMacysLogoCont'])")).isDisplayed())
				{
					//66.Verify wherther bvr signout is works fine 
					String logErrsmp661="66,Verify wherther bvr signout is works fine, Pass"; 
					logInfo1(logErrsmp661);	
				}
				else
				{
					//66.Verify wherther bvr signout is works fine 
					String logErrsmf661 ="66,Verify wherther bvr signout is works fine, Fail"; 
					logInfo1(logErrsmf661);
				}
				driver.switchTo().window(registrylpm);
			}		
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}		
	private static void registryFind() 
	{
		try
		{
			try
			{
				driver.findElement(By.xpath("(//*[@class='cls_mamFMenu'])")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("(//*[@class='cls_mamFSMBtns'])[5]")).click();
				Thread.sleep(1000);
				String logErr3000="Pass:The Registry Landing Page gets displayed"; 
				logInfo(logErr3000);
				String registrylpf=driver.getWindowHandle();
				System.out.println(registrylpf);
				//Registry Find Pages
				driver.findElement(By.xpath("(//*[@class='itemTextInner'])[3]")).click();
				Thread.sleep(4000);
				if(driver.findElement(By.xpath("(//*[@class='fr_headerText'])")).isDisplayed())
				{
					//40.Verify on selecting find registry, Find registry page is loading and page is not floating 
					String logErrsmp40 ="40,Verify on selecting find registry Find registry page is loading and page is not floating, Pass"; 
					logInfo1(logErrsmp40);			
				}
				else
				{
					//40.Verify user is able to login usign create page, Then BVR page should be shown with "I Do" icon enabled 
					String logErrsmf40 ="40,Verify on selecting find registry Find registry page is loading and page is not floating, Fail"; 
					logInfo1(logErrsmf40);
				}		
				driver.findElement(By.xpath("//*[@id='id_fr_ipBox_firstName']")).sendKeys("qa");
				driver.findElement(By.xpath("//*[@id='id_fr_ipBox_lastName']")).sendKeys("qa");
				Thread.sleep(1000);
				//41.Verify on selecting find registry, Find registry page is loading and page is not floating 
				String logErrsmp41 ="41,Verify user is able to enter the first name last name and search registry, Pass"; 
				logInfo1(logErrsmp41);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_fr_searchBtn']")).click();
				Thread.sleep(3000);
				if(driver.findElement(By.xpath("(//*[@class='llists_idoImage'])")).isDisplayed())
				{
					//42.Verify the registry result are shown in shown, else if there is network error retry option is shown 
					String logErrsmp42="42,Verify the registry result are shown in shown, else if there is network error retry option is shown, Pass"; 
					logInfo1(logErrsmp42);			
				}
				else
				{
					//42.Verify the registry result are shown in shown, else if there is network error retry option is shown 
					String logErrsmf42="42,Verify the registry result are shown in shown, else if there is network error retry option is shown, Fail"; 
					logInfo1(logErrsmf42);
				}	
				driver.findElement(By.xpath("(//*[@class='llists_tryAgainBtn'])")).click();
				Thread.sleep(1000);
				if(driver.findElement(By.xpath("(//*[@class='fr_headerText'])")).isDisplayed())
				{
					//49.Verify the Try again option is working in Find registry search result page
					String logErrsmp49="49,Verify the Try again option is working in Find registry search result page, Pass"; 
					logInfo1(logErrsmp49);			
				}
				else
				{
					String logErrsmf49="49,Verify the Try again option is working in Find registry search result page, Fail"; 
					logInfo1(logErrsmf49);					
				}
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_fr_ipBox_firstName']")).clear();
				driver.findElement(By.xpath("//*[@id='id_fr_ipBox_lastName']")).clear();
				//69.Verify the entered chars gets retain in the firstname and lastname field while navigating back to the find registry page 
				String logErrsmp69="69,Verify the entered chars gets retain in the firstname and lastname field while navigating back to the find registry page, Pass"; 
				logInfo1(logErrsmp69);
				driver.findElement(By.xpath("//*[@id='id_fr_ipBox_firstName']")).sendKeys("sunil"); //qa
				driver.findElement(By.xpath("//*[@id='id_fr_ipBox_lastName']")).sendKeys("reddy"); //qa
				//driver.findElement(By.xpath("(//*[@class='fr_dd_arrow closed'])")).click();  
				//driver.findElement(By.xpath("(//*[@class='fr_dd_month_options'])[11]")).click();
				//driver.findElement(By.xpath("//*[@id='id_fr_yearDropDownCont']/div[4]")).click();
				//driver.findElement(By.xpath("(//*[@class='fr_dd_year_options'])[3]")).click();
				//70.Verify that while selecting the dropdown in the find registry pages for the Month and year dropdown the dropdown should be enabled 
				String logErrsmp70="70,Verify that while selecting the dropdown in the find registry pages for the Month and year dropdown the dropdown should be enabled, Pass"; 
				logInfo1(logErrsmp70);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_fr_searchBtn']")).click();
				Thread.sleep(3000);
				//Find Registry - Search Results page
				String findregUrl = "http://cmsmacyspreprod.skavaone.com/skavastream/registry/rl/macys/event/findregistry?campaignId=383&firstname=sunil&lastname=reddy&month=11&year=2015";
				URL frurl  = new URL(findregUrl);
				String frpageSource  = new Scanner(frurl.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("frpageSource:" +frpageSource); 	
				if(frpageSource.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr3k = "Fail:The Find Registry Page doesn't gets displayed:\n" +frpageSource;
					logInfo(logErr3k);
				}
				else
				{
					JSONObject strmfrjson = new JSONObject(frpageSource);
					JSONObject strmfrchildren=strmfrjson.getJSONObject("children");
					JSONArray strmfrevents=strmfrchildren.getJSONArray("events");
					JSONObject strmfrobject=strmfrevents.getJSONObject(0);
					JSONObject strmfrprop=strmfrobject.getJSONObject("properties");
					JSONObject strmfrregistryinfo=strmfrprop.getJSONObject("registryinfo");
					String strmfreventdate=strmfrregistryinfo.getString("eventdate");
					System.out.println("strmfreventdate:" +strmfreventdate); 
					String strmfreventdatetrim=strmfreventdate.replaceAll("T.*","");
					System.out.println("strmfreventdatetrim:" +strmfreventdatetrim);
					SimpleDateFormat existingformatter = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					String strmfreventdateformat = formatter.format(existingformatter.parse(strmfreventdatetrim));
					System.out.println("strmfreventdateformat:"+strmfreventdateformat);			
					JSONObject strmfrstate=strmfrprop.getJSONObject("state");
					String strmfreventlocation=strmfrstate.getString("location");
					System.out.println("strmfreventlocation:" +strmfreventlocation);
					JSONArray strmfruserinfo=strmfrprop.getJSONArray("userinfo");
					JSONObject strmfruserobj0=strmfruserinfo.getJSONObject(0);
					String strmfrregfirstname=strmfruserobj0.getString("firstname");
					System.out.println("strmfrregfirstname:" +strmfrregfirstname);
					String strmfrreglastname=strmfruserobj0.getString("lastname");
					System.out.println("strmfrreglastname:" +strmfrreglastname);
					JSONObject strmfruserobj1=strmfruserinfo.getJSONObject(1);
					String strmfrcoregfirstname=strmfruserobj1.getString("firstname");
					System.out.println("strmfrcoregfirstname:" +strmfrcoregfirstname);
					String strmfrcoreglastname=strmfruserobj1.getString("lastname");
					System.out.println("strmfrcoreglastname:" +strmfrcoreglastname);	
					String stfrregfirstname=driver.findElement(By.xpath("(//*[@class='rFirstName'])")).getText();
					System.out.println("stfrregfirstname:" +stfrregfirstname);
					String stfrcoregfirstname=driver.findElement(By.xpath("(//*[@class='crFirstName'])")).getText();
					System.out.println("stfrcoregfirstname:" +stfrcoregfirstname);
					String stfrregeventdate=driver.findElement(By.xpath("(//*[@class='llists_grid_eventDate'])[1]")).getText();
					System.out.println("stfrregeventdate:" +stfrregeventdate);	
					String stfrregeventdatetrim=stfrregeventdate.substring(stfrregeventdate.indexOf("Event Date : ")+13);
					System.out.println("stfrregeventdatetrim:" +stfrregeventdatetrim);
					String stfrregeventlocation=driver.findElement(By.xpath("(//*[@class='llists_grid_eventDate'])[2]")).getText();
					System.out.println("stfrregeventlocation:" +stfrregeventlocation);
					String stfrregeventlocationtrim=stfrregeventlocation.substring(stfrregeventlocation.indexOf("Event Location : ")+17);
					System.out.println("stfrregeventlocationtrim:" +stfrregeventlocationtrim);
					if(strmfreventdateformat.equals(stfrregeventdatetrim)&&strmfreventlocation.equals(stfrregeventlocationtrim)&&strmfrregfirstname.equals(stfrregfirstname)&&strmfrcoregfirstname.equals(stfrcoregfirstname))	
					{
						String logErr3k1 = "Pass:The Find Registry Page details gets matched:\n"+strmfrregfirstname+"\n"+strmfrreglastname+"\n"+strmfrcoregfirstname+"\n"+strmfrcoreglastname+"\n"+strmfreventdateformat+"\n"+strmfreventlocation;
						logInfo(logErr3k1);

					}
					else
					{
						String logErr3k2 = "Fail:The Find Registry Page details gets matched:\n"+strmfrregfirstname+"\n"+strmfrreglastname+"\n"+strmfrcoregfirstname+"\n"+strmfrcoreglastname+"\n"+strmfreventdateformat+"\n"+strmfreventlocation;
						logInfo(logErr3k2);
					}
				}
				Thread.sleep(2000);
				driver.findElement(By.xpath("(//*[@class='llists_registry_grid_Container'])[1]")).click();
				Thread.sleep(2000);
				String frregistryid=driver.findElement(By.xpath("(//*[@class='llists_registry_grid_Container'])[1]")).getAttribute("identifier");
				System.out.println("frregistryid:" +frregistryid);
				Thread.sleep(3000);
				if(driver.findElement(By.xpath("(//*[@class='panelImg'])")).isDisplayed())			
				{
					//43.Verify the On selecting any registry from registry result page, individual registry page is loaded 
					String logErrsmp43="43,Verify the On selecting any registry from registry result page individual registry page is loaded, Pass"; 
					logInfo1(logErrsmp43);					
				}
				else
				{
					//43.Verify the registry result are shown in shown, else if there is network error retry option is shown 
					String logErrsmf43="43,Verify the On selecting any registry from registry result page individual registry page is loaded, Fail"; 
					logInfo1(logErrsmf43);
				}
				//Find Registry GVR Page
				String findreggvrUrl = "http://cmsmacyspreprod.skavaone.com/skavastream/registry/rl/macys/event/gvr?campaignId=383&type=GVR&storeid=1&orderid="+frregistryid+"&x-skava-apikey=cb73326c07ff4344471b45655323f1a0";
				URL frgvrurl=new URL(findreggvrUrl);
				String frgvrpageSource=new Scanner(frgvrurl.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("frgvrpageSource:" +frgvrpageSource); 	
				if(frgvrpageSource.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr3k = "Fail:The Find Registry Page doesn't gets displayed:\n" +frpageSource;
					logInfo(logErr3k);
				}
				else
				{
					JSONObject strmfrgvrjson = new JSONObject(frgvrpageSource);	
					JSONObject strmfrgvrproperties=strmfrgvrjson.getJSONObject("properties");
					JSONObject strmfrgvrregistryinfo=strmfrgvrproperties.getJSONObject("registryinfo");
					String strmfrgvrregistrylocation=strmfrgvrregistryinfo.getString("location");
					System.out.println("strmfrgvrregistrylocation:" +strmfrgvrregistrylocation);
					JSONObject strmfrgvrevents=strmfrgvrregistryinfo.getJSONObject("events");
					String strmfrgvrmonth=strmfrgvrevents.getString("month");
					System.out.println("strmfrgvrmonth:" +strmfrgvrmonth);
					int strmfrgvrmonthfloat=Integer.parseInt(strmfrgvrmonth);
					String strmfrgvrday=strmfrgvrevents.getString("day");
					System.out.println("strmfrgvrday:" +strmfrgvrday);
					int strmfrgvrdayfloat=Integer.parseInt(strmfrgvrday);
					String strmfrgvryear=strmfrgvrevents.getString("year");
					System.out.println("strmfrgvryear:" +strmfrgvryear);
					int strmfrgvryearfloat=Integer.parseInt(strmfrgvryear);
					String strmfrgvreventdateconcat=(strmfrgvrmonth+"/"+strmfrgvrday+"/"+strmfrgvryear);
					System.out.println("strmfrgvreventdateconcat:" +strmfrgvreventdateconcat);
					String strmfrgvrbarcode=strmfrgvrregistryinfo.getString("barcode");
					System.out.println("strmfrgvrbarcode:" +strmfrgvrbarcode);
					String strmfrgvrtype=strmfrgvrregistryinfo.getString("type");
					System.out.println("strmfrgvrtype:" +strmfrgvrtype);
					JSONObject strmfrgvriteminfo=strmfrgvrproperties.getJSONObject("iteminfo");
					JSONArray strmfrgvrflags=strmfrgvriteminfo.getJSONArray("flags");
					JSONObject strmfrgvrobject=strmfrgvrflags.getJSONObject(0);
					String strmfrgvrgogreenvalue=strmfrgvrobject.getString("value");
					System.out.println("strmfrgvrgogreenvalue:" +strmfrgvrgogreenvalue);
					String strmfrgvrgogreenlabel=strmfrgvrobject.getString("label");
					System.out.println("strmfrgvrgogreenlabel:" +strmfrgvrgogreenlabel);
					JSONArray strmfrgvruserinfo=strmfrgvrproperties.getJSONArray("userinfo");
					JSONObject strmfrgvruserinfo0=strmfrgvruserinfo.getJSONObject(0);
					String strmfrgvruiregfirstname=strmfrgvruserinfo0.getString("firstname");
					System.out.println("strmfrgvruiregfirstname:" +strmfrgvruiregfirstname);
					String strmfrgvruireglastname=strmfrgvruserinfo0.getString("lastname");
					System.out.println("strmfrgvruireglastname:" +strmfrgvruireglastname);
					String strmregistrantnameconcat=strmfrgvruiregfirstname+" "+strmfrgvruireglastname;
					System.out.println("strmregistrantnameconcat:" +strmregistrantnameconcat);		
					JSONObject strmfrgvruserinfo1=strmfrgvruserinfo.getJSONObject(1);
					String strmfrgvruicoregfirstname=strmfrgvruserinfo1.getString("firstname");
					System.out.println("strmfrgvruicoregfirstname:" +strmfrgvruicoregfirstname);
					String strmfrgvruicoreglastname=strmfrgvruserinfo1.getString("lastname");
					System.out.println("strmfrgvruicoreglastname:" +strmfrgvruicoreglastname);	
					String strmcoregistrantnameconcat=strmfrgvruicoregfirstname+" "+strmfrgvruicoreglastname;
					System.out.println("strmcoregistrantnameconcat:" +strmcoregistrantnameconcat);
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					Date currentdate = new Date();
					String strmfrgvrcurrentdate= dateFormat.format(currentdate); 
					System.out.println("strmfrgvrcurrentdate:" +strmfrgvrcurrentdate);
					String sitecurrentdatemonth=strmfrgvrcurrentdate.substring(0,2);
					System.out.println("sitecurrentdatemonth:" +sitecurrentdatemonth);
					int sitecurrentdatemonthfloat=Integer.parseInt(sitecurrentdatemonth);
					String sitecurrentdatedate=strmfrgvrcurrentdate.substring(3,5);
					System.out.println("sitecurrentdatedate:" +sitecurrentdatedate);
					int sitecurrentdatedatefloat=Integer.parseInt(sitecurrentdatedate);   
					String sitecurrentdateyear=strmfrgvrcurrentdate.substring(6,10);
					System.out.println("sitecurrentdateyear:" +sitecurrentdateyear);
					int sitecurrentdateyearfloat=Integer.parseInt(sitecurrentdateyear);
					//GVR Page Details 
					String frgvrregistrantName=driver.findElement(By.xpath("(//*[@class='registrantName'])")).getText();
					System.out.println("frgvrregistrantName:" +frgvrregistrantName);
					String frgvrcoregistrantName=driver.findElement(By.xpath("(//*[@class='coregistrantName'])")).getText();
					System.out.println("frgvrcoregistrantName:" +frgvrcoregistrantName);
					String frgvrregistrantandcoregName=frgvrregistrantName+" "+"&"+" "+frgvrcoregistrantName;
					System.out.println("frgvrregistrantandcoregName:" +frgvrregistrantandcoregName);
					if((sitecurrentdateyearfloat<strmfrgvryearfloat)||(sitecurrentdatemonthfloat<strmfrgvrmonthfloat)||(sitecurrentdatedatefloat<strmfrgvrdayfloat))
					{		
						int streamdate=365*strmfrgvryearfloat + strmfrgvryearfloat/4 - strmfrgvryearfloat/100 + strmfrgvryearfloat/400 + strmfrgvrdayfloat + (153*strmfrgvrmonthfloat+8)/5;
						System.out.println("streamdate:" +streamdate);
						int currentdatetime=365*sitecurrentdateyearfloat + sitecurrentdateyearfloat/4 - sitecurrentdateyearfloat/100 + sitecurrentdateyearfloat/400 + sitecurrentdatedatefloat + (153*sitecurrentdatemonthfloat+8)/5;
						System.out.println("currentdatetime:" +currentdatetime);	
						int daysreamining=streamdate-currentdatetime;
						System.out.println("daysreamining:" +daysreamining);
						String frgvreventDateCont=driver.findElement(By.xpath("(//*[@class='eventDateCont'])")).getText();
						System.out.println("frgvreventDateCont:" +frgvreventDateCont);
						String frgvreventDateConttri=frgvreventDateCont.replaceAll("d.*","");
						System.out.println("frgvreventDateConttrim:" +frgvreventDateConttri);
						String frgvreventDateConttrim=frgvreventDateConttri.trim();
						int frgvreventDateContint=Integer.parseInt(frgvreventDateConttrim);			
						System.out.println("frgvreventDateContint:" +frgvreventDateContint);
						if(strmregistrantnameconcat.equals(frgvrregistrantName)&&strmcoregistrantnameconcat.equals(frgvrcoregistrantName)&&daysreamining==frgvreventDateContint)
						{
							String logErr3k3= "Pass:The Find Registry GVR page Left Panel values gets matched:\n"+strmregistrantnameconcat+ "\n" +strmcoregistrantnameconcat+ "\n" +frgvreventDateCont+ "\n" +strmfrgvrregistrylocation;
							logInfo(logErr3k3);			
						}
						else
						{
							String logErr3k4= "Fail:The Find Registry GVR page Left Panel values doesn't gets matched:\n"+strmregistrantnameconcat+ "\n" +strmcoregistrantnameconcat+ "\n" +frgvreventDateCont+ "\n" +strmfrgvrregistrylocation;
							logInfo(logErr3k4);			
						}	
					}
					else
					{
						String logErr3k5= "INFO:The EventDate is completed";
						logInfo(logErr3k5);			

					}
					//gogreen flag 
					if(strmfrgvrgogreenvalue.equals("true")&&strmfrgvrgogreenlabel.equals("goGreen"))		
					{
						driver.findElement(By.xpath("(//*[@class='goingGreenCont'])")).isDisplayed();
						//71.Verify that "gogreen" logo and text  should be displayed for the registry in the gvr page which contains the gogreen flag as true
						String logErrsmp71="71,Verify that gogreen logo and text should be displayed for the registry in the gvr page which contains the gogreen flag as true, Pass"; 
						logInfo1(logErrsmp71);
					}
					else
					{
						String logErrsmf71="71,Verify that gogreen logo and text should be displayed for the registry in the gvr page which contains the gogreen flag as true, Fail"; 
						logInfo1(logErrsmf71);
					}
					//BarCode Functionalities
					driver.findElement(By.xpath("(//*[@class='barCodeBtn'])")).click();
					Thread.sleep(1000);
					if(driver.findElement(By.xpath("(//*[@class='barCodeContentContainer'])")).isDisplayed())
					{
						//72.Verify that while selecting the barcode option in the gvr page the barcode overlay should be enabled 
						String logErrsmp72="72,Verify that while selecting the barcode option in the gvr page the barcode overlay should be enabled , Pass"; 
						logInfo1(logErrsmp72);			
					}
					else
					{
						String logErrsmf72="72,Verify that while selecting the barcode option in the gvr page the barcode overlay should be enabled , Fail"; 
						logInfo1(logErrsmf72);
					}
					String frgvrbrcdregname=driver.findElement(By.xpath("(//*[@class='registryName'])")).getText();
					System.out.println("frgvrbrcdregname:" +frgvrbrcdregname);
					String frgvrbrcdregid=driver.findElement(By.xpath("(//*[@class='registryId'])")).getText();
					System.out.println("frgvrbrcdregid:" +frgvrbrcdregid);		
					String frgvrbrcdriddata=driver.findElement(By.xpath("//*[@id='id_barCodeContainer']/div[2]/div[2]/div[3]/div/div[70]")).getText();
					System.out.println("frgvrbrcdriddata:" +frgvrbrcdriddata);
					if(frgvrregistrantandcoregName.equals(frgvrbrcdregname)&&frgvrbrcdregid.equals(frregistryid)&&frgvrbrcdriddata.equals(strmfrgvrbarcode))
					{
						String logErr3k6= "Pass:The Find Registry GVR page barcode overlay values gets matched:\n"+frgvrregistrantandcoregName+ "\n" +frgvrbrcdregid+ "\n" +frgvrbrcdriddata;
						logInfo(logErr3k6);	
					}
					else
					{
						String logErr3k7= "Fail:The Find Registry GVR page barcode overlay values gets matched:\n"+frgvrregistrantandcoregName+ "\n" +frgvrbrcdregid+ "\n" +frgvrbrcdriddata;
						logInfo(logErr3k7);	
					}
					driver.findElement(By.xpath("(//*[@class='barCodeCloseBtn'])")).click();	
					Thread.sleep(1000);
					//Right Panel Comparison
					JSONObject strmfrgvrchildren=strmfrgvrjson.getJSONObject("children");
					JSONArray strmfrgvrproduct=strmfrgvrchildren.getJSONArray("products");
					int frgvrproduct=driver.findElements(By.xpath("(//*[@class='productItemCont firstItemCon listView'])")).size();
					System.out.println("frgvrproduct:" +frgvrproduct);
					for(int frgvrs=1;frgvrs<=frgvrproduct;frgvrs++) 
					{
						String frgvrproductid=driver.findElement(By.xpath("(//*[@class='productItemCont firstItemCon listView'])["+frgvrs+"]")).getAttribute("product");
						System.out.println("frgvrproductid:" +frgvrproductid);
						String frgvrregistryTitle=driver.findElement(By.xpath("(//*[@class='registryTitle'])["+frgvrs+"]")).getText();
						System.out.println("frgvrregistryTitle:" +frgvrregistryTitle);
						String frgvrpdtDescription=driver.findElement(By.xpath("(//*[@class='pdtDescription'])["+frgvrs+"]")).getText();
						System.out.println("frgvrpdtDescription:" +frgvrpdtDescription);
						String frgvrcolorValue=driver.findElement(By.xpath("(//*[@class='colorValue'])["+frgvrs+"]")).getText();
						System.out.println("frgvrcolorValue:" +frgvrcolorValue);	
						String frgvrsizeValue=driver.findElement(By.xpath("(//*[@class='sizeValue'])["+frgvrs+"]")).getText();
						System.out.println("frgvrsizeValue:" +frgvrsizeValue);
						for(int frgvr=0;frgvr<strmfrgvrproduct.length();frgvr++)		
						{
							JSONObject strmfrgvrproductobj=strmfrgvrproduct.getJSONObject(frgvr);
							String strmfrgvrproductidentifier=strmfrgvrproductobj.getString("identifier");
							System.out.println("strmfrgvrproductidentifier:" +strmfrgvrproductidentifier);
							if(frgvrproductid.equals(strmfrgvrproductidentifier))
							{
								String strmfrgvrproductname=strmfrgvrproductobj.getString("name");
								System.out.println("strmfrgvrproductname:" +strmfrgvrproductname);
								JSONObject strmfrgvrchildproperties=strmfrgvrproductobj.getJSONObject("properties");
								JSONObject strmfrgvrchildskuinfo=strmfrgvrchildproperties.getJSONObject("skuinfo");
								JSONObject strmfrgvrskuinfosize1=strmfrgvrchildskuinfo.getJSONObject("size1");
								String strmfrgvrskuinfosize1value=strmfrgvrskuinfosize1.getString("value");
								System.out.println("strmfrgvrskuinfosize1value:" +strmfrgvrskuinfosize1value);
								JSONObject strmfrgvrchildstate=strmfrgvrchildproperties.getJSONObject("state");
								JSONObject strmfrgvrchilditeminfo=strmfrgvrchildproperties.getJSONObject("iteminfo");
								String strmfrgvrchilditeminfoitemtype=strmfrgvrchilditeminfo.getString("itemtype");
								System.out.println("strmfrgvrchilditeminfoitemtype:" +strmfrgvrchilditeminfoitemtype);
								String strmfrgvrchilditeminfoupccolor=strmfrgvrchilditeminfo.getString("upccolor");	
								System.out.println("strmfrgvrchilditeminfoupccolor:" +strmfrgvrchilditeminfoupccolor);
								String strmfrgvrchilditeminfoupcvaluer=strmfrgvrchilditeminfo.getString("upc");	
								System.out.println("strmfrgvrchilditeminfoupcvaluer:" +strmfrgvrchilditeminfoupcvaluer);
								if(frgvrpdtDescription.equals(strmfrgvrproductname)&&frgvrcolorValue.equals(strmfrgvrchilditeminfoupccolor)&&frgvrsizeValue.equals(strmfrgvrskuinfosize1value))
								{
									String logErr3k8= "Pass:The Find Registry GVR page product details values gets matched:\n"+frgvrpdtDescription+ "\n" +frgvrregistryTitle+ "\n" +frgvrcolorValue+ "\n" +frgvrsizeValue;
									logInfo(logErr3k8);	
								}
								else
								{
									String logErr3k9= "Fail:The Find Registry GVR page product details values gets matched:\n"+frgvrpdtDescription+ "\n" +frgvrregistryTitle+ "\n" +frgvrcolorValue+ "\n" +frgvrsizeValue;
									logInfo(logErr3k9);	
								}
								Thread.sleep(1000);
								driver.findElement(By.xpath("(//*[@class='addtofavorite handlingPressState'])["+frgvrs+"]")).click();
								Thread.sleep(2000);
								if(driver.findElement(By.xpath("(//*[@class='cls_alertMsg'])")).isEnabled())
								{
									//73.Verify that while selecting the Add to Favorites button for the individual product in the Find Registry GVR pages the item should be added to favorites page  
									String logErrsmp73="73,Verify that while selecting the Add to Favorites button for the individual product in the Find Registry GVR pages the item should be added to favorites page , Pass"; 
									logInfo1(logErrsmp73);					
								}
								else
								{
									//73.Verify that while selecting the Add to Favorites button for the individual product in the Find Registry GVR pages the item should be added to favorites page  
									String logErrsmf73="73,Verify that while selecting the Add to Favorites button for the individual product in the Find Registry GVR pages the item should be added to favorites page , Fail"; 
									logInfo1(logErrsmf73);
								}
								Thread.sleep(4000);
								driver.findElement(By.xpath("(//*[@class='qtyArrow'])["+frgvrs+"]")).click();
								Thread.sleep(2000);
								int qtysize=driver.findElements(By.xpath("(//*[@class='qtyTextCont handleBodyClick'])["+frgvrs+"]/div")).size();
								System.out.println("qtysize:" +qtysize);
								Thread.sleep(2000);
								driver.findElement(By.xpath("(//*[@class='qtyTextCont handleBodyClick'])["+frgvrs+"]/div[3]")).click();   
								Thread.sleep(1000);	
								if(qtysize==13)
								{  
									String logErrsmp74="74,Verify that while selecting the Qty button for the individual product in the Find Registry GVR pages the Qty overlay should be enabled with 12Qty,Pass"; 
									logInfo1(logErrsmp74);				
								}
								else
								{
									String logErrsmf74="74,Verify that while selecting the Qty button for the individual product in the Find Registry GVR pages the Qty overlay should be enabled with 12Qty,Fail"; 
									logInfo1(logErrsmf74);				
								}
								String findreggvrcheckother="http://cmsmacyspreprod.skavaone.com/skavastream/core/rl/macys/product/"+strmfrgvrchilditeminfoupcvaluer+"?type=UPCWithNearbyStores&storeid=1&campaignId=383&x-skava-apikey=cb73326c07ff4344471b45655323f1a0";			
								URL frgvrcturl=new URL(findreggvrcheckother);
								String frgvrctpageSource=new Scanner(frgvrcturl.openConnection().getInputStream()).useDelimiter("\\Z").next(); 
								JSONObject frgvrctpageSourcejson=new JSONObject(frgvrctpageSource);
								JSONObject frgvrctpageSourcechildren=frgvrctpageSourcejson.getJSONObject("children");
								JSONArray frgvrctpageSourcesku=frgvrctpageSourcechildren.getJSONArray("skus");
								JSONObject frgvrctpageSourceskuobj=frgvrctpageSourcesku.getJSONObject(0);
								JSONObject frgvrctpageSourceproperties=frgvrctpageSourceskuobj.getJSONObject("properties");
								JSONObject frgvrctpageSourcebuyinfo=frgvrctpageSourceproperties.getJSONObject("buyinfo");
								JSONObject frgvrctpageSourcepricing=frgvrctpageSourcebuyinfo.getJSONObject("pricing");
								JSONArray frgvrctpageSourceprices=frgvrctpageSourcepricing.getJSONArray("prices");
								JSONObject frgvrctpageSourceorigprices=frgvrctpageSourceprices.getJSONObject(0);
								String frgvrctpageSourceorigpricesvalue=frgvrctpageSourceorigprices.getString("value");
								Float frgvrctpageSourceorigpricesvaluefloat=Float.parseFloat(frgvrctpageSourceorigpricesvalue);	
								System.out.println("frgvrctpageSourceorigpricesvaluefloat:" +frgvrctpageSourceorigpricesvaluefloat);
								JSONObject frgvrctpageSourcecurrentprices=frgvrctpageSourceprices.getJSONObject(1);
								String frgvrctpageSourcecurrentpricesvalue=frgvrctpageSourcecurrentprices.getString("value");
								Float frgvrctpageSourcecurrentpricesvaluefloat=Float.parseFloat(frgvrctpageSourcecurrentpricesvalue);	
								System.out.println("frgvrctpageSourcecurrentpricesvaluefloat:" +frgvrctpageSourcecurrentpricesvaluefloat);
								JSONObject frgvrctpageSourcefedfilprices=frgvrctpageSourceprices.getJSONObject(2);	
								String frgvrctpageSourcefedfilpricesvalue=frgvrctpageSourcefedfilprices.getString("value");
								Float frgvrctpageSourcefedfilpricesvaluefloat=Float.parseFloat(frgvrctpageSourcefedfilpricesvalue);	
								System.out.println("frgvrctpageSourcefedfilpricesvaluefloat:" +frgvrctpageSourcefedfilpricesvaluefloat);
								JSONArray frgvrctpageSourceavailability=frgvrctpageSourcebuyinfo.getJSONArray("availability");
								JSONObject frgvrctpageSourceavailabilityobj=frgvrctpageSourceavailability.getJSONObject(0);
								String frgvrctpageSourcefedfilavailability=frgvrctpageSourceavailabilityobj.getString("fedfil");
								System.out.println("frgvrctpageSourcefedfilavailability:" +frgvrctpageSourcefedfilavailability);
								String frgvrctpageSourceonlineavailability=frgvrctpageSourceavailabilityobj.getString("online");
								System.out.println("frgvrctpageSourceonlineavailability:" +frgvrctpageSourceonlineavailability);
								JSONObject frgvrpagesourceobjproperties=frgvrctpageSourcejson.getJSONObject("properties");
								JSONObject frgvrpagesourceobjregistryinfo=frgvrpagesourceobjproperties.getJSONObject("registryinfo");
								String frgvrpagesourceobjregistryinfotype=frgvrpagesourceobjregistryinfo.getString("type");
								System.out.println("frgvrpagesourceobjregistryinfotype:" +frgvrpagesourceobjregistryinfotype);	
								String pricefrgvrvalueinpdp=driver.findElement(By.xpath("//*[@id='id_pdpRegPrice']")).getText();
								System.out.println("pricefrgvrvalueinpdp:" +pricefrgvrvalueinpdp);		
								//Current/Store is same
								if(frgvrctpageSourcecurrentpricesvaluefloat.compareTo(frgvrctpageSourceorigpricesvaluefloat)==0 && (frgvrctpageSourcefedfilavailability.equals("true")|| frgvrctpageSourcefedfilavailability.equals("false")))
								{
									String logErr3k10= "Pass:The UPC Contains Current/Store price:\n" +frgvrctpageSourcecurrentpricesvaluefloat; 
									logInfo(logErr3k10);
								}
								// Current/Store<Original
								else if(frgvrctpageSourcecurrentpricesvaluefloat.compareTo(frgvrctpageSourceorigpricesvaluefloat)<0 && (frgvrctpageSourcefedfilavailability.equals("true")|| frgvrctpageSourcefedfilavailability.equals("false")))
								{
									String logErr3k11= "Pass:The UPC Contains Current/Store in Red Original in Black w/ label:\n" +frgvrctpageSourcecurrentpricesvaluefloat+"\n" +frgvrctpageSourceorigpricesvaluefloat; 
									logInfo(logErr3k11);
								}
								// Current/Store>Original
								else if(frgvrctpageSourcecurrentpricesvaluefloat.compareTo(frgvrctpageSourceorigpricesvaluefloat)>0 && (frgvrctpageSourcefedfilavailability.equals("true")|| frgvrctpageSourcefedfilavailability.equals("false")))
								{
									String logErr3k12= "Pass:The UPC Contains Current/Store in Black:\n" +frgvrctpageSourcecurrentpricesvaluefloat; 
									logInfo(logErr3k12);
								}
								// Original Price is null
								else if((frgvrctpageSourceorigpricesvaluefloat==0) && (frgvrctpageSourcefedfilavailability.equals("true")|| frgvrctpageSourcefedfilavailability.equals("false")))
								{
									String logErr3k13= "Pass:The UPC Contains Current/Store in Black:\n" +frgvrctpageSourceorigpricesvaluefloat; 
									logInfo(logErr3k13);
								}
								// Current price is null and Fedfilavailability price available 
								else if((frgvrctpageSourcefedfilpricesvaluefloat==0) && (frgvrctpageSourcefedfilavailability.equals("true")))
								{
									String logErr3k14= "Pass:The UPC Contains Fedfil price:\n" +frgvrctpageSourcefedfilpricesvaluefloat; 
									logInfo(logErr3k14);
								}
								// current price is null and Fedfilavailability price not available
								else if((frgvrctpageSourcefedfilpricesvaluefloat==0) && (frgvrctpageSourcefedfilavailability.equals("false")))
								{
									String logErr3k15= "Pass:The UPC Contains Original price:\n" +frgvrctpageSourcefedfilpricesvaluefloat; 
									logInfo(logErr3k15);
								}
								// Current and original price is null and Fedfilavailability price not available
								else if((frgvrctpageSourcecurrentpricesvaluefloat==0) && (frgvrctpageSourceorigpricesvaluefloat==0) && (frgvrctpageSourcefedfilavailability.equals("false")))
								{
									String text="Price is unavailable, please validate price status in mPOS or POS.";
									String text1="Price is unavailable, please contact an  associate for pricing";
									if(pricefrgvrvalueinpdp.equalsIgnoreCase(text))
									{
										String logErr3k17= "Pass:The Product Page contains penny price in Associate Mode:\n" +pricefrgvrvalueinpdp; 
										logInfo(logErr3k17);
									}
									else  
									{
										String logErr3k18= "Pass:The Product Page contains penny price in Customer Mode:\n" +pricefrgvrvalueinpdp; 
										logInfo(logErr3k18);
									}
								}
								else if((frgvrctpageSourceorigpricesvaluefloat==0) && (frgvrctpageSourcefedfilpricesvaluefloat==0))
								{
									String logErr3k16= "Pass:The Product Page contains Original Price:\n" +frgvrctpageSourceorigpricesvaluefloat; 
									logInfo(logErr3k16);

								}
								//  Current/store price has penny price 
								else if	((frgvrctpageSourcecurrentpricesvaluefloat==0.01) && (frgvrctpageSourcefedfilpricesvaluefloat<=0.10)) 			
								{
									String text="Price is unavailable, please validate price status in mPOS or POS.";
									String text1="Price is unavailable, please contact an  associate for pricing";
									if(pricefrgvrvalueinpdp.equalsIgnoreCase(text))
									{
										String logErr3k19= "Pass:The Product Page contains penny price in Associate Mode:\n" +pricefrgvrvalueinpdp; 
										logInfo(logErr3k19);
									}
									else  
									{
										String logErr3k20= "Pass:The Product Page contains penny price in Customer Mode:\n" +pricefrgvrvalueinpdp; 
										logInfo(logErr3k20);
									}
								}
								//75.Verify that while selecting the Qty options for the individual product in the Find Registry GVR pages the online price should be displayed on the price logic
								String logErrsmp75="75,Verify that while selecting the Qty options for the individual product in the Find Registry GVR pages the online price should be displayed on the price logic,Pass"; 
								logInfo1(logErrsmp75);	
								//Find Registry GVR See All Available 
								Thread.sleep(1000);
								driver.findElement(By.xpath("(//*[@class='listSeeAllAvailable handlingPressState'])["+frgvrs+"]")).click();
								Thread.sleep(2000);
								if(driver.findElement(By.xpath("(//*[@class='overlayTitleDiv'])")).isDisplayed())
								{
									//76.Verify that while selecting the See all available options for the individual product in the Find Registry GVR pages the check other storesoverlay should be enabled
									String logErrsmp76="76,Verify that while selecting the See all available options for the individual product in the Find Registry GVR pages the check other storesoverlay should be enabled,Pass"; 
									logInfo1(logErrsmp76);	

								}
								else
								{
									String logErrsmf76="76,Verify that while selecting the See all available options for the individual product in the Find Registry GVR pages the check other storesoverlay should be enabled,Fail"; 
									logInfo1(logErrsmf76);	
								}
								JSONArray frgvrpagestoreinfo=frgvrctpageSourceproperties.getJSONArray("storeinfo");	
								int frgvrstorecount=driver.findElements(By.xpath("//*[@id='storeContainer']/div")).size();
								System.out.println("frgvrstorecount:" +frgvrstorecount);
								for(int fgstrstre=0,fgstrstrem=1;fgstrstre<frgvrpagestoreinfo.length()||fgstrstrem<=frgvrstorecount;fgstrstre++,fgstrstrem++)
								{
									JSONObject frstrm1Storeinfo1=frgvrpagestoreinfo.getJSONObject(fgstrstre);
									String frstrm1Storeinfophone=frstrm1Storeinfo1.getString("phone");
									System.out.println("frstrm1Storeinfophone:" +frstrm1Storeinfophone);								
									String frstrm1StoreinfoSequenceno=frstrm1Storeinfo1.getString("sequencenumber");
									System.out.println("frstrm1StoreinfoSequenceno:" +frstrm1StoreinfoSequenceno);
									String frstrm1StoreinfoInventory=frstrm1Storeinfo1.getString("inventory");
									System.out.println("frstrm1StoreinfoInventory:" +frstrm1StoreinfoInventory);
									String frstrm1StoreinfoName=frstrm1Storeinfo1.getString("name");
									System.out.println("frstrm1StoreinfoName:" +frstrm1StoreinfoName);
									String frstrm1StoreinfoZipcode=frstrm1Storeinfo1.getString("zipcode");
									System.out.println("frstrm1StoreinfoZipcode:" +frstrm1StoreinfoZipcode);
									String frstrm1StoreinfoState=frstrm1Storeinfo1.getString("state");
									System.out.println("frstrm1StoreinfoState:" +frstrm1StoreinfoState);								
									String frstrm1StoreinfoAddress1=frstrm1Storeinfo1.getString("address1");
									System.out.println("frstrm1StoreinfoAddress1:" +frstrm1StoreinfoAddress1);			
									String frstrm1StoreinfoAddress2=frstrm1Storeinfo1.getString("address2");
									System.out.println("frstrm1StoreinfoAddress2:" +frstrm1StoreinfoAddress2);		
									String frstrm1StoreinfoIdentifier=frstrm1Storeinfo1.getString("identifier");
									System.out.println("frstrm1StoreinfoIdentifier:" +frstrm1StoreinfoIdentifier);			
									String frstrm1StoreinfoCity=frstrm1Storeinfo1.getString("city");		
									System.out.println("frstrm1StoreinfoCity:" +frstrm1StoreinfoCity);								
									String frstrm1StoreinfoNameconcat=frstrm1StoreinfoName+frstrm1StoreinfoIdentifier;			
									System.out.println("frstrm1StoreinfoNameconcat:" +frstrm1StoreinfoNameconcat);								
									String frstrm1storeinfocombined=frstrm1StoreinfoNameconcat.replaceAll(""+frstrm1StoreinfoIdentifier+".*","")+" "+"("+frstrm1StoreinfoIdentifier+")";					
									System.out.println("strm1storeinfocombined:" +frstrm1storeinfocombined);				
									String frcheck1availabilityheadertitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+fgstrstrem+"]/div[1]/div/div[2]/div")).getText();
									System.out.println("frcheck1availabilityheadertitle:" +frcheck1availabilityheadertitle);					
									driver.findElement(By.xpath("//*[@id='storeContainer']/div["+fgstrstrem+"]/div[1]/div/div[2]/div")).click();
									Thread.sleep(2000);
									String frcheck1availabilitystreetNametitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+fgstrstrem+"]/div[2]/div/div[1]")).getText();
									System.out.println("frcheck1availabilitystreetNametitle:" +frcheck1availabilitystreetNametitle);							
									String frcheck1availabilitystoreLocationtitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+fgstrstrem+"]/div[2]/div/div[2]")).getText();
									System.out.println("frcheck1availabilitystoreLocationtitle:" +frcheck1availabilitystoreLocationtitle);	
									String frcheck1availabilitystorePhonetitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+fgstrstrem+"]/div[2]/div/div[3]")).getText();
									System.out.println("frcheck1availabilitystorePhonetitle:" +frcheck1availabilitystorePhonetitle);
									driver.findElement(By.xpath("//*[@id='storeContainer']/div["+fgstrstrem+"]/div[1]/div/div[2]/div")).click();	
									if(frcheck1availabilityheadertitle.equals(frstrm1storeinfocombined)&&frcheck1availabilitystreetNametitle.equals(frstrm1StoreinfoAddress1)&&frcheck1availabilitystoreLocationtitle.equals(frstrm1StoreinfoAddress2)&&frcheck1availabilitystorePhonetitle.equals(frstrm1Storeinfophone))
									{
										String logErr3k21= "Pass:The Product Page UPC Checkother Stores value gets matched:\n" +frcheck1availabilityheadertitle+"\n" +frstrm1storeinfocombined+ "\n" +frcheck1availabilitystreetNametitle+ "\n" +frstrm1StoreinfoAddress1+ "\n" +frcheck1availabilitystoreLocationtitle+ "\n" +frstrm1StoreinfoAddress2+ "\n" +frcheck1availabilitystorePhonetitle+ "\n" +frstrm1Storeinfophone;  
										logInfo(logErr3k21);
									}	
									else
									{
										String logErr3k22= "Fail:The Product Page UPC Checkother Stores value doesn't gets matched:\n" +frcheck1availabilityheadertitle+"\n" +frstrm1storeinfocombined+ "\n" +frcheck1availabilitystreetNametitle+ "\n" +frstrm1StoreinfoAddress1+ "\n" +frcheck1availabilitystoreLocationtitle+ "\n" +frstrm1StoreinfoAddress2+ "\n" +frcheck1availabilitystorePhonetitle+ "\n" +frstrm1Storeinfophone;
										logInfo(logErr3k22);
									}
									Thread.sleep(5000);
									String logErr3k23= "-----------------------------------------------------------------------------";
									logInfo(logErr3k23);
									System.out.println("-----------------------------------------");  
								}	
								Thread.sleep(1000);
								driver.findElement(By.xpath("(//*[@class='otherStoreCloseBtnDiv'])")).click();
								Thread.sleep(3000);
								//Fr GVR: Buy as Gift
								if(frgvrctpageSourcefedfilavailability.equals("true")&&frgvrctpageSourceonlineavailability.equals("true"))
								{
									driver.findElement(By.xpath("(//*[@class='listAddToBag cls_assisted_checkout'])[2]")).click(); 
									Thread.sleep(2000);
									//Buy as Gift overlay
									String frgvrbuyasgiftoverlayPdtTitle=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])["+frgvrs+"]")).getText();		
									System.out.println("frgvrbuyasgiftoverlayPdtTitle:" +frgvrbuyasgiftoverlayPdtTitle);	
									String frgvrbuyasgiftoverlayPdtWebId=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])["+frgvrs+"]")).getText();		
									System.out.println("frgvrbuyasgiftoverlayPdtWebId:" +frgvrbuyasgiftoverlayPdtWebId);
									String frgvrbuyasgiftoverlayPriceDiv=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])["+frgvrs+"]")).getText();		
									System.out.println("frgvrbuyasgiftoverlayPriceDiv:" +frgvrbuyasgiftoverlayPriceDiv);
									String frgvrbuyasgiftoverlayColorInfoValue=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])["+frgvrs+"]")).getText();		
									System.out.println("frgvrbuyasgiftoverlayColorInfoValue:" +frgvrbuyasgiftoverlayColorInfoValue);					
									String frgvrbuyasgiftoverlaySizeInfoValue=driver.findElement(By.xpath("(//*[@class='overlaySizeInfoValue'])["+frgvrs+"]")).getText();		
									System.out.println("frgvrbuyasgiftoverlaySizeInfoValue:" +frgvrbuyasgiftoverlaySizeInfoValue);
									if(strmfrgvrskuinfosize1value.equals(frgvrbuyasgiftoverlaySizeInfoValue)&&strmfrgvrchilditeminfoupccolor.equals(frgvrbuyasgiftoverlayColorInfoValue)&&pricefrgvrvalueinpdp.equals(frgvrbuyasgiftoverlayPriceDiv))
									{
										String logErr3k24= "Pass:The Product details in the Buy as gift overlay gets match:\n" +frgvrbuyasgiftoverlayPdtTitle+ "\n" +frgvrbuyasgiftoverlayPdtWebId+ "\n" +frgvrbuyasgiftoverlayPriceDiv+ "\n" +frgvrbuyasgiftoverlayColorInfoValue+ "\n" +frgvrbuyasgiftoverlaySizeInfoValue;
										logInfo(logErr3k24);
									}
									else
									{
										String logErr3k25= "Fail:The Product details in the Buy as gift overlay gets match:\n" +frgvrbuyasgiftoverlayPdtTitle+ "\n" +frgvrbuyasgiftoverlayPdtWebId+ "\n" +frgvrbuyasgiftoverlayPriceDiv+ "\n" +frgvrbuyasgiftoverlayColorInfoValue+ "\n" +frgvrbuyasgiftoverlaySizeInfoValue;
										logInfo(logErr3k25);
									}
									//77.Verify that while selecting the Buy as Gift option for the individual product in the Find Registry GVR page the item should be added to the shopping bag
									String logErrsmp77="77,Verify that while selecting the Buy as Gift option for the individual product in the Find Registry GVR page the item should be added to the shopping bag,Pass"; 
									logInfo1(logErrsmp77);		
								}
								else
								{
									String logErr3k23= "Fail:The Product doesn't contain the availability of shipping";
									logInfo(logErr3k23);
								}
								driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();
								Thread.sleep(2000);		
								//Fr GVR: Add to registry
								String registryfrgvr=driver.getWindowHandle();
								System.out.println(registryfrgvr);
								if(frgvrpagesourceobjregistryinfotype.equals("Wedding"))
								{
									driver.findElement(By.xpath("(//*[@class='addtoregistry handlingPressState'])["+frgvrs+"]")).click();
									Thread.sleep(8000);
									driver.switchTo().frame("wssgPageFrame");
									//78.Verify that while selecting the Add to registry option for the individual product in the Find Registry GVR page the manage registry page should be displayed 
									if(driver.findElement(By.xpath("(//*[@class='bigger-bottom-margin'])")).isDisplayed())
									{
										//78.Verify that while selecting the Add to registry option for the individual product in the Find Registry GVR page the manage registry page should be displayed
										String logErrsmp78="78,Verify that while selecting the Add to registry option for the individual product in the Find Registry GVR page the manage registry page should be displayed,Pass"; 
										logInfo1(logErrsmp78);	
									}
									else
									{
										//78.Verify that while selecting the Add to registry option for the individual product in the Find Registry GVR page the manage registry page should be displayed
										String logErrsmf78="78,Verify that while selecting the Add to registry option for the individual product in the Find Registry GVR page the manage registry page should be displayed,Fail"; 
										logInfo1(logErrsmf78);	
									}
								}
								else
								{
									String logErr3k26= "Fail:The Product doesn't contain the Add to Registry option";
									logInfo(logErr3k26);
								}
								driver.findElement(By.xpath("(//*[@class='navbt-general navbt-static cancelBt'])")).click();
								Thread.sleep(2000);
								driver.switchTo().window(registryfrgvr);
							}
						}		
					}
					Thread.sleep(2000);
					//Fr GVR "Add to Favorites"
					driver.findElement(By.xpath("(//*[@class='favIcon'])")).click();
					Thread.sleep(2000);
					//79.Verify that while selecting the Add to Favorites option in the Find Registry GVR page all the items in the GVR page should be added to the favorites page 
					if(driver.findElement(By.xpath("(//*[@class='cls_alertMsg'])")).isEnabled())
					{
						//79.Verify that while selecting the Add to Favorites option in the Find Registry GVR page all the items in the GVR page should be added to the favorites page 
						String logErrsmp79="79,Verify that while selecting the Add to Favorites option in the Find Registry GVR page all the items in the GVR page should be added to the favorites page, Pass"; 
						logInfo1(logErrsmp79);					
					}
					else
					{
						//79.Verify that while selecting the Add to Favorites option in the Find Registry GVR page all the items in the GVR page should be added to the favorites page  
						String logErrsmf79="79,Verify that while selecting the Add to Favorites option in the Find Registry GVR page all the items in the GVR page should be added to the favorites page, Fail"; 
						logInfo1(logErrsmf79);
					}	
					//FR GVR "Share all" option
					driver.findElement(By.xpath("(//*[@class='shareIcon'])")).click();
					Thread.sleep(1000);
					driver.findElement(By.xpath("(//*[@class='cls_favListShareContinue'])")).click();
					Thread.sleep(2000);
					//80.Verify that while selecting the share all option in the Find Registry GVR page the email share overlay should be enabled 
					if(driver.findElement(By.xpath("(//*[@class='pdpSingleSharePopup handleBodyClick'])")).isDisplayed())  
					{
						//80.Verify that while selecting the share all option in the Find Registry GVR page the email share overlay should be enabled  
						String logErrsmp80="80,Verify that while selecting the share all option in the Find Registry GVR page the email share overlay should be enabled , Pass"; 
						logInfo1(logErrsmp80);
					}
					else
					{
						//80.Verify that while selecting the share all option in the Find Registry GVR page the email share overlay should be enabled  
						String logErrsmf80="80,Verify that while selecting the share all option in the Find Registry GVR page the email share overlay should be enabled , Fail"; 
						logInfo1(logErrsmf80);
					}	
					driver.findElement(By.xpath("//*[@id='id_custNameText']")).sendKeys("QA");
					driver.findElement(By.xpath("//*[@id='id_custEmailText']")).sendKeys("naresh@skava.com");
					driver.findElement(By.xpath("//*[@id='id_associateNameText']")).sendKeys("QA");
					driver.findElement(By.xpath("//*[@id='id_associateEmailText']")).sendKeys("q@q.com");
					driver.findElement(By.xpath("(//*[@class='singelPdtEmailShare'])")).click();
					Thread.sleep(3000);	
					String frfvrsharesuccess=driver.findElement(By.xpath("(//*[@class='mailStatusInfo']")).getText();
					System.out.println("frfvrsharesuccess:" +frfvrsharesuccess);
					if(frfvrsharesuccess.equals("Your email has been sent successfully"))
					{
						//81.Verify that �email success� overlay should be displayed in Find Registry GVR page when the e-mail has been send successfully
						String logErrsmp81 ="81,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, Pass"; 
						logInfo(logErrsmp81);
					}  
					else
					{
						//81.Verify that �email success� overlay should be displayed in Find Registry GVR page when the e-mail has been send successfully
						String logErrsmf81 ="81,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, fail"; 
						logInfo(logErrsmf81);
					} 			
				}		
				driver.findElement(By.xpath("(//*[@class='cls_mamFMacysLogoCont'])")).click();	
				//driver.switchTo().window(registrylpf);
			}		
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static String fileRead(InputStream inputStream) 
	{
		String response = "";
		try{

			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			String line = "";
			while((line = br.readLine())!=null){
				response +=line;
			}

		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
		return response;
	}
	private static String concat(String strmStoreinfoIdentifier,String strmStoreinfoName)
	{
		return null;
	} 
	private static String readFile(String filename)
	{
		String retData =  null;

		try
		{
			byte data[] = null;
			FileInputStream fis = new FileInputStream(filename);
			DataInputStream dis = new DataInputStream(fis);
			data = new byte[fis.available()];
			dis.readFully(data);
			retData = new String(data);
		}
		catch(Exception e)
		{
			System.out.println("Exception in readfile "+ e.toString());
		}
		return retData;
	}
}